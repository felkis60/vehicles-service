package controllers

import (
	"gitlab.com/felkis60/vehicles-service/repository"
	_ "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type RansomOrderController struct {
	Repository repository.RansomOrderRepository
}

// @Summary Create ransom order
// @Tags Ransom Orders
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param   _     body it.InputCreateRansomOrder true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-orders/ [post]
func (ctrl *RansomOrderController) Create(c *gin.Context) {
	var input it.InputCreateRansomOrder
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Edit ransom order
// @Tags Ransom Orders
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param   _     body it.InputEditRansomOrder true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-orders/{uid}/edit [post]
func (ctrl *RansomOrderController) Edit(c *gin.Context) {
	var input it.InputEditRansomOrder
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), &input, c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Delete ransom order
// @Tags Ransom Orders
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Ransom Order UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-orders/{uid}/delete [post]
func (ctrl *RansomOrderController) Delete(c *gin.Context) {

	data, err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get one ransom order
// @Tags Ransom Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Ransom Order UID"
// @Success 200 {object} _.RansomOrder
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-orders/{uid} [post]
func (ctrl *RansomOrderController) GetByUID(c *gin.Context) {

	ransomOrder, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string), true, true)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: ransomOrder})
}

// @Summary Get ransom orders
// @Tags Ransom Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListRansomOrders true "Request body"
// @Success 200 {object} []_.RansomOrder
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-orders/list [post]
func (ctrl *RansomOrderController) List(c *gin.Context) {
	var input it.InputGetListRansomOrders
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}

// @Summary Get ransom orders
// @Tags Ransom Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListRansomOrders true "Request body"
// @Success 200 {object} []_.RansomOrder
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-orders/search [post]
func (ctrl *RansomOrderController) Search(c *gin.Context) {
	var input it.InputGetListRansomOrders
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.Search(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}

// @Summary Contribute to order
// @Tags Ransom Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputContributeIntoRansomOrder true "Request body"
// @Success 200 {object} []_.RansomOrder
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-orders/{uid}/contribute [post]
func (ctrl *RansomOrderController) Contribute(c *gin.Context) {
	var input it.InputContributeIntoRansomOrder
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.Contribute(c.MustGet("uid").(string), &input, c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}

// @Summary Get ransom orders
// @Tags Ransom Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListRansomOrders true "Request body"
// @Success 200 {object} []_.RansomOrder
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-orders/list [post]
func (ctrl *RansomOrderController) History(c *gin.Context) {
	var input it.InputGetHistoryList
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.History(c.MustGet("uid").(string), c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}
