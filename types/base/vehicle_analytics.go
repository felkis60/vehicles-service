package models

type VehicleAnalytics struct {
	VehiclesID                                   int64   `json:"vehicles_id"`
	BuyDate                                      string  `json:"buy_date"` //- находим операцию с категорией ext_id = 14 и берем date операции
	VehiclesMark                                 string  `json:"vehicles_mark"`
	VehiclesModel                                string  `json:"vehicles_model"`
	VehiclesVin                                  string  `json:"vehicles_vin"`
	VehiclesNumber                               string  `json:"vehicles_number"`
	VehiclesSts                                  string  `json:"vehicles_sts"`
	VehiclesOwner                                string  `json:"vehicles_owner"`
	VehicleAmount                                float64 `json:"vehicle_amount"`                      // - находим операции с категорией ext_id = 14 и берем amount операции
	Users                                        *[]User `json:"users"`                               //- все пользователи в договорах которого участвовала машина
	AllVehicleAmount                             float64 `json:"all_vehicle_amount"`                  // - total_vehicle_amount из тарифа ransom is_active = true
	Deposit                                      float64 `json:"deposit"`                             //- все операции по машине в deposit = true
	VehicleAmountForUser                         float64 `json:"vehicle_amount_for_user"`             //- vehicle_amount из тарифа ransom is_active = true
	DaysCount                                    uint64  `json:"days_count"`                          //- количество дней по операциям с категорией ext_id = 8
	DayPaymentAmount                             float64 `json:"day_payment_amount"`                  //- day_payment из тарифа ransom is_active = true умножаем на X 30.4167
	Result                                       float64 `json:"result"`                              //- (сумма всех операций в категориях ext_id IN (10,48,57) AND parent_id{ext_id} IN (49,54,58)) минус (сумма всех операций в категориях ext_id IN (14,16,17,20,21) AND parent_id{ext_id} IN (15,19,23)
	MetaVehicleAmount                            float64 `json:"meta_vehicle_amount"`                 //- current.vehicleAmount + current.repairCreditAmount + current.otherCreditAmount
	VehicleAmountWithoutInititalFee              float64 `json:"vehicle_amount_without_initital_fee"` //- берем тариф ransom is_active = true (vehicle_amount - initial_fee)
	YearKoeff                                    float64 `json:"year_koeff"`
	RansTariffTotalVehicleWithoutInsuranceAmount float64 `json:"rans_tariff_total_vehicle_without_insurance_amount"`
	RansTariffInsuranceRate                      int64   `json:"rans_tariff_insurance_rate"`
	MonthPaymentAmount                           float64 `json:"month_payment_amount"`
	DayPaymentsVehicleAmount                     float64 `json:"day_payments_vehicle_amount"` //-
	//const yearKoeff = ransom_tariff.term / 12
	//const termDays = yearKoeff X 365
	//return (current.vehicle_amount_without_initital_fee / termDays) X current.days_count
	DayPaymentsMetaVehicleAmount  float64 `json:"day_payments_meta_vehicle_amount"`  //- (current.meta_vehicle_amount / termDays) X current.days_count
	MandatoryPaymentAmount        float64 `json:"mandatory_payment_amount"`          // - (ransom_tariff.tracker_service_amount X ransom_tariff.term) + (ransom_tariff.tracker_amount) + (ransom_tariff.vehicle_tax_amount X yearKoeff) + (ransom_tariff.registration_amount) + (ransom_tariff.insurance_osago_amount X yearKoeff)
	DayPaymentsMandatoryAmount    float64 `json:"day_payments_mandatory_amount"`     // - (current.mandatory_payment_amount / termDays) X current.days_count
	InvestAmount                  float64 `json:"invest_amount"`                     // - (ransom_tariff.vehicle_amount) + (ransom_tariff.tracker_service_amount X ransom_tariff.term) + (ransom_tariff.vehicle_tax_amoun X yearKoeff) + (ransom_tariff.registration_amount) + (ransom_tariff.tracker_amount) + (ransom_tariff.insurance_osago_amount X yearKoeff)
	DayPaymentsCoverageAmount     float64 `json:"day_payments_coverage_amount"`      // - ((ransom_tariff.total_vehicle_without_insurance_amount - current.invest_amount) / termDays) X current.days_count
	SecurityDepositAmount         float64 `json:"security_deposit_amount"`           // - ((ransom_tariff.total_vehicle_without_insurance_amount  X (ransom_tariff.insurance_amount / 100)) / termDays) X current.days_count
	DayPaymentsMarginIncomeAmount float64 `json:"day_payments_margin_income_amount"` // - ((tariff.vehicle_amount - current.meta_vehicle_amount) / termDays) * current.days_count

	TrackerInstallAmount           float64 `json:"tracker_install_amount"`            //[16]
	TrackerMonthAmount             float64 `json:"tracker_month_amount"`              //[17]
	RegistrationAmount             float64 `json:"registration_amount"`               // [20],
	RepairCreditAmount             float64 `json:"repair_credit_amount"`              // [21],
	OtherCreditAmount              float64 `json:"other_credit_amount"`               // [23],
	VehicleTaxAmount               float64 `json:"vehicle_tax_amount"`                // [19],
	TotalCreditAmount              float64 `json:"total_credit_amount"`               // [14, 16, 17, 20, 21], // = 14 ~ 2000, 17 ~ 1000 = 3000
	RentDebitAmount                float64 `json:"rent_debit_amount"`                 // [10],
	InitialPaymentAmount           float64 `json:"initial_payment_amount"`            // [48],
	CategoryMandatoryPaymentAmount float64 `json:"category_mandatory_payment_amount"` // [49],
	RepairDebitAmount              float64 `json:"repair_debit_amount"`               // [57],
	TotalDebitAmount               float64 `json:"total_debit_amount"`                // [10, 48, 57],
	DayPaymentsAmount              float64 `json:"day_payments_amount"`               // [8],

	//CATEGORIES_PARENTS_IDS
	ParentsOsagoAndDkAmount       float64 `json:"parents_osago_and_dk_amount"`      // [15],
	ParentsTotalCreditAmount      float64 `json:"parents_total_credit_amount"`      // [15, 19, 23], // = 15 ~ 200, 23 ~ 3000 = 3200
	ParentsMandatoryPaymentAmount float64 `json:"parents_mandatory_payment_amount"` // [49, 54],
	ParentsOtherDebitAmount       float64 `json:"parents_other_debit_amount"`       // [58],
	ParentsTotalDebitAmount       float64 `json:"parents_total_debit_amount"`       // [49, 54, 58],

	OrdersUIDs *[]string `json:"orders_uids"`
}
