package providers

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"os"

	ot "gitlab.com/felkis60/vehicles-service/types/other"
)

type RexProviderType string

const RexProvider RexProviderType = "https://rent.2012222.ru/"

func (provider RexProviderType) VehicleList() (*[]ot.RexVehicle, error) {

	var jsonResp ot.ResponceRexGetVehcile

	req, err := http.NewRequest("GET", string(provider)+"api/v1/vehicles", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-Auth-Public-Token", os.Getenv("rex_provider_token"))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &jsonResp)
	if err != nil {
		return nil, err
	}

	tempVehicles, err := json.Marshal(jsonResp.Payload.(map[string]interface{})["items"].([]interface{}))
	if err != nil {
		return nil, err
	}
	var vehicles []ot.RexVehicle
	json.Unmarshal(tempVehicles, &vehicles)

	return &vehicles, nil

}

func (provider RexProviderType) UserList() (*[]ot.RexUser, error) {

	var jsonResp ot.ResponceRexGetVehcile

	req, err := http.NewRequest("GET", string(provider)+"api/v1/rent-users", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-Auth-Public-Token", os.Getenv("rex_provider_token"))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &jsonResp)
	if err != nil {
		return nil, err
	}

	tempUsers, err := json.Marshal(jsonResp.Payload.(map[string]interface{})["items"].([]interface{}))
	if err != nil {
		return nil, err
	}
	var users []ot.RexUser
	json.Unmarshal(tempUsers, &users)

	return &users, nil

}

func (provider RexProviderType) RansomTariffList() (*[]ot.RexRansomTariff, error) {

	var jsonResp ot.ResponceRexGetVehcile

	req, err := http.NewRequest("GET", string(provider)+"api/v1/rent-users/tariffs/list", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-Auth-Public-Token", os.Getenv("rex_provider_token"))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &jsonResp)
	if err != nil {
		return nil, err
	}

	tempTariffs, err := json.Marshal(jsonResp.Payload.(map[string]interface{})["items"].([]interface{}))
	if err != nil {
		return nil, err
	}
	var tariffs []ot.RexRansomTariff
	json.Unmarshal(tempTariffs, &tariffs)

	return &tariffs, nil

}

func (provider RexProviderType) RansomOrderList() (*[]ot.RexRansomOrder, error) {

	var jsonResp ot.ResponceRexGetVehcile

	req, err := http.NewRequest("GET", string(provider)+"api/v1/rent-users/vehicle-rent-users-list", nil)
	if err != nil {
		return nil, errors.New("Ransom Order List 1: " + err.Error())
	}
	req.Header.Set("X-Auth-Public-Token", os.Getenv("rex_provider_token"))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.New("Ransom Order List 2: " + err.Error())
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.New("Ransom Order List 3: " + err.Error())
	}

	err = json.Unmarshal(body, &jsonResp)
	if err != nil {
		return nil, errors.New("Ransom Order List 4: " + err.Error())
	}

	tempOrders, err := json.Marshal(jsonResp.Payload.(map[string]interface{})["items"].([]interface{}))
	if err != nil {
		return nil, errors.New("Ransom Order List 5: " + err.Error())
	}
	var tariffs []ot.RexRansomOrder
	json.Unmarshal(tempOrders, &tariffs)

	return &tariffs, nil

}
