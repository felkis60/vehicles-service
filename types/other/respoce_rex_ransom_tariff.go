package models

import (
	"strconv"

	it "gitlab.com/felkis60/vehicles-service/types/input"
)

type RexRansomTariff struct {
	ID                                 int64   `json:"id"`
	VehiclesID                         int64   `json:"vehicles_id"`
	InitialFee                         int     `json:"initial_fee"`
	VehicleAmount                      string  `json:"vehicle_amount"`
	TrackerAmount                      string  `json:"tracker_amount"`
	VehicleTaxAmount                   string  `json:"vehicle_tax_amount"`
	RegistrationAmount                 string  `json:"registration_amount"`
	TrackerServiceAmount               string  `json:"tracker_service_amount"`
	InsuranceOsagoAmount               string  `json:"insurance_osago_amount"`
	Term                               int     `json:"term"`
	YearRate                           float64 `json:"year_rate"`
	InsuranceRate                      int     `json:"insurance_rate"`
	MonthPayment                       string  `json:"month_payment"`
	DayPayment                         string  `json:"day_payment"`
	TotalVehicleAmount                 string  `json:"total_vehicle_amount"`
	TotalVehicleWithoutInsuranceAmount string  `json:"total_vehicle_without_insurance_amount"`
	CreatedAt                          string  `json:"created_at"`
	UpdatedAt                          string  `json:"updated_at"`
	AchivedAt                          string  `json:"achive_at"`
}

func (from *RexRansomTariff) ToInputCreateRansomTariff(to *it.InputCreateRansomTariff) {

	var tempFloat float64
	var err error

	to.ExtID = from.ID
	to.InitialFee = from.InitialFee
	if tempFloat, err = strconv.ParseFloat(from.VehicleAmount, 64); err == nil {
		to.VehicleAmount = tempFloat
	}
	if tempFloat, err = strconv.ParseFloat(from.TrackerAmount, 64); err == nil {
		to.TrackerAmount = tempFloat
	}
	if tempFloat, err = strconv.ParseFloat(from.VehicleTaxAmount, 64); err == nil {
		to.VehicleTaxAmount = tempFloat
	}
	if tempFloat, err = strconv.ParseFloat(from.RegistrationAmount, 64); err == nil {
		to.RegistrationAmount = tempFloat
	}
	if tempFloat, err = strconv.ParseFloat(from.InsuranceOsagoAmount, 64); err == nil {
		to.InsuranceOsagoAmount = tempFloat
	}
	to.Term = from.Term
	to.YearRate = from.YearRate
	to.InsuranceRate = from.InsuranceRate
	if tempFloat, err = strconv.ParseFloat(from.MonthPayment, 64); err == nil {
		to.MonthPayment = tempFloat
	}
	if tempFloat, err = strconv.ParseFloat(from.DayPayment, 64); err == nil {
		to.DayPayment = tempFloat
	}
	if tempFloat, err = strconv.ParseFloat(from.TotalVehicleAmount, 64); err == nil {
		to.TotalVehicleAmount = tempFloat
	}
	if tempFloat, err = strconv.ParseFloat(from.TotalVehicleWithoutInsuranceAmount, 64); err == nil {
		to.TotalVehicleWithoutInsuranceAmount = tempFloat
	}
	if tempFloat, err = strconv.ParseFloat(from.TrackerServiceAmount, 64); err == nil {
		to.TrackerServiceAmount = tempFloat
	}
	to.CreatedAt = &from.CreatedAt

}
