package controllers

import (
	"context"
	"encoding/json"
	"errors"

	pb "gitlab.com/felkis60/vehicles-service/api/grpc/app"
	"gitlab.com/felkis60/vehicles-service/helpers"
	logs "gitlab.com/felkis60/vehicles-service/log"
	"gitlab.com/felkis60/vehicles-service/repository"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"
)

type GRPCOrderServer struct {
	Repository repository.OrderRepository
	pb.UnimplementedOrderServiceServer
}

func (s *GRPCOrderServer) List(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputGetListOrders
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	pagInfo, err := s.Repository.List2(req.AccountToken, &unmarshledInput, true)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonPage, err := json.Marshal(pagInfo)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonPage}, nil
}

func (s *GRPCOrderServer) ListFromAllAccounts(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputGetListOrders
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	pagInfo, err := s.Repository.List2(req.AccountToken, &unmarshledInput, false)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonPage, err := json.Marshal(pagInfo)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonPage}, nil
}

func (s *GRPCOrderServer) PeriodSummary(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputGetPeriodSummary
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.PeriodSummary(req.AccountToken, &unmarshledInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}
