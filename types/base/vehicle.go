package models

import (
	"github.com/lib/pq"
	db "gitlab.com/felkis60/vehicles-service/database/elastic_search"

	"time"

	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type Vehicle struct {
	ID                    int64           `json:"id" gorm:"primarykey"`
	UID                   string          `json:"uid"`
	ExtID                 int64           `json:"ext_id"`
	OtherExtIDs           pq.Int64Array   `json:"other_ext_ids" gorm:"type:integer[]" swaggertype:"array,integer"`
	AccountsID            int64           `json:"accounts_id"`
	Number                string          `json:"number"`
	Mark                  string          `json:"mark"`
	Model                 string          `json:"model"`
	Type                  string          `json:"type"`
	Sts                   string          `json:"sts"`
	Win                   string          `json:"win"`
	Width                 *int            `json:"widht"`
	Height                *int            `json:"height"`
	Depth                 *int            `json:"depth"`
	Capacity              *float32        `json:"capacity"`
	FuelTank              *int            `json:"fuel_tank"`
	SummerRate            *float32        `json:"summer_rate"`
	WinterRate            *float32        `json:"winter_rate"`
	Ransom                bool            `json:"ransom"`
	Description           string          `json:"description"`
	Bodywork              string          `json:"bodywork"`                           // Кузов
	Color                 string          `json:"color"`                              // Цвет
	CustomFields          datatypes.JSON  `json:"custom_fields" swaggertype:"string"` // Свободные поля
	Drive                 string          `json:"drive"`                              // Привод
	Engine                string          `json:"engine"`                             // Двигатель
	Kpp                   string          `json:"kpp"`                                // Коробка передач
	Owner                 string          `json:"owner"`                              // Владелец
	RentUsersID           *int            `json:"rent_users_id"`                      // Идентификатор арендатора
	Salon                 string          `json:"salon"`                              // Салон
	Year                  string          `json:"year"`                               // Год выпуска
	DefaultRansomTariffID *int64          `json:"default_ransom_tariff_id"`           // Тариф на рассрочку по умолчанию
	Status                string          `json:"status" gorm:"default:FREE"`         // Статус
	DateOsagoStart        *time.Time      `json:"date_osago_start"`                   // Дата начала ОСАГО
	DateOsagoEnd          *time.Time      `json:"date_osago_end"`                     // Дата окончания ОСАГО
	BuyDate               *time.Time      `json:"buy_date"`                           // Дата покупки
	BuyAmount             *float64        `json:"buy_amount"`                         // Цена покупки
	RegistrationDate      *time.Time      `json:"registration_date"`                  // Дата постановки на учет
	TrackerInstallDate    *time.Time      `json:"tracker_install_date"`               // Дата установки трекера
	TrackerNumber         string          `json:"tracker_number"`                     // Номер трекера
	LighthouseNumber      string          `json:"lighthouse_number"`                  // Номер маяка
	DKPAmount             float64         `json:"dkp_amount"`
	RentOrder             *RentOrder      `json:"rent_order,omitempty" gorm:"-"`
	RansomOrder           *RansomOrder    `json:"ransom_order,omitempty" gorm:"-"`
	RentOrders            *[]RentOrder    `json:"rent_orders,omitempty" gorm:"-"`
	RansomOrders          *[]RansomOrder  `json:"ransom_orders,omitempty" gorm:"-"`
	RentTariffs           *[]RentTariff   `json:"rent_tariffs,omitempty" gorm:"-"`
	RansomTariffs         *[]RansomTariff `json:"ransom_tariffs,omitempty" gorm:"-"`
	Bill                  *Bill           `json:"bill,omitempty" gorm:"-"`
	LastPayment           *interface{}    `json:"last_payment,omitempty" gorm:"-"`
	RemindersCount        *int64          `json:"reminders_count,omitempty" gorm:"-"`
	AvatarUri             string          `json:"avatar_uri"`
	DkpEnterFiles         datatypes.JSON  `json:"dkp_enter_files" swaggertype:"string"`
	StsFiles              datatypes.JSON  `json:"sts_files" swaggertype:"string"`
	OsagoFiles            datatypes.JSON  `json:"osago_files" swaggertype:"string"`
	DutyFiles             datatypes.JSON  `json:"duty_files" swaggertype:"string"`
	ContractsFiles        datatypes.JSON  `json:"contracts_files" swaggertype:"string"`
	OtherFiles            datatypes.JSON  `json:"other_files" swaggertype:"string"`
	ImagesFiles           datatypes.JSON  `json:"images_files" swaggertype:"string"`

	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}

//func (u *Vehicle) AfterCreate(tx *gorm.DB) (err error) {
//
//	db.ESAddEdit("rent-sevice-vehicle", data)
//
//	return
//}

func (v *Vehicle) AfterCreate(tx *gorm.DB) (err error) {

	db.ESAddEdit("rent-sevice-vehicle", v)

	return nil
}
