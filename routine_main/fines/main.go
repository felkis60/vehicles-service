package main

import (
	"os"

	"gitlab.com/felkis60/vehicles-service/database"
	startup "gitlab.com/felkis60/vehicles-service/init"
	"gitlab.com/felkis60/vehicles-service/routines"
	bt "gitlab.com/felkis60/vehicles-service/types/base"

	log "github.com/sirupsen/logrus"
)

func main() {
	var args = os.Args
	var ignorTG = false
	var firstPageOnly = false
	var argsMap = make(map[string]bool)

	for i := range args {
		argsMap[args[i]] = true
	}

	if argsMap["--ignortg"] || argsMap["ignortg"] {
		ignorTG = true
	}

	if argsMap["--firstPageOnly"] || argsMap["firstPageOnly"] {
		firstPageOnly = true
	}

	startup.SystemStartup(true, true, false)
	var accounts []bt.Account
	if result := database.Db.Find(&accounts, "accounts.deleted_at IS NULL"); result.Error != nil {
		log.Warn(result.Error)
		return
	}
	for i := range accounts {
		if accounts[i].ShtrafovnetAuthToken != "" && accounts[i].ShtrafovnetCompanyID != 0 {
			routines.SyncFinesFromShtrafovnet(&accounts[i], ignorTG, firstPageOnly)
		}
	}
}
