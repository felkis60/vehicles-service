package models

import (
	"gorm.io/datatypes"
)

type InputCreateUser struct {
	ExtID                int64          `json:"ext_id"`
	FirstName            string         `json:"first_name"`
	LastName             string         `json:"last_name"`
	MiddleName           string         `json:"middle_name"`
	Phone                string         `json:"phone"`
	FirebaseUID          string         `json:"firebase_uid"`
	Firebase             datatypes.JSON `json:"firebase" swaggertype:"string"`
	PartnerType          string         `json:"partner_type" example:"LEGAL|PHYSICAL"`
	PassportNumber       string         `json:"passport_number"`
	PassportSeries       string         `json:"passport_series"`
	PassportIssuedBy     string         `json:"passport_issued_by"`
	PassportDateOfIssue  string         `json:"passport_date_of_issue" example:"YYYY-MM-DD"`
	PassportPlaceOfIssue string         `json:"passport_place_of_issue"`
	Birthday             string         `json:"birthday" example:"YYYY-MM-DD"`
	Email                string         `json:"email"`
	ContactPerson        string         `json:"contact_person"`
	Comment              string         `json:"comment"`
	AddressOfLiving      string         `json:"address_of_living"`
	LegalAddress         string         `json:"legal_address"`
	INN                  string         `json:"inn"`
	OGRN                 string         `json:"ogrn"`
	Bank                 string         `json:"bank"`
	BIK                  string         `json:"bik"`
	KPP                  string         `json:"kpp"`
	CheckingBill         string         `json:"checking_bill"`
	CorrespondentBill    string         `json:"correspondent_bill"`
	CreatedAt            *string        `json:"created_at"`
}

type InputEditUser struct {
	ExtID                *int64          `json:"ext_id"`
	FirstName            *string         `json:"first_name"`
	LastName             *string         `json:"last_name"`
	MiddleName           *string         `json:"middle_name"`
	Phone                *string         `json:"phone"`
	FirebaseUID          *string         `json:"firebase_uid"`
	PartnerType          *string         `json:"partner_type" example:"LEGAL|PHYSICAL"`
	PassportNumber       *string         `json:"passport_number"`
	PassportSeries       *string         `json:"passport_series"`
	PassportIssuedBy     *string         `json:"passport_issued_by"`
	PassportDateOfIssue  *string         `json:"passport_date_of_issue" example:"YYYY-MM-DD"`
	PassportPlaceOfIssue *string         `json:"passport_place_of_issue"`
	Birthday             *string         `json:"birthday" example:"YYYY-MM-DD"`
	Email                *string         `json:"email"`
	ContactPerson        *string         `json:"contact_person"`
	Comment              *string         `json:"comment"`
	AddressOfLiving      *string         `json:"address_of_living"`
	LegalAddress         *string         `json:"legal_address"`
	INN                  *string         `json:"inn"`
	OGRN                 *string         `json:"ogrn"`
	Bank                 *string         `json:"bank"`
	BIK                  *string         `json:"bik"`
	KPP                  *string         `json:"kpp"`
	CheckingBill         *string         `json:"checking_bill"`
	CorrespondentBill    *string         `json:"correspondent_bill"`
	Firebase             *datatypes.JSON `json:"firebase" swaggertype:"string"`
}
