package main

import (
	"log"
	"os"

	r "gitlab.com/felkis60/vehicles-service/api/rest/router"
	s "gitlab.com/felkis60/vehicles-service/init"
	l "gitlab.com/felkis60/vehicles-service/log"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	_ "gitlab.com/felkis60/vehicles-service/api/rest/server/docs"
)

func main() {
	s.SystemStartup(true, true, true)
	router := r.SetupRouter()
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	log.Printf(l.INFStartServer, "rest")
	router.Run(":" + os.Getenv("RESTPORT"))

}
