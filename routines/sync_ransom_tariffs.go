package routines

import (
	"log"
	"strconv"

	db "gitlab.com/felkis60/vehicles-service/database"
	logs "gitlab.com/felkis60/vehicles-service/log"
	p "gitlab.com/felkis60/vehicles-service/providers"
	"gitlab.com/felkis60/vehicles-service/repository"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"
)

func syncRansomTariffs(accountID int64) {

	log.Print("INFO: Ransom Tariffs sync started")

	tariffs, err := p.RexProvider.RansomTariffList()
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Print("INFO: Ransom Tariffs recived: " + strconv.Itoa(len(*tariffs)))
	var tempCreated = 0
	var tempRep = repository.RansomTariffRepository{Db: db.Db}
	for _, tf := range *tariffs {
		var tempTariff bt.RansomTariff
		if result := db.Db.Find(&tempTariff, "ext_id = ?", tf.ID); result.RowsAffected > 0 {
			continue
		}

		var tempInput it.InputCreateRansomTariff
		tf.ToInputCreateRansomTariff(&tempInput)

		if tf.VehiclesID > 0 {
			var tempVehicle bt.Vehicle
			if result := db.Db.Find(&tempVehicle, "(ext_id = ? OR ? = ANY(other_ext_ids))", tf.VehiclesID, tf.VehiclesID); result.RowsAffected > 0 {
				tempInput.VehiclesUID = tempVehicle.UID
			} else {
				log.Print("ERROR: No vehicle with ext_id = " + strconv.FormatUint(uint64(tf.VehiclesID), 10))
				continue
			}
		}

		if _, err := tempRep.Create(&tempInput, accountID, 0); err != nil {
			log.Print(err)
		} else {
			tempCreated++
		}
	}
	log.Print("INFO: Ransom Tariffs created: " + strconv.Itoa(tempCreated))
	log.Println(logs.INFRansomTariffsSyncedREX)
}
