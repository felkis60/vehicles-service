package models

type RexRansomOrder struct {
	ID                     int64   `json:"id"`
	VehiclesID             int64   `json:"vehicles_id"`
	RentUsersID            int64   `json:"rent_users_id"`
	TariffsID              int64   `json:"tariffs_id"`
	ContractDate           string  `json:"contract_date"`
	ContractEndAt          *string `json:"contract_end_at"`
	ContractEndDescription string  `json:"contract_end_description"`
	CreatedAt              string  `json:"created_at"`
	UpdatedAt              string  `json:"updated_at"`
	AchivedAt              string  `json:"achive_at"`
}
