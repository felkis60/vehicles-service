package models

type InputCreateRentTariff struct {
	ExtID       int64   `json:"ext_id"`
	VehiclesUID string  `json:"vehicles_uid"`
	FromDays    int     `json:"from_days"`
	ToDays      int     `json:"to_days"`
	DayPayment  float64 `json:"day_payment"`
}

type InputEditRentTariff struct {
	ExtID      *int64   `json:"ext_id"`
	FromDays   *int     `json:"from_days"`
	ToDays     *int     `json:"to_days"`
	DayPayment *float64 `json:"day_payment"`
}
