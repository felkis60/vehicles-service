package models

import (
	"time"

	"gorm.io/gorm"
)

type Manager struct {
	ID           int64                     `json:"id" gorm:"primarykey"`
	UID          string                    `json:"uid"`
	ExtID        int64                     `json:"ext_id"`
	AccountsID   int64                     `json:"accounts_id"`
	FirstName    string                    `json:"first_name"`
	MiddleName   string                    `json:"middle_name"`
	LastName     string                    `json:"last_name"`
	Phone        string                    `json:"phone"`
	Email        string                    `json:"email"`
	Birthday     time.Time                 `json:"birthday"`
	RansomOrders *[]map[string]interface{} `json:"ransom_orders,omitempty" gorm:"-"`
	RentOrders   *[]map[string]interface{} `json:"rent_orders,omitempty" gorm:"-"`
	CreatedAt    time.Time                 `json:"created_at"`
	UpdatedAt    time.Time                 `json:"updated_at"`
	DeletedAt    gorm.DeletedAt            `json:"deleted_at" gorm:"index" swaggertype:"string"`
}
