#!/bin/bash

cd /var/www/microservices/vehicles_service/routine_main && go build .
cd /var/www/microservices/vehicles_service/routine_main/copy_vehicles && go build .
cd /var/www/microservices/vehicles_service/routine_main/mandatory_payment && go build .
cd /var/www/microservices/vehicles_service/routine_main/telegram && go build .
cd /var/www/microservices/vehicles_service/routine_main/update_enddate_rent && go build .