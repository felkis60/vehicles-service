package repository

import (
	"errors"
	"math"

	logs "gitlab.com/felkis60/vehicles-service/log"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"gorm.io/gorm"
)

type AccountRepository struct {
	Db *gorm.DB
}

func (rep *AccountRepository) Create(data *bt.Account, input *it.InputCreateAccount) error {

	var account bt.Account
	if result := rep.Db.Find(&account, "token = ?", input.Token); result.RowsAffected != 0 {
		return errors.New(logs.ERRTokenIsOccupied)
	}
	data.Name = input.Name
	data.Token = input.Token

	if data.Name == "" {
		return errors.New(logs.ErrNameReq)
	}

	if data.Token == "" {
		return errors.New(logs.ERRTokenReq)
	}

	rep.Db.Create(data)

	return nil
}

func (rep *AccountRepository) Edit(data *bt.Account, input *it.InputEditAccount, token string) error {

	if err := rep.Get(data, token); err != nil {
		return err
	}

	data.Name = input.Name
	if input.NewToken != "" {
		var account bt.Account
		if result := rep.Db.Find(&account, "token = ?", input.NewToken); result.RowsAffected != 0 {
			return errors.New(logs.ERRTokenIsOccupied)
		}
		data.Token = input.NewToken
	}

	if data.Name == "" {
		return errors.New(logs.ErrNameReq)
	}

	if data.Token == "" {
		return errors.New(logs.ERRTokenReq)
	}

	rep.Db.Save(data)
	return nil
}

func (rep *AccountRepository) Get(data *bt.Account, token string) error {
	if result := rep.Db.Find(data, "token = ?", token); result.RowsAffected == 0 {
		return errors.New(logs.ERRNoSuchToken)
	}
	return nil
}

func (rep *AccountRepository) List(input *it.InputGetList) (*helpers.Pagination, error) {

	var tempCount int64
	rep.Db.Model(&bt.Account{}).Count(&tempCount)

	var orderString = ""
	if input.OrderBy != "" {
		orderString = "accounts." + input.OrderBy
	}

	if input.OrderDir != "" {
		orderString += " " + input.OrderDir
	}

	var tx2 = rep.Db.Model(&bt.Account{})

	if orderString != "" {
		tx2 = tx2.Order(orderString)
	}

	var data []bt.Account
	if result := helpers.PaginateManual(&input.Page, &input.PageSize, 5, 100)(tx2).Scan(&data); result.Error != nil {
		return nil, result.Error
	}

	var tempPages = int(math.Ceil(float64(tempCount) / float64(input.PageSize)))

	return &helpers.Pagination{Page: input.Page, Items: &data, TotalItems: tempCount, TotalPages: tempPages}, nil
}
