package models

type FullSysInfo struct {
	TotalVehicles                 int64            `json:"total_vehicles"`             //Calculated in Vehicles Service
	TotalActiveRansomOrders       int64            `json:"total_active_ransom_orders"` //Calculated in Vehicles Service
	TotalActiveRentOrders         int64            `json:"total_active_rent_orders"`   //Calculated in Vehicles Service
	TotalVehiclesByStatuses       map[string]int64 `json:"total_vehicles_by_statuses"` //Calculated in Vehicles Service
	DebtorsOrdersAmount           int64            `json:"debtors_orders_amount"`
	DebtorsBalance                float64          `json:"debtors_balance"`
	CoverageAmount                float64          `json:"coverage_amount"`
	ResidualFleetAmount           float64          `json:"residual_fleet_amount"`
	PlannedIncomeAmount           float64          `json:"planned_income_amount"` //Calculated in Vehicles Service
	ActualIncomeAmount            float64          `json:"actual_income_amount"`
	RansomBalanceOperationsAmount float64          `json:"ransom_balance_operations_amount"`
	RentBalanceOperationsAmount   float64          `json:"rent_balance_operations_amount"`
	ActiveRansomOrdersUIDs        []string         `json:"active_ransom_orders_uids"`          //Calculated in Vehicles Service
	ActiveRentOrdersUIDs          []string         `json:"active_rent_orders_uids"`            //Calculated in Vehicles Service
	RansomOrdersUIDs              []string         `json:"ransom_orders_uids"`                 //Calculated in Vehicles Service
	RentOrdersUIDs                []string         `json:"rent_orders_uids"`                   //Calculated in Vehicles Service
	SumOfTotalVehicleAmountActive float64          `json:"sum_of_total_vehicle_amount_active"` //Calculated in Vehicles Service
	SumOfTotalVehicleAmount       float64          `json:"sum_of_total_vehicle_amount"`        //Calculated in Vehicles Service
}

type SysInfoByDate struct {
	TotalCreatedVehicles       int64            `json:"total_created_vehiles"`
	TotalCompletedRansomOrders int64            `json:"total_completed_ransom_orders"`
	TotalCompletedRentOrders   int64            `json:"total_completed_rent_orders"`
	TotalVehiclesStatuses      map[string]int64 `json:"total_vehicles_statuses"`
	TotalNewAndCompletedOrders int64            `json:"total_new_and_completed_orders"`
}
