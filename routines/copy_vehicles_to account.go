package routines

import (
	"errors"

	db "gitlab.com/felkis60/vehicles-service/database"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
)

func CopyVehiclesToAccount(fromAccount string, toAccount string, first_name, last_name string) error {

	var mainAccount bt.Account
	var destAccount bt.Account

	if result := db.Db.Find(&mainAccount, "name ILIKE ?", fromAccount); result.Error != nil {
		return result.Error
	} else if result.RowsAffected <= 0 {
		return errors.New("ERROR: no such fromAccount(" + fromAccount + ")")
	}

	if result := db.Db.Find(&destAccount, "name ILIKE ?", toAccount); result.Error != nil {
		return result.Error
	} else if result.RowsAffected <= 0 {
		return errors.New("ERROR: no such toAccount(" + toAccount + ")")
	}

	var tempUser bt.User
	if result := db.Db.Find(&tempUser, "first_name ILIKE ? AND last_name ILIKE ?", first_name, last_name); result.Error != nil {
		return result.Error
	} else if result.RowsAffected <= 0 {
		return errors.New("ERROR: no such user(" + first_name + " " + last_name + ")")
	}

	var tempRansomOrders []bt.RansomOrder
	if result := db.Db.Model(&bt.RansomOrder{}).Find(&tempRansomOrders, "users_id = ?", tempUser.ID); result.Error != nil {
		return result.Error
	}

	var tempVehiclesIDs []int64
	for i := range tempRansomOrders {
		tempVehiclesIDs = append(tempVehiclesIDs, tempRansomOrders[i].VehiclesID)
	}

	var allVehicles []bt.Vehicle
	if result := db.Db.Model(&bt.Vehicle{}).Find(&allVehicles, "id in (?)", tempVehiclesIDs); result.Error != nil {
		return result.Error
	}

	for i := range allVehicles {
		var existingVehicle bt.Vehicle
		if result := db.Db.Find(&existingVehicle, "accounts_id = ? AND ext_id = ? AND number = ?", destAccount.ID, allVehicles[i].ID, allVehicles[i].Number); result.RowsAffected <= 0 {
			allVehicles[i].AccountsID = destAccount.ID
			allVehicles[i].ExtID = allVehicles[i].ID
			allVehicles[i].ID = 0
			db.Db.Save(&allVehicles[i])
		}
	}

	return nil
}
