package models

import (
	it "gitlab.com/felkis60/vehicles-service/types/input"
)

type RexUser struct {
	ID         int64  `json:"id"`
	ExtID      int64  `json:"ext_id"`
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
	MiddleName string `json:"middle_name"`
	Phone      string `json:"phone"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
	AchivedAt  string `json:"achive_at"`
}

func (from *RexUser) ToInputCreateUser(to *it.InputCreateUser) {
	to.ExtID = from.ID
	to.FirstName = from.FirstName
	to.MiddleName = from.MiddleName
	to.LastName = from.LastName
	to.Phone = from.Phone
	to.CreatedAt = &from.CreatedAt

}
