package models

import (
	"time"
)

type InputCreateRansomOrder struct {
	ExtID                       int64      `json:"ext_id"`
	UsersUID                    string     `json:"users_uid"`
	VehiclesUID                 string     `json:"vehicles_uid"`
	TariffsUID                  string     `json:"tariffs_uid"`
	ManagersUID                 string     `json:"managers_uid"`
	ContractDate                time.Time  `json:"contract_date"`
	ContractDateEnd             *time.Time `json:"contract_date_end"`
	ContractDateEndDescription  *string    `json:"contract_date_end_description"`
	IsActive                    bool       `json:"is_active"`
	IsMandatoryPaymentsIncluded bool       `json:"is_mandatory_payments_included"`
	IsInitialPay                bool       `json:"is_initial_pay"`
	WriteOffPriority            []string   `json:"write_off_priority"`
	DepositPayout               float64    `json:"deposit_payout"`
	DKPAmount                   float64    `json:"dkp_amount"`
	CreatedAt                   *string    `json:"created_at"`
	AdditionalEquipment         float64    `json:"additional_equipment"`
}

type InputEditRansomOrder struct {
	ExtID                       *int64     `json:"ext_id"`
	UsersUID                    *string    `json:"users_uid"`
	VehiclesUID                 *string    `json:"vehicles_uid"`
	TariffsUID                  *string    `json:"tariffs_uid"`
	ManagersUID                 *string    `json:"managers_uid"`
	ContractDate                *time.Time `json:"contract_date"`
	ContractDateEnd             *time.Time `json:"contract_date_end"`
	ContractDateEndDescription  *string    `json:"contract_date_end_description"`
	IsActive                    *bool      `json:"is_active"`
	IsMandatoryPaymentsIncluded *bool      `json:"is_mandatory_payments_included"`
	IsInitialPay                *bool      `json:"is_initial_pay"`
	WriteOffPriority            *[]string  `json:"write_off_priority"`
	DepositPayout               *float64   `json:"deposit_payout"`
	DKPAmount                   *float64   `json:"dkp_amount"`
	AdditionalEquipment         *float64   `json:"additional_equipment"`
	VehicleStatus               *string    `json:"vehicle_status"`
}

type InputContributeIntoRansomOrder struct {
	Date               string  `json:"date" example:"YYYY-MM-DD"`
	ContributionAmount float64 `json:"contribution_amount"`
}
