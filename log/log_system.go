package log

import (
	"log"
	"os"
)

func StartLogs() {

	if os.Getenv("log_file") == "true" {
		file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
		log.SetOutput(file)
	}
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	if os.Getenv("INFOLOG") == "true" {
		PrintInfo = true
	}
}

const (
	ERR                      = "ERROR: %v"
	ERREnvLoad               = "ERROR: Failed to load .env file"
	ERRDbConnect             = "ERROR: Failed to connect database: %v"
	ERRAutoMigrate           = "ERROR: Failed to automigrate: %v"
	ERRListen                = "ERROR: Failed to listen: %v"
	ERRServe                 = "ERROR: Failed to serve: %v"
	ERRTokenReq              = "ERROR: Account Token is required"
	ERRVehicleUIDReq         = "ERROR: 'vehicles_uid' is required"
	ERRUserUIDReq            = "ERROR: 'users_uid' is required"
	ERRManagerUIDReq         = "ERROR: 'managers_uid' is required"
	ERRTariffUIDReq          = "ERROR: 'tariffs_uid' is required"
	ERRUIDReq                = "ERROR: 'uid' is required"
	ERRPhoneReq              = "ERROR: 'phone' is required"
	ERROrderUIDReq           = "ERROR: 'order_uid' is required"
	ErrNameReq               = "ERROR: 'name' is required"
	ErrFirstNameReq          = "ERROR: 'first_name' is required"
	ErrLastNameReq           = "ERROR: 'last_name' is required"
	ErrEmailReq              = "ERROR: 'email' is required"
	ERRAutoNumber            = "ERROR: 'number' is required"
	ERRAutoModel             = "ERROR: 'model' is required"
	ERRAutoMark              = "ERROR: 'mark' is required"
	ERRTextReq               = "ERROR: 'text' is required"
	ERRDateReq               = "ERROR: 'date' is required"
	ERRNoSuchToken           = "ERROR: No such Account Token in Vehicles Service"
	ERRNoSuchVehiclesNumber  = "ERROR: No such Vehicle Number"
	ERRNoSuchVehiclesUID     = "ERROR: No such Vehicle UID"
	ERRNoSuchOrdersUID       = "ERROR: No such Order UID"
	ERRNoSuchUsersUID        = "ERROR: No such User UID"
	ERRNoSuchTariffsUID      = "ERROR: No such Tariff UID"
	ERRNoSuchManagersUID     = "ERROR: No such Manager UID"
	ERRNoSuchRemindersID     = "ERROR: No such Reminder ID"
	ERRNoSuchUID             = "ERROR: No such 'uid'"
	ERRAttachedData          = "ERROR: this data is attached to some active order and cannot be deleted"
	ERRTokenIsOccupied       = "ERROR: Token is occupied"
	ERRWrongTariff           = "ERROR: Wrong tariff for another car"
	ERRExistingPhoNum        = "ERROR: User with this phone number already exists"
	ERRManagerExistingPhoNum = "ERROR: Manager with this phone number already exists"
	ERRRentTariffIntersect   = "ERROR: Rent Tariff intersects with other tariffs"
	ERRRansomOrderIntersect  = "ERROR: Ransom Order intersects with other orders"
	ERRNoUserInOrder         = "ERROR: no User in Order"
	ERRNoManagerInOrder      = "ERROR: no Manager in Order"
	ERRNoTariffInOrder       = "ERROR: no Tariff in Order"
	ERRNoVehicleInOrder      = "ERROR: no Vehicle in Order"

	ERRBusyVehicle            = "ERROR: Vehicle is busy at the date"
	ERRWrongStatus            = "ERROR: Wrong 'status'"
	INF                       = "INFO: %v"
	INFReceived               = "INFO: Received: %v"
	INFStartServer            = "INFO: %v server starts"
	INFEndServer              = "INFO: %v server ends"
	INFFuncStart              = "INFO: Function %v start"
	INFFuncEnd                = "INFO: Function %v end"
	INFVehiclesSyncedREX      = "INFO: Vehicles from rex is synced"
	INFRansomTariffsSyncedREX = "INFO: Ransom Tariffs from rex is synced"
	INFRansomOrdersSyncedREX  = "INFO: Ransom Orders from rex is synced"
	INFUsersSyncedREX         = "INFO: Users from rex is synced"
)

var (
	PrintInfo = false
)
