package routines

import (
	"log"
	"strconv"
	"time"

	db "gitlab.com/felkis60/vehicles-service/database"
	logs "gitlab.com/felkis60/vehicles-service/log"
	p "gitlab.com/felkis60/vehicles-service/providers"
	"gitlab.com/felkis60/vehicles-service/repository"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"
)

func syncRansomOrders(accountID int64, managerUID string) {

	data, err := p.RexProvider.RansomOrderList()
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Print("INFO: Ransom Orders recived: " + strconv.Itoa(len(*data)))

	var tempCreated = 0
	var tempRep = repository.RansomOrderRepository{Db: db.Db}
	for _, order := range *data {
		var tempExistingOrder bt.RentOrder
		if result := db.Db.Find(&tempExistingOrder, "ext_id = ? AND accounts_id = ?", order.ID, accountID); result.RowsAffected != 0 {
			continue
		}

		var tempInput it.InputCreateRansomOrder
		tempInput.CreatedAt = &order.CreatedAt
		tempInput.ExtID = order.ID
		tempInput.IsActive = true
		tempContratDate, err := time.Parse("2006-01-02 15:04:05", order.ContractDate)
		if err != nil {
			log.Print(err)
		}
		tempInput.ContractDate = tempContratDate
		if order.ContractEndAt != nil {
			tempContratDateEnd, err := time.Parse("2006-01-02 15:04:05", *order.ContractEndAt)
			if err != nil {
				log.Print(err)
			}
			tempInput.ContractDateEnd = &tempContratDateEnd
			tempInput.IsActive = false
		}
		tempInput.ContractDateEndDescription = &order.ContractEndDescription

		var tempVehicle bt.Vehicle
		if order.VehiclesID != 0 {
			if result := db.Db.Find(&tempVehicle, "(ext_id = ? OR ? = ANY(other_ext_ids)) AND accounts_id = ?", order.VehiclesID, order.VehiclesID, accountID); result.RowsAffected > 0 {
				tempInput.VehiclesUID = tempVehicle.UID
			}
		}

		if order.RentUsersID != 0 {
			var tempUser bt.User
			if result := db.Db.Find(&tempUser, "ext_id = ? AND accounts_id = ?", order.RentUsersID, accountID); result.RowsAffected > 0 {
				tempInput.UsersUID = tempUser.UID
			}
		}

		var tempTariff bt.RansomTariff
		if result := db.Db.First(&tempTariff, "vehicles_id = ? AND accounts_id = ?", tempVehicle.ID, accountID); result.RowsAffected > 0 {
			tempInput.TariffsUID = tempTariff.UID
		}

		tempInput.ManagersUID = managerUID

		if _, err := tempRep.Create(&tempInput, accountID, 0); err != nil {
			log.Print(err)
		} else {
			tempCreated++
		}

	}
	log.Print("INFO: Ransom Orders created: " + strconv.Itoa(tempCreated))
	log.Println(logs.INFRansomOrdersSyncedREX)
}
