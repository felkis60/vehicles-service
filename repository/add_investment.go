package repository

import (
	"errors"
	"log"
	"time"

	"gitlab.com/felkis60/vehicles-service/helpers/history"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

const (
	addInvestmentTableName = "add_investments"
)

type AddInvestmentRepository struct {
	Db *gorm.DB
}

func (rep *AddInvestmentRepository) write(data *bt.AddInvestment, input *it.InputAddInvestment) error {
	if input.Title != nil {
		data.Title = *input.Title
	}
	if input.Type != nil {
		data.Type = *input.Type
	}
	if input.Amount != nil {
		data.Amount = input.Amount
	}
	if input.DayAmount != nil {
		data.DayAmount = input.DayAmount
	}
	if input.Description != nil {
		data.Description = *input.Description
	}
	if input.RelationsID != nil {
		data.RelationsID = *input.RelationsID
	}
	if input.RelationsTable != nil {
		data.RelationsTable = *input.RelationsTable
	}
	if input.Date != nil {
		tempTime, err := time.Parse("2006-01-02", *input.Date)
		if err != nil {
			return err
		}
		data.Date = tempTime
	}
	if input.DateEnd != nil {
		tempTime, err := time.Parse("2006-01-02", *input.DateEnd)
		if err != nil {
			return err
		}
		data.DateEnd = &tempTime
	}
	return nil
}

func (rep *AddInvestmentRepository) check(data *bt.AddInvestment, accountToken string) error {

	if data.RelationsID <= 0 {
		return errors.New("ERROR: wrong relations_id")
	}

	switch data.RelationsTable {
	case "ransom_order":
		var ransRep = RansomOrderRepository{Db: rep.Db}
		order, err := ransRep.GetByID(data.RelationsID, accountToken, true, false)
		if err != nil {
			return err
		}

		if order.ContractDateEnd != nil {
			if data.Date.Before(order.ContractDate) || data.Date.After(*order.ContractDateEnd) {
				return errors.New("ERROR: wrong date")
			}
		} else if order.RansomTariff != nil {
			if data.Date.Before(order.ContractDate) || data.Date.After(order.ContractDate.AddDate(0, 0, int(float64(order.RansomTariff.Term)*30.4167))) {
				return errors.New("ERROR: wrong date")
			}
		} else {
			return errors.New("ERROR: order without tariff, can't calculate date")
		}

	case "rent_order":
		var rentRep = RentOrderRepository{Db: rep.Db}
		order, err := rentRep.GetByID(data.RelationsID, accountToken, false, false)
		if err != nil {
			return err
		}
		if data.Date.Before(order.DateStart) || data.Date.After(order.DateEnd) {
			return errors.New("ERROR: wrong date")
		}

	default:
		return errors.New("ERROR: wrong relations_table")

	}
	if data.Amount != nil && *data.Amount <= 0 {
		return errors.New("ERROR: wrong amount")
	}
	if data.DayAmount != nil && *data.DayAmount <= 0 {
		return errors.New("ERROR: wrong day_amount")
	}

	return nil
}

func (rep *AddInvestmentRepository) Create(input *it.InputAddInvestment, accountToken string, authUserID int64) (*bt.AddInvestment, error) {

	var accRep = AccountRepository{Db: rep.Db}
	var account bt.Account
	if err := accRep.Get(&account, accountToken); err != nil {
		return nil, err
	}

	var data bt.AddInvestment

	data.AccountsID = account.ID
	data.UID = uuid.Must(uuid.NewV4()).String()

	if err := rep.write(&data, input); err != nil {
		return nil, err
	}

	if err := rep.check(&data, accountToken); err != nil {
		return nil, err
	}

	if result := rep.Db.Create(&data); result.Error != nil {
		return nil, result.Error
	}

	if err := history.CreateHistory(data.ID, addInvestmentTableName, "create", nil, &data, authUserID, account.ID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return &data, nil

}

func (rep *AddInvestmentRepository) Edit(uid string, accountToken string, input *it.InputAddInvestment, authUserID int64) (*bt.AddInvestment, error) {

	data, err := rep.GetByUID(uid, accountToken)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	if err := rep.write(data, input); err != nil {
		return nil, err
	}
	if err := rep.check(data, accountToken); err != nil {
		return nil, err
	}

	if result := rep.Db.Save(data); result.Error != nil {
		return nil, result.Error
	}

	if err := history.CreateHistory(data.ID, addInvestmentTableName, "edit", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil

}

func (rep *AddInvestmentRepository) Delete(uid string, accountToken string, authUserID int64) (*bt.AddInvestment, error) {

	data, err := rep.GetByUID(uid, accountToken)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	if result := rep.Db.Delete(data); result.Error != nil {
		return nil, err
	}

	if err := history.CreateHistory(data.ID, addInvestmentTableName, "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil

}

func (rep *AddInvestmentRepository) GetByUID(UID string, accountToken string) (*bt.AddInvestment, error) {
	var data bt.AddInvestment
	if result := helpers.DbAccountLeftJoin(addInvestmentTableName, accountToken, rep.Db).Find(&data, "uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New("ERROR: no such AddInvestment UID")
	}

	return &data, nil
}

func (rep *AddInvestmentRepository) listHelper(input *it.AddInvestmentFilters, accountToken string, trueTx *gorm.DB) (*gorm.DB, error) {

	if trueTx == nil {
		trueTx = helpers.DbAccountLeftJoin(addInvestmentTableName, accountToken, rep.Db.Model(&bt.AddInvestment{}))
	} else {
		trueTx = helpers.DbAccountLeftJoin(addInvestmentTableName, accountToken, trueTx)
	}

	if input.RelationsID != nil {
		trueTx = trueTx.Where("add_investments.relations_id = ?", *input.RelationsID)
	}
	if input.RelationsTable != nil {
		trueTx = trueTx.Where("add_investments.relations_table = ?", *input.RelationsTable)

		if input.RelationsUID != nil {
			switch *input.RelationsTable {
			case "ransom_order":
				trueTx = trueTx.Joins("LEFT JOIN ransom_orders AS ro ON ro.id = add_investments.relations_id").Where("ro.uid = ?", *input.RelationsUID)

			case "rent_order":
				trueTx = trueTx.Joins("LEFT JOIN rent_orders AS ro ON ro.id = add_investments.relations_id").Where("ro.uid = ?", *input.RelationsUID)

			default:
				return nil, errors.New("ERROR: wrong table")

			}
		}
	}

	trueTx = trueTx.Where("add_investments.deleted_at IS NULL")

	return trueTx, nil
}

func (rep *AddInvestmentRepository) List(accountToken string, input *it.InputGetListAddInvestment) (*helpers.Pagination, error) {

	tx, err := rep.listHelper(&input.Filters, accountToken, nil)
	if err != nil {
		return nil, err
	}

	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.AddInvestment
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, addInvestmentTableName, &data)(tx)

}

func (rep *AddInvestmentRepository) Search(accountToken string, input *it.AddInvestmentFilters) (*[]bt.AddInvestment, error) {

	tx, err := rep.listHelper(input, accountToken, nil)
	if err != nil {
		return nil, err
	}

	var data []bt.AddInvestment
	if result := tx.Find(&data); result.Error != nil {
		return nil, result.Error
	}

	return &data, nil

}

func (rep *AddInvestmentRepository) History(UID string, accountToken string, input *it.InputGetHistoryList) (*helpers.Pagination, error) {

	//Getting base model data
	dataByUID, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return nil, err
	}

	//Filters
	tx := rep.Db.Model(&bt.History{}).Where("source_table = ? AND source_id = ?", addInvestmentTableName, dataByUID.ID)

	if input.Filters.OperationType != nil {
		tx = tx.Where("operation_type = ?", *input.Filters.OperationType)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.History
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, "histories", &data)(tx)
}
