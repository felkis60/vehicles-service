#!/bin/bash

docker build -f dockerfile.rest -t vehicles-service-rest_stage . --network=host
docker build -f dockerfile.grpc -t vehicles-service-grpc_stage . --network=host
docker stop vehicles-service-rest_stage || true
docker stop vehicles-service-grpc_stage || true
docker rm vehicles-service-rest_stage || true
docker rm vehicles-service-grpc_stage || true
docker run --network host -d -p 6312:6312 -v `pwd`:/srv/app --name vehicles-service-rest_stage vehicles-service-rest_stage
docker run --network host -d -p 6313:6313 -v `pwd`:/srv/app --name vehicles-service-grpc_stage vehicles-service-grpc_stage