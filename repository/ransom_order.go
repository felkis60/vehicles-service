package repository

import (
	"encoding/json"
	"errors"
	"log"
	"time"

	"gitlab.com/felkis60/vehicles-service/helpers/history"
	logs "gitlab.com/felkis60/vehicles-service/log"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

const (
	ransomOrderTableName = "ransom_orders"
)

type RansomOrderRepository struct {
	Db *gorm.DB
}

func (rep *RansomOrderRepository) findAllUIDs(data *bt.RansomOrder, vehUID, userUID, managerUID, tariffUID *string, accountToken string) error {
	if vehUID != nil {
		var vehRep = VehicleRepository{Db: rep.Db}
		vehicle, err := vehRep.GetByUID(*vehUID, accountToken, false)
		if err != nil {
			return err
		}
		data.VehiclesID = vehicle.ID
		data.VehicleUID = &vehicle.UID
	}

	if userUID != nil {
		var userRep = UserRepository{Db: rep.Db}
		user, err := userRep.GetByUID(*userUID, accountToken, false)
		if err != nil {
			return err
		}
		data.UsersID = user.ID
		data.UserUID = &user.UID
	}

	if managerUID != nil {
		var managerRep = ManagerRepository{Db: rep.Db}
		manager, err := managerRep.GetByUID(*managerUID, accountToken)
		if err != nil {
			return err
		}
		data.ManagersID = manager.ID
	}

	if tariffUID != nil {
		var ranTarRep = RansomTariffRepository{Db: rep.Db}
		ransomTariff, err := ranTarRep.GetByUID(*tariffUID, accountToken, false)
		if err != nil {
			return err
		}
		data.TariffsID = ransomTariff.ID
	}
	return nil
}

func (rep *RansomOrderRepository) checkOrder(data *bt.RansomOrder, accountToken string) error {

	if data.VehiclesID <= 0 {
		return errors.New(logs.ERRNoVehicleInOrder)
	}
	if data.UsersID <= 0 {
		return errors.New(logs.ERRNoUserInOrder)
	}
	if data.ManagersID <= 0 {
		return errors.New(logs.ERRNoManagerInOrder)
	}
	if data.TariffsID <= 0 {
		return errors.New(logs.ERRNoTariffInOrder)
	}

	if data.ContractDateEnd != nil && data.ContractDate.After(*data.ContractDateEnd) {
		return errors.New("ERROR: 'contract_date_end' must be greater than 'contract_date'")
	}

	{
		var tempTariff bt.RansomTariff
		rep.Db.Find(&tempTariff, "id = ?", data.TariffsID)
		if tempTariff.VehiclesID == 0 || data.VehiclesID == 0 {
			return errors.New(logs.ERRWrongTariff)
		}
		if tempTariff.VehiclesID != data.VehiclesID {
			return errors.New(logs.ERRWrongTariff)
		}
	}

	if data.IsActive {
		var activeRansOrder bt.RansomOrder
		if result := helpers.DbAccountLeftJoin("ransom_orders", accountToken, rep.Db.Model(&bt.RansomOrder{})).Find(&activeRansOrder, "ransom_orders.is_active = 'true' AND ransom_orders.vehicles_id = ? AND ransom_orders.id != ?", data.VehiclesID, data.ID); result.RowsAffected > 0 {
			return errors.New("ERROR: active Ransom Order is already exists")
		}
	}
	return nil
}

func (rep *RansomOrderRepository) addData(data *bt.RansomOrder, user, vehicle, tariff bool) {
	if user {
		var addData bt.User
		if result := rep.Db.Find(&addData, "id = ? AND deleted_at is NULL", data.UsersID); result.Error == nil {
			data.User = &addData
		}
	}
	if vehicle {
		var addData bt.Vehicle
		if result := rep.Db.Find(&addData, "id = ? AND deleted_at is NULL", data.VehiclesID); result.Error == nil {
			data.Vehicle = &addData
		}
	}
	if tariff {
		var addData bt.RansomTariff
		if result := rep.Db.Find(&addData, "id = ? AND deleted_at is NULL", data.TariffsID); result.Error == nil {
			data.RansomTariff = &addData
		}
	}
}

func (rep *RansomOrderRepository) Create(input *it.InputCreateRansomOrder, accountID int64, authUserID int64) (*bt.RansomOrder, error) {

	var data bt.RansomOrder

	var account bt.Account
	rep.Db.First(&account, accountID)

	if err := rep.findAllUIDs(&data, &input.VehiclesUID, &input.UsersUID, &input.ManagersUID, &input.TariffsUID, account.Token); err != nil {
		return nil, err
	}

	data.UID = uuid.Must(uuid.NewV4()).String()
	data.ExtID = input.ExtID
	data.AccountsID = accountID
	data.ContractDate = input.ContractDate
	data.ContractDateEndDescription = input.ContractDateEndDescription
	data.ContractDateEnd = input.ContractDateEnd
	data.IsActive = input.IsActive
	data.IsInitialPay = input.IsInitialPay
	data.IsMandatoryPaymentsIncluded = input.IsMandatoryPaymentsIncluded
	data.DepositPayout = input.DepositPayout
	data.DKPAmount = input.DKPAmount
	data.AdditionalEquipment = input.AdditionalEquipment

	if input.CreatedAt != nil {
		if tempCreatedAt, err := time.Parse("2006-01-02 15:04:05", *input.CreatedAt); err == nil {
			data.CreatedAt = tempCreatedAt
		} else {
			log.Print(err)
		}
	}

	WriteOffPriorityByte, err := json.Marshal(input.WriteOffPriority)
	if err != nil {
		return nil, err
	}
	data.WriteOffPriority = datatypes.JSON(WriteOffPriorityByte)

	if err = rep.checkOrder(&data, account.Token); err != nil {
		return nil, err
	}

	var tempRansomTariffs bt.RansomTariff
	if result := rep.Db.Find(&tempRansomTariffs, "id = ?", data.TariffsID); result.RowsAffected > 0 {
		data.RansomTariff = &tempRansomTariffs
	}

	var tempUser bt.User
	if result := rep.Db.Find(&tempUser, "id = ?", data.UsersID); result.RowsAffected > 0 {
		data.User = &tempUser
	}

	var tempVehicle bt.Vehicle
	if result := rep.Db.Find(&tempVehicle, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
		data.Vehicle = &tempVehicle
	}

	//tools.CalculateRansomOrderDayPayment(&data, &tempRansomTariffs)

	var tx = rep.Db

	if data.IsActive {
		var vehRep = VehicleRepository{Db: rep.Db}
		vehicle, err := vehRep.GetByUID(input.VehiclesUID, account.Token, false)
		if err != nil {
			return nil, err
		}

		tx = tx.Begin()
		vehicle.Status = "RANSOM"
		if result := tx.Save(vehicle); result.Error != nil {
			tx.Rollback()
			return nil, result.Error
		}

	}

	if result := rep.Db.Create(&data); result.Error != nil {
		if data.IsActive {
			tx.Rollback()
		}
		return nil, result.Error
	}

	if data.IsActive {
		tx.Commit()
	}

	if err := history.CreateHistory(data.ID, ransomOrderTableName, "create", nil, &data, authUserID, accountID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	//if natsH.EncodedConnection != nil {
	//	if err := natsH.EncodedConnection.Publish("ransom-order-created", &data); err != nil {
	//		log.Print("ERROR: " + err.Error())
	//	}
	//} else {
	//	log.Print("INFO: No nats connection. No bill for ransom order")
	//}

	return &data, nil
}

func (rep *RansomOrderRepository) Edit(uid string, input *it.InputEditRansomOrder, accountToken string, authUserID int64) (*bt.RansomOrder, error) {

	data, err := rep.GetByUID(uid, accountToken, false, false)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	if err := rep.findAllUIDs(data, input.VehiclesUID, input.UsersUID, input.ManagersUID, input.TariffsUID, accountToken); err != nil {
		return nil, err
	}

	if input.ContractDate != nil {
		data.ContractDate = *input.ContractDate
	}

	if input.ContractDateEnd != nil {
		if input.ContractDateEnd.IsZero() {
			data.ContractDateEnd = nil
		} else {
			data.ContractDateEnd = input.ContractDateEnd
		}
	}

	if input.ExtID != nil {
		data.ExtID = *input.ExtID
	}
	if input.ContractDateEndDescription != nil {
		data.ContractDateEndDescription = input.ContractDateEndDescription
	}
	if input.ExtID != nil {
		data.ExtID = *input.ExtID
	}
	if input.IsActive != nil {
		data.IsActive = *input.IsActive
	}
	if input.IsInitialPay != nil {
		data.IsInitialPay = *input.IsInitialPay
	}
	if input.IsMandatoryPaymentsIncluded != nil {
		data.IsMandatoryPaymentsIncluded = *input.IsMandatoryPaymentsIncluded
	}
	if input.DepositPayout != nil {
		data.DepositPayout = *input.DepositPayout
	}
	if input.DKPAmount != nil {
		data.DKPAmount = *input.DKPAmount
	}
	if input.AdditionalEquipment != nil {
		data.AdditionalEquipment = *input.AdditionalEquipment
	}
	if input.VehicleStatus != nil {
		if CheckVehicleStatus(*input.VehicleStatus) {
			var vehicleFromOrder bt.Vehicle
			if result := rep.Db.Find(&vehicleFromOrder, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
				vehicleFromOrder.Status = *input.VehicleStatus
				rep.Db.Save(&vehicleFromOrder)
			}
		}
	}

	if input.WriteOffPriority != nil {
		WriteOffPriorityByte, err := json.Marshal(input.WriteOffPriority)
		if err != nil {
			return nil, err
		}
		data.WriteOffPriority = datatypes.JSON(WriteOffPriorityByte)
	}

	if err = rep.checkOrder(data, accountToken); err != nil {
		return nil, err
	}

	var tempRansomTariffs bt.RansomTariff
	if result := rep.Db.Find(&tempRansomTariffs, "id = ?", data.TariffsID); result.RowsAffected > 0 {
		data.RansomTariff = &tempRansomTariffs
	}

	var tempUser bt.User
	if result := rep.Db.Find(&tempUser, "id = ?", data.UsersID); result.RowsAffected > 0 {
		data.User = &tempUser
	}

	var tempVehicle bt.Vehicle
	if result := rep.Db.Find(&tempVehicle, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
		data.Vehicle = &tempVehicle
	}

	//tools.CalculateRansomOrderDayPayment(data, &tempRansomTariffs)

	var tx = rep.Db

	if input.IsActive != nil {
		var vehRep = VehicleRepository{Db: rep.Db}
		vehicle, err := vehRep.GetByID(data.VehiclesID, accountToken, false)
		if err != nil {
			return nil, err
		}

		tx = tx.Begin()
		if data.IsActive {
			vehicle.Status = "RANSOM"
		} else {
			var activeCount int64
			rep.Db.Model(&bt.RansomOrder{}).Where("ransom_orders.is_active = 'true' AND ransom_orders.vehicles_id = ?", data.VehiclesID).Count(&activeCount)
			if activeCount <= 0 {
				vehicle.Status = "FREE"
			}
		}
		if result := tx.Save(vehicle); result.Error != nil {
			tx.Rollback()
			return nil, result.Error
		}

	}

	if result := rep.Db.Save(data); result.Error != nil {
		if input.IsActive != nil {
			tx.Rollback()
		}
		return nil, result.Error
	}

	if input.IsActive != nil {
		tx.Commit()
	}

	if err := history.CreateHistory(data.ID, ransomOrderTableName, "edit", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil
}

func (rep *RansomOrderRepository) Delete(uid string, accountToken string, authUserID int64) (*bt.RansomOrder, error) {
	data, err := rep.GetByUID(uid, accountToken, false, false)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	rep.Db.Delete(data)

	if err := history.CreateHistory(data.ID, ransomOrderTableName, "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil
}

func (rep *RansomOrderRepository) GetByID(ID int64, accountToken string, withNestedData bool, withAuto bool) (*bt.RansomOrder, error) {
	var data bt.RansomOrder
	if result := helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, rep.Db).Find(&data, "ransom_orders.id = ? AND ransom_orders.deleted_at IS NULL", ID); result.RowsAffected == 0 {
		return nil, errors.New("ERROR: No such Ransom Order ID")
	}

	if withNestedData {

		var tempRansomTariffs bt.RansomTariff
		if result := rep.Db.Find(&tempRansomTariffs, "id = ?", data.TariffsID); result.RowsAffected > 0 {
			data.RansomTariff = &tempRansomTariffs
		}

		var tempUser bt.User
		if result := rep.Db.Find(&tempUser, "id = ?", data.UsersID); result.RowsAffected > 0 {
			data.User = &tempUser
		}

		var tempManager bt.Manager
		if result := rep.Db.Find(&tempManager, "id = ?", data.ManagersID); result.RowsAffected > 0 {
			data.Manager = &tempManager
		}

		if withAuto {
			var tempVehicle bt.Vehicle
			if result := rep.Db.Find(&tempVehicle, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
				data.Vehicle = &tempVehicle
			}
		}

	}

	return &data, nil
}

func (rep *RansomOrderRepository) GetByUID(UID string, accountToken string, withNestedData bool, withAuto bool) (*bt.RansomOrder, error) {
	var data bt.RansomOrder
	if result := helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, rep.Db).Find(&data, "ransom_orders.uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchOrdersUID)
	}

	if withNestedData {

		var tempRansomTariffs bt.RansomTariff
		if result := rep.Db.Find(&tempRansomTariffs, "id = ?", data.TariffsID); result.RowsAffected > 0 {
			data.RansomTariff = &tempRansomTariffs
		}

		var tempUser bt.User
		if result := rep.Db.Find(&tempUser, "id = ?", data.UsersID); result.RowsAffected > 0 {
			data.User = &tempUser
		}

		var tempManager bt.Manager
		if result := rep.Db.Find(&tempManager, "id = ?", data.ManagersID); result.RowsAffected > 0 {
			data.Manager = &tempManager
		}

		if withAuto {
			var tempVehicle bt.Vehicle
			if result := rep.Db.Find(&tempVehicle, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
				data.Vehicle = &tempVehicle
			}
		}

	}

	return &data, nil
}

func (rep *RansomOrderRepository) listHelper(filters *it.RansomOrderFilters, accountToken string, trueTx *gorm.DB, withJoinFilters bool) (*gorm.DB, error) {

	if trueTx == nil {
		trueTx = helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, rep.Db.Model(&bt.RansomOrder{}))
	} else {
		trueTx = helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, trueTx)
	}

	if filters.Type != nil {
		switch *filters.Type {
		case "active":
			trueTx = trueTx.Where("ransom_orders.contract_date_end IS NULL")
		case "contractEnd":
			trueTx = trueTx.Where("ransom_orders.contract_date_end IS NOT NULL")
		}
	}

	if filters.IsActive != nil {
		trueTx = trueTx.Where("ransom_orders.is_active = ?", *filters.IsActive)
	}

	if filters.UsersPhone != nil {
		var userRep = UserRepository{Db: rep.Db}
		user, err := userRep.GetByPhone(accountToken, *filters.UsersPhone)
		if err != nil {
			return nil, err
		}
		trueTx = trueTx.Where("ransom_orders.users_id = ?", user.ID)
	}

	trueTx = trueTx.Where("ransom_orders.deleted_at IS null")

	if withJoinFilters {
		if filters.Search != nil {
			trueTx = trueTx.Where("CONCAT(u.last_name, ' ', u.first_name, ' ', v.mark, ' ', v.model, ' ', v.number, ' ', u.phone) ILIKE '%" + *filters.Search + "%'")
		}
		if filters.IncludeUserUIDs != nil {
			trueTx = trueTx.Where("u.uid IN (?)", *filters.IncludeUserUIDs)
		}

		if filters.ExcludeUserUIDs != nil {
			trueTx = trueTx.Where("u.uid NOT IN (?)", *filters.ExcludeUserUIDs)
		}
	}

	return trueTx, nil
}

func (rep *RansomOrderRepository) List(accountToken string, input *it.InputGetListRansomOrders) (*helpers.Pagination, error) {

	var tx, err = rep.listHelper(&input.Filters, accountToken, nil, false)
	if err != nil {
		return nil, err
	}

	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.RansomOrder

	pagData, err := helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, ransomOrderTableName, &data)(tx)
	if err != nil {
		return nil, err
	}
	if input.AdditionalData.Tariff || input.AdditionalData.User || input.AdditionalData.Vehicle {
		for i := range data {
			rep.addData(&data[i], input.AdditionalData.User, input.AdditionalData.Vehicle, input.AdditionalData.Tariff)
		}
	}
	return pagData, err

}

func (rep *RansomOrderRepository) Search(accountToken string, input *it.InputGetListRansomOrders) (*[]map[string]interface{}, error) {

	//var default_sort_keys = []string{"id",
	//	"phone",
	//	"fullName",
	//	"vehicleFullname",
	//	"totalVehiclesAmount",
	//	"dayPayment",
	//	"contractDate",
	//	"contractEndAt",
	//	"contractEndDescription",
	//	"term",
	//	"tariffInitialFee",
	//}
	var default_sort_keys_map = map[string]string{
		"id":                     "ransom_order_id",
		"phone":                  "ransom_users_phone",
		"fullName":               "ransom_users_fullname",
		"vehicleFullname":        "vehicles_fullname",
		"totalVehiclesAmount":    "rt.total_vehicle_amount",
		"dayPayment":             "day_payment",
		"contractDate":           "ransom_orders.contract_date",
		"contractEndAt":          "contract_end_at",
		"contractEndDescription": "contract_end_description",
		"tariffInitialFee":       "initial_fee",
		"sum_of_unpaid_fines":    "sum_of_unpaid_fines",
	}

	var tx = rep.Db.Table(ransomOrderTableName)
	tx = tx.Select("*, ransom_orders.id AS ransom_order_id, ransom_orders.uid AS ransom_order_uid, ransom_orders.contract_date_end_description AS contract_end_description, u.id AS ransom_users_id, u.uid AS ransom_users_uid, v.id AS vehicles_id, v.uid AS vehicles_uid, v.number AS vehicles_number, u.phone AS ransom_users_phone, CONCAT(u.last_name, ' ', u.first_name) AS ransom_users_fullname, CONCAT(v.mark, ' ', v.model, ' ', v.number) AS vehicles_fullname, rt.day_payment AS day_payment, ransom_orders.contract_date AS contract_date, ransom_orders.contract_date_end AS contract_end_at, rt.term AS term,rt.initial_fee AS initial_fee, rt.total_vehicle_amount AS vehicle_amount, rt.month_payment AS month_payment, mp.mandatory_amount AS year_mandatory_amount, mp.contribution_amount AS year_mandatory_payout_amount, COALESCE(f.amount_to_pay_a, 0) AS sum_of_unpaid_fines")

	tx, err := rep.listHelper(&input.Filters, accountToken, tx, true)
	if err != nil {
		return nil, err
	}

	tx = tx.Joins("LEFT JOIN users AS u ON ransom_orders.users_id = u.id")
	tx = tx.Joins("LEFT JOIN ( SELECT mp.mandatory_amount, mp.contribution_amount, mp.ransom_orders_id, mp.contribution_amount_array FROM mandatory_payments AS mp WHERE (mp.contract_year_start < now() AND mp.contract_year_end > now()) GROUP BY mp.contribution_amount, mp.mandatory_amount, mp.ransom_orders_id, mp.contribution_amount_array) AS mp ON mp.ransom_orders_id = ransom_orders.id")
	tx = tx.Joins("LEFT JOIN vehicles AS v ON ransom_orders.vehicles_id = v.id")
	tx = tx.Joins("LEFT JOIN ransom_tariffs AS rt ON ransom_orders.tariffs_id = rt.id")
	tx = tx.Joins("LEFT JOIN (SELECT SUM(fines.amount_to_pay) AS amount_to_pay_a, vehicles_id AS fines_vehicles_id  FROM fines GROUP BY vehicles_id) f ON f.fines_vehicles_id = v.id")

	if input.OrderBy != "" {
		if trueSortKey, ok := default_sort_keys_map[input.OrderBy]; ok {
			tx = helpers.DbSetOrder("", trueSortKey, input.OrderDir)(tx)
		}
	}

	var data []map[string]interface{}

	if err := tx.Scan(&data).Error; err != nil {
		return nil, err
	}

	for i := range data {
		var addInvestments []map[string]interface{}
		helpers.DbAccountLeftJoin(addInvestmentTableName, accountToken, rep.Db.Model(&bt.AddInvestment{})).Where("add_investments.deleted_at is NULL AND add_investments.relations_id = ? AND add_investments.relations_table = 'ransom_order'", data[i]["ransom_order_id"]).Find(&addInvestments)
		if addInvestments != nil {
			data[i]["add_investments"] = addInvestments
		}
	}

	return &data, nil

}

func (rep *RansomOrderRepository) History(UID string, accountToken string, input *it.InputGetHistoryList) (*helpers.Pagination, error) {

	//Getting base model data
	dataByUID, err := rep.GetByUID(UID, accountToken, false, false)
	if err != nil {
		return nil, err
	}

	//Filters
	tx := rep.Db.Model(&bt.History{}).Where("source_table = ? AND source_id = ?", ransomOrderTableName, dataByUID.ID)

	if input.Filters.OperationType != nil {
		tx = tx.Where("operation_type = ?", *input.Filters.OperationType)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.History
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, "histories", &data)(tx)

}

func (rep *RansomOrderRepository) Contribute(UID string, input *it.InputContributeIntoRansomOrder, accountToken string, authUserID int64) (*[]bt.MandatoryPayment, error) {
	tempOrder, err := rep.GetByUID(UID, accountToken, false, false)
	if err != nil {
		return nil, err
	}

	var tempMandatoryPayment []bt.MandatoryPayment
	if result := rep.Db.Table("mandatory_payments").Find(&tempMandatoryPayment, "mandatory_payments.ransom_orders_id = ?", tempOrder.ID); result.Error != nil {
		return nil, result.Error
	}

	var tempMap = make(map[int]*bt.MandatoryPayment)
	for i := range tempMandatoryPayment {
		tempMap[tempMandatoryPayment[i].ContractYearStart.Year()] = &tempMandatoryPayment[i]
	}

	var tempCount = 0
	tempInputTime, err := time.Parse("2006-01-02", input.Date)
	if err != nil {
		return nil, err
	}
	if tempInputTime.Year() > 2050 {
		return nil, errors.New("ERROR: 'date' is too far")
	}

	var tempTariff bt.RansomTariff
	if tempOrder.TariffsID > 0 {
		if result := rep.Db.Find(&tempTariff, tempOrder.TariffsID); result.RowsAffected == 0 {
			return nil, errors.New("ERROR: no tariff in order")
		}
	} else {
		return nil, errors.New("ERROR: no tariff in order")
	}

	var mandatoryAmount = (tempTariff.TrackerServiceAmount*12 + tempTariff.VehicleTaxAmount + tempTariff.TrackerAmount + tempTariff.RegistrationAmount + tempTariff.InsuranceOsagoAmount + tempOrder.AdditionalEquipment)
	for i := tempOrder.ContractDate.Year(); i <= tempInputTime.Year(); i++ {
		tempMP, ok := tempMap[i]
		if !ok {
			tempMP = &bt.MandatoryPayment{MandatoryAmount: mandatoryAmount, ContractYearStart: tempOrder.ContractDate.AddDate(tempCount, 0, 0), ContractYearEnd: tempOrder.ContractDate.AddDate(tempCount+1, 0, 0), RansomOrdersID: tempOrder.ID}
		}
		if (tempInputTime.After(tempMP.ContractYearStart) || tempInputTime.Equal(tempMP.ContractYearStart)) && tempInputTime.Before(tempMP.ContractYearEnd) {
			if input.ContributionAmount >= 0 {
				tempMP.ContributionAmount = input.ContributionAmount
				var amountArray []bt.MandatoryPaymentOne
				var newAmountArray []bt.MandatoryPaymentOne
				var newAdded = false
				json.Unmarshal(tempMP.ContributionAmountArray, &amountArray)
				for j := range amountArray {
					if amountArray[j].Date.Equal(tempInputTime) {
						if input.ContributionAmount != 0 {
							newAmountArray = append(newAmountArray, bt.MandatoryPaymentOne{Date: tempInputTime, Amount: input.ContributionAmount})
						}
						newAdded = true
					} else {
						newAmountArray = append(newAmountArray, amountArray[j])
					}
				}
				if !newAdded {
					if input.ContributionAmount != 0 {
						newAmountArray = append(newAmountArray, bt.MandatoryPaymentOne{Date: tempInputTime, Amount: input.ContributionAmount})
					}
				}
				var newAmountArrayBytes, err = json.Marshal(newAmountArray)
				if err != nil {
					log.Print(err)
				}
				tempMP.ContributionAmountArray = newAmountArrayBytes
			}
		}
		tempMP.MandatoryAmount = mandatoryAmount
		rep.Db.Save(tempMP)

		tempCount++
	}

	return &tempMandatoryPayment, nil

}
