package controllers

import (
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/felkis60/vehicles-service/repository"
	_ "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"
)

type OrderController struct {
	Repository repository.OrderRepository
}

// @Summary Get orders
// @Tags Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetList true "Request body"
// @Success 200 {object} []_.Order
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/orders/not-working [post]
//func (ctrl *OrderController) List2(c *gin.Context) {
//	var input it.InputGetListOrders
//	if err := c.BindJSON(&input); err != nil {
//		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
//		return
//	}
//
//	start := time.Now()
//	pagInfo, err := ctrl.Repository.List2(c.MustGet("token").(string), &input, true)
//	log.Print(time.Since(start))
//	if err != nil {
//		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
//		return
//	}
//
//	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
//}

// @Summary Get orders
// @Tags Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListOrders true "Request body"
// @Success 200 {object} []_.Order
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/orders/list [post]
func (ctrl *OrderController) List(c *gin.Context) {
	var input it.InputGetListOrders
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	start := time.Now()
	pagInfo, err := ctrl.Repository.List2(c.MustGet("token").(string), &input, true)
	log.Print(time.Since(start))
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}
