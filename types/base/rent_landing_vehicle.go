package models

import (
	"strconv"
)

type RentLandingVehicle struct {
	ID                 int64  `json:"id"`
	VehiclesServiceUid string `json:"vehiclesServiceUid"`
	Mark               string `json:"mark"`
	Model              string `json:"model"`
	Year               int    `json:"year"`
	Color              string `json:"color"`
	Transmission       string `json:"transmission"`
	Engine             int    `json:"engine"`
	Drive              string `json:"drive"`

	Volume   int `json:"volume"`
	Rent     int `json:"rent"`
	DayPrice int `json:"dayPrice"`
}

func VehicleToRentLandingVehicle(from *Vehicle, to *RentLandingVehicle) {
	to.VehiclesServiceUid = from.UID
	to.Mark = from.Mark
	to.Model = from.Model
	to.Color = from.Color
	to.Drive = from.Drive
	to.Transmission = from.Kpp

	to.Year, _ = strconv.Atoi(from.Year)
	to.Engine, _ = strconv.Atoi(from.Engine)

}
