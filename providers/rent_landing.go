package providers

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
)

type RentLandingProviderType string

const RentProvider RentLandingProviderType = "http://prod.nord.fondo.ru:1337/"

func (provider RentLandingProviderType) Get(vehicleUID string) (*bt.RentLandingVehicle, error) {
	req, err := http.NewRequest("GET", string(provider)+"autos?vehiclesServiceUid="+vehicleUID, nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var landVehicles []bt.RentLandingVehicle
	err = json.Unmarshal(body, &landVehicles)
	if err != nil {
		return nil, err
	}

	var landVehicle bt.RentLandingVehicle
	if len(landVehicles) <= 0 {
		return nil, nil
	}

	landVehicle = landVehicles[0]
	return &landVehicle, nil
}

func (provider RentLandingProviderType) Edit(vehicle *bt.RentLandingVehicle) error {

	body, err := json.Marshal(vehicle)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("PUT", string(provider)+"autos/"+strconv.FormatUint(uint64(vehicle.ID), 10), bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return err
	}

	_, err = http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	return nil
}

func (provider RentLandingProviderType) Create(vehicle *bt.RentLandingVehicle) error {
	body, err := json.Marshal(vehicle)
	if err != nil {
		return err
	}

	_, err = http.Post(string(provider)+"autos/", "application/json", bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	return nil
}
