package database

import (
	"log"
	"os"
	"time"

	l "gitlab.com/felkis60/vehicles-service/log"
	bt "gitlab.com/felkis60/vehicles-service/types/base"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var Db *gorm.DB

func Start() {

	dsn := "host=" + os.Getenv("DBHOST") +
		" user=" + os.Getenv("DBUSER") +
		" password=" + os.Getenv("DBPASSWORD") +
		" dbname=" + os.Getenv("DBNAME") +
		" port=" + os.Getenv("DBPORT") +
		" sslmode=" + os.Getenv("DBSSLMODE") +
		" TimeZone=" + os.Getenv("DBTIMEZONE")

	newLogger := logger.New(
		log.New(log.Writer(), "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Millisecond * 300,
			LogLevel:                  logger.Error,
			IgnoreRecordNotFoundError: true,
			Colorful:                  false,
		},
	)

	var err error
	Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{Logger: newLogger})

	if err != nil {
		log.Fatalf(l.ERRDbConnect, err.Error())
	}
}

func Migrate() {

	err := Db.AutoMigrate(&bt.History{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.Account{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.User{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.Vehicle{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.RentTariff{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.RentOrder{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.RansomOrder{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.RansomTariff{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.Manager{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.Reminder{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.MandatoryPayment{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.AddInvestment{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.Fine{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

}

func DbDateTimeIntersect(checkStart time.Time, checkEnd *time.Time, db *gorm.DB, startField, endField string) *gorm.DB {

	if ((checkEnd != nil && checkEnd.IsZero()) || checkEnd == nil) && checkStart.IsZero() {
		return db
	}
	var tx1 = Db
	var tx2 = Db.Where(endField + " IS NULL")

	if checkEnd != nil && !checkEnd.IsZero() {
		tx1 = tx1.Where(startField+" <= ?", *checkEnd)
		tx2 = tx2.Where(startField+" <= ?"+" OR "+startField+" <= ?", checkStart, *checkEnd)
	}

	if !checkStart.IsZero() {
		tx1 = tx1.Where(endField+" >= ?", checkStart)
	}

	db = db.Where(Db.Where(tx2).Or(tx1))
	return db
}
