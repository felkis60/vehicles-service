package repository

import (
	"math"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

	log "github.com/sirupsen/logrus"

	"errors"
	"time"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

const (
	fineTableName = "fines"
)

type FineRepository struct {
	Db *gorm.DB
}

func (rep *FineRepository) ConvertFineFromSite(in *bt.FineFromSite) *bt.Fine {
	var out bt.Fine

	out.Number = in.Number
	out.BillDate = in.BillDate
	out.Amount = int(math.Round(in.Amount))
	out.AmountToPay = int(in.AmountToPay)
	out.PaymentStatus = in.PaymentStatus
	out.DiscountDate = in.DiscountDate
	out.DiscountSize = int(in.DiscountSize)
	out.ReplacedWith = in.ReplacedWith
	out.ChangeStatus = in.ChangeStatus
	out.ChangeStatusReason = in.ChangeStatusReason
	out.ViolationDate = in.ViolationDate
	out.ViolatorName = in.ViolatorName
	out.Location = in.Location
	out.Coordinates = in.Coordinates
	out.KoapCode = in.KoapCode
	out.KoapText = in.KoapText
	out.DivisionName = in.DivisionName
	out.DivisionCode = in.DivisionCode
	out.Name = in.Name
	out.Org = in.Org
	out.Vin = in.BestMatchCar.Vin
	out.Sts = in.BestMatchCar.Sts
	out.PayeeUsername = in.PayeeUsername
	out.PayeeInn = in.PayeeInn
	out.PayeeKpp = in.PayeeKpp
	out.PayeeOktmo = in.PayeeOktmo
	out.PayeeKbk = in.PayeeKbk
	out.PayeeBankName = in.PayeeBankName
	out.PayeeBankBik = in.PayeeBankBik
	out.PayeeBankAccount = in.PayeeBankAccount
	out.PayeeBankCorrAccount = in.PayeeBankCorrAccount
	out.ManualPayment = in.ManualPayment
	out.FsspIP = in.FsspIP
	out.FsspUin = in.FsspUin
	out.OriginalCreatedAt = in.CreatedAt
	out.OriginalUpdatedAt = in.UpdatedAt
	if in.ArchivedAt != nil {
		if tempTime, ok := in.ArchivedAt.(time.Time); ok {
			out.OriginalArchivedAt = &tempTime
		}
	}
	out.Comment = in.Comment
	out.CommentUpdatedAt = in.CommentUpdatedAt
	out.PaidAt = in.PaidAt

	return &out

}

func (rep *FineRepository) Create(accountToken string, input *bt.Fine, authUserID int64, ignorTG bool) (*bt.Fine, error) {

	if input == nil {
		return nil, errors.New("no input to create fine")
	}

	var account bt.Account
	if result := rep.Db.Find(&account, "accounts.token = ?", string(accountToken)); result.RowsAffected <= 0 {
		return nil, errors.New("no such token")
	}

	if fine, _ := rep.GetByNumber(accountToken, input.Number); fine != nil {
		return nil, errors.New("fine already exists")
	}

	log.Info("STS: " + input.Sts)
	var vehRep = VehicleRepository{Db: rep.Db}
	veh, err := vehRep.GetBySts(accountToken, input.Sts, false)
	if err != nil {
		return nil, err
	}

	input.UID = uuid.Must(uuid.NewV4()).String()
	input.AccountsID = account.ID
	input.VehiclesID = veh.ID

	if result := rep.Db.Create(input); result.Error != nil {
		return nil, result.Error
	}

	if !ignorTG {
		if input.PaidAt.IsZero() {
			if account.TelegramBotToken != "" && account.TelegramChatID != "" {
				bot, err := tgbotapi.NewBotAPI(account.TelegramBotToken)
				if err != nil {
					log.Warn(err.Error())
					return input, nil
				}

				bot.Debug = true

				log.Info("telegram authorized on account %s", bot.Self.UserName)

				chatID, err := strconv.ParseInt(account.TelegramChatID, 10, 64)
				if err != nil {
					log.Warn(err.Error())
					return input, nil
				}

				msg := tgbotapi.NewMessage(chatID, "Штраф под номером "+input.Number+", на машину "+veh.Mark+" "+veh.Model+" "+veh.Number+", не оплачен")
				if _, err := bot.Send(msg); err != nil {
					log.Warn(err.Error())
					return input, nil
				}
			}
		}
	}

	//if err := history.CreateHistory(input.ID, fineTableName, "create", nil, input, authUserID, account.ID); err != nil {
	//	log.Error(err.Error())
	//}

	return input, nil

}

//func (rep *FineRepository) Edit(accountToken helpers.AccountToken, input *types.InputEdit, uid string, authUserID int64) (*types.Fine, error) {
//
//	data, err := rep.GetByUID(accountToken, uid)
//	if err != nil {
//		return nil, err
//	}
//
//	var dataBefore = *data
//
//
//	rep.Db.Save(data)
//
//	if err := history.CreateHistory(data.ID, fineTableName, "edit", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
//		log.Error(err.Error())
//	}
//
//	return data, nil
//
//}

func (rep *FineRepository) Delete(accountToken string, uid string, authUserID int64) (*bt.Fine, error) {

	data, err := rep.GetByUID(accountToken, uid)
	if err != nil {
		return nil, err
	}

	//var dataBefore = *data

	rep.Db.Delete(data)

	//if err := history.CreateHistory(data.ID, fineTableName, "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
	//	log.Error(err.Error())
	//}

	return data, nil

}

func (rep *FineRepository) GetByUID(accountToken string, UID string) (*bt.Fine, error) {
	var data bt.Fine
	if result := helpers.DbAccountLeftJoin(fineTableName, accountToken, rep.Db).Find(&data, "fines.uid = ?", UID); result.Error != nil {
		return nil, result.Error
	} else if result.RowsAffected == 0 {
		return nil, errors.New("no such fine")
	}

	return &data, nil
}

func (rep *FineRepository) GetByID(accountToken string, ID int64) (*bt.Fine, error) {
	var data bt.Fine
	if result := helpers.DbAccountLeftJoin(fineTableName, accountToken, rep.Db).Find(&data, "fines.id = ?", ID); result.Error != nil {
		return nil, result.Error
	} else if result.RowsAffected == 0 {
		return nil, errors.New("no such fine")
	}

	return &data, nil
}

func (rep *FineRepository) GetByNumber(accountToken string, number string) (*bt.Fine, error) {
	var data bt.Fine
	if result := helpers.DbAccountLeftJoin(fineTableName, accountToken, rep.Db).Find(&data, "fines.number = ?", number); result.Error != nil {
		return nil, result.Error
	} else if result.RowsAffected == 0 {
		return nil, errors.New("no such fine")
	}

	return &data, nil
}

func (rep *FineRepository) List(accountToken string, input *bt.InputGetListFines) (*helpers.Pagination, error) {

	var tx = helpers.DbAccountLeftJoin(fineTableName, accountToken, rep.Db.Model(&bt.Fine{}))

	if input.Filters.VehicleNumber != nil {
		tx = tx.Joins("LEFT JOIN vehicles AS v on v.id = fines.veehicles_id").Where("v.number ILIKE ?", *input.Filters.VehicleNumber)
	}

	if input.Filters.VehicleUID != nil {
		tx = tx.Joins("LEFT JOIN vehicles AS v on v.id = fines.veehicles_id").Where("v.uid ILIKE ?", *input.Filters.VehicleUID)
	}

	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.Fine
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, fineTableName, &data)(tx)

}

func (rep *FineRepository) History(accountToken string, input *it.InputGetHistoryList, UID string) (*helpers.Pagination, error) {

	//Getting base model data
	dataByUID, err := rep.GetByUID(accountToken, UID)
	if err != nil {
		return nil, err
	}

	//Filters
	tx := rep.Db.Model(&bt.History{}).Where("source_table = ? AND source_id = ?", fineTableName, dataByUID.ID)

	if input.Filters.OperationType != nil {
		tx = tx.Where("operation_type = ?", *input.Filters.OperationType)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.History
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, "histories", &data)(tx)
}
