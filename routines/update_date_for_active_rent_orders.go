package routines

import (
	"log"
	"time"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"
	"gitlab.com/felkis60/vehicles-service/database"
	db "gitlab.com/felkis60/vehicles-service/database"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
)

func UpdateDateForActiveRentOrders() {
	var rentOrders []bt.RentOrder

	if result := db.Db.Model(&bt.RentOrder{}).Find(&rentOrders, "is_active = true"); result.Error != nil {
		log.Print(result.Error)
		return
	}
	for i := range rentOrders {
		if rentOrders[i].IsActive {
			var currentTime = time.Now()
			if currentTime.After(rentOrders[i].DateEnd) {
				rentOrders[i].DateEnd = time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), currentTime.Hour(), currentTime.Minute(), 0, 0, currentTime.Location())
				if result := db.Db.Save(&rentOrders[i]); result.Error != nil {
					log.Print(result.Error)
				} else {
					if db.RedisDB != nil {
						//[]string{rentOrders[i].User.UID, rentOrders[i].Vehicle.UID}
						if result := db.RedisDB.Del(database.RedisContext, helpers.GenerateRedisCacheKeyForSearchRentOrder(rentOrders[i].UID)); result.Err() != nil {
							log.Print(result.Err())
						}
					} else {
						log.Print("ERROR: redis is not inited for UpdateDateForActiveRentOrders routine")
					}
				}
			}
		} else {
			log.Print("ERROR: non-active order in loop")
		}
	}
}
