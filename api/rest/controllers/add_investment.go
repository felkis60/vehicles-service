package controllers

import (
	"net/http"

	"gitlab.com/felkis60/vehicles-service/repository"
	_ "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type AddInvestmentController struct {
	Repository repository.AddInvestmentRepository
}

// @Summary Create new add investment
// @Tags Add Investments
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_     body it.InputAddInvestment true "Request body"
// @Success 200 {object} _.AddInvestment
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/add_investments/ [post]
func (ctrl *AddInvestmentController) Create(c *gin.Context) {
	var input it.InputAddInvestment
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Delete add investment
// @Tags Add Investments
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "AddInvestment UID"
// @Success 200 {object} _.AddInvestment
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/add_investments/{uid}/delete [post]
func (ctrl *AddInvestmentController) Delete(c *gin.Context) {

	data, err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Edit add investment
// @Tags Add Investments
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ 	body it.InputAddInvestment true "Request body"
// @Param uid path string true "AddInvestment UID"
// @Success 200 {object} _.AddInvestment
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/add_investments/{uid}/edit [post]
func (ctrl *AddInvestmentController) Edit(c *gin.Context) {
	var input it.InputAddInvestment
	if err := c.BindJSON(&input); err != nil {
		c.String(http.StatusBadRequest, err.Error())
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), c.MustGet("token").(string), &input, 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})

}

// @Summary Get one add investment
// @Tags Add Investments
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "AddInvestment UID"
// @Success 200 {object} _.AddInvestment
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/add_investments/{uid} [post]
func (ctrl *AddInvestmentController) GetByUID(c *gin.Context) {

	data, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get list of Add Investments
// @Tags Add Investments
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListAddInvestment true "Request body"
// @Success 200 {object} []_.AddInvestment
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/add_investments/list [post]
func (ctrl *AddInvestmentController) List(c *gin.Context) {
	var input it.InputGetListAddInvestment
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}

// @Summary Get add investments
// @Tags Add Investments
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.AddInvestmentFilters true "Request body"
// @Success 200 {object} []_.AddInvestment
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/add_investments/list [post]
func (ctrl *AddInvestmentController) Search(c *gin.Context) {
	var input it.AddInvestmentFilters
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Search(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}
