package repository

import (
	"errors"
	"math"
	"time"

	logs "gitlab.com/felkis60/vehicles-service/log"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"gorm.io/gorm"
)

const (
	reminderTableName = "reminders"
)

type ReminderRepository struct {
	Db *gorm.DB
}

func (rep *ReminderRepository) checkReminder(data *bt.Reminder) error {
	if data.Text == "" {
		return errors.New(logs.ERRTextReq)
	}
	if data.Date.IsZero() {
		return errors.New(logs.ERRDateReq)
	}
	return nil
}

func (rep *ReminderRepository) Create(input *it.InputCreateReminder, accountID int64) (*bt.Reminder, error) {

	var data bt.Reminder

	var err error

	data.Text = input.Text
	data.Closed = false
	data.VehiclesID = input.VehiclesID
	data.AccountsID = accountID
	if data.Date, err = time.Parse("2006-01-02 15:04:05", input.Date); err != nil {
		return nil, err
	}

	if err := rep.checkReminder(&data); err != nil {
		return nil, err
	}

	rep.Db.Create(&data)

	return &data, nil

}

func (rep *ReminderRepository) Edit(id int64, accountToken string, input *it.InputEditReminder) (*bt.Reminder, error) {

	data, err := rep.GetByID(id, accountToken)
	if err != nil {
		return nil, err
	}

	if input.Text != nil {
		data.Text = *input.Text
	}
	if input.Closed != nil {
		data.Closed = *input.Closed
	}
	if input.VehiclesID != nil {
		data.VehiclesID = *input.VehiclesID
	}
	if input.Date != nil {
		if data.Date, err = time.Parse("2006-01-02 15:04:05", *input.Date); err != nil {
			return nil, err
		}
	}

	if err := rep.checkReminder(data); err != nil {
		return nil, err
	}

	rep.Db.Save(data)

	return data, nil

}

func (rep *ReminderRepository) Delete(id int64, accountToken string) (*bt.Reminder, error) {

	data, err := rep.GetByID(id, accountToken)
	if err != nil {
		return nil, err
	}

	rep.Db.Delete(data)

	return data, nil

}

func (rep *ReminderRepository) GetByID(ID int64, accountToken string) (*bt.Reminder, error) {
	var data bt.Reminder
	if result := helpers.DbAccountLeftJoin(reminderTableName, accountToken, rep.Db).Find(&data, "reminders.id = ?", ID); result.Error != nil {
		return nil, result.Error
	} else if result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchRemindersID)
	}
	return &data, nil
}

func (rep *ReminderRepository) List(accountToken string, input *it.InputGetListReminders) (*helpers.Pagination, error) {

	var err error
	var tx = helpers.DbAccountLeftJoin(reminderTableName, accountToken, rep.Db.Model(&bt.Reminder{}))

	if input.Filters.VehicleID != nil {
		tx = tx.Where("vehicles_id = ?", *input.Filters.VehicleID)
	}
	var tempVeh *bt.Vehicle
	if input.Filters.VehicleUID != nil {
		var tempRep = VehicleRepository{rep.Db}
		if tempVeh, err = tempRep.GetByUID(*input.Filters.VehicleUID, accountToken, false); err == nil && tempVeh != nil {
			tx = tx.Where("vehicles_id = ?", tempVeh.ID)
		}
	}

	tx = tx.Where("closed = false")

	tx = tx.Session(&gorm.Session{})

	var totalItems int64
	if result := tx.Count(&totalItems); result.Error != nil {
		return nil, result.Error
	}

	tx = helpers.DbSetOrder(reminderTableName, input.OrderBy, input.OrderDir)(tx)

	var data []bt.Reminder
	if result := helpers.PaginateManual(&input.Page, &input.PageSize, 5, 100)(tx).Scan(&data); result.Error != nil {
		return nil, result.Error
	}

	var totalPages = int(math.Ceil(float64(totalItems) / float64(input.PageSize)))

	return &helpers.Pagination{Page: input.Page, Items: &data, TotalItems: totalItems, TotalPages: totalPages}, nil
}
