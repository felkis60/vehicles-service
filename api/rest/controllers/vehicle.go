package controllers

import (
	"gitlab.com/felkis60/vehicles-service/repository"

	_ "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type VehicleController struct {
	Repository repository.VehicleRepository
}

// @Summary Create new vehicle
// @Tags Vehicles
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputCreateVehicle true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/vehicles/ [post]
func (ctrl *VehicleController) Create(c *gin.Context) {
	var input it.InputCreateVehicle
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Delete vehicle
// @Tags Vehicles
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Vehicle UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/vehicles/{uid}/delete [post]
func (ctrl *VehicleController) Delete(c *gin.Context) {

	data, err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})

}

// @Summary Edit vehicle
// @Tags Vehicles
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputEditVehicle true "Request body"
// @Param uid path string true "Vehicle UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/vehicles/{uid}/edit [post]
func (ctrl *VehicleController) Edit(c *gin.Context) {
	var input it.InputEditVehicle
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), c.MustGet("token").(string), &input, 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get one vehicle
// @Tags Vehicles
// @Description
// @Accept  json
// @Produce  json
// @Param Token header string true "Account Token"
// @Param uid path string true "Vehicle UID"
// @Success 200 {object} _.Vehicle
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/vehicles/{uid} [post]
func (ctrl *VehicleController) GetByUID(c *gin.Context) {

	vehicle, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string), false)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: vehicle})
}

// @Summary Get vehicles
// @Tags Vehicles
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListVehicles true "Request body"
// @Success 200 {object} []_.Vehicle
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/vehicles/list [post]
func (ctrl *VehicleController) List(c *gin.Context) {
	var input it.InputGetListVehicles
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}
