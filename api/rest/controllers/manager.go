package controllers

import (
	"net/http"

	"gitlab.com/felkis60/vehicles-service/repository"
	_ "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type ManagerController struct {
	Repository repository.ManagerRepository
}

// @Summary Create new manager
// @Tags Managers
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_     body it.InputCreateManager true "Request body"
// @Success 200 {object} _.Manager
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/managers/ [post]
func (ctrl *ManagerController) Create(c *gin.Context) {
	var input it.InputCreateManager
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	manager, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: manager})
}

// @Summary Delete manager
// @Tags Managers
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Manager UID"
// @Success 200 {object} _.Manager
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/managers/{uid}/delete [post]
func (ctrl *ManagerController) Delete(c *gin.Context) {

	data, err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Edit manager
// @Tags Managers
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ 	body it.InputEditManager true "Request body"
// @Param uid path string true "Manager UID"
// @Success 200 {object} _.Manager
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/managers/{uid}/edit [post]
func (ctrl *ManagerController) Edit(c *gin.Context) {
	var input it.InputEditManager
	if err := c.BindJSON(&input); err != nil {
		c.String(http.StatusBadRequest, err.Error())
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), c.MustGet("token").(string), &input, 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})

}

// @Summary Get one manager
// @Tags Managers
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Manager UID"
// @Success 200 {object} _.Manager
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/managers/{uid} [post]
func (ctrl *ManagerController) GetByUID(c *gin.Context) {

	manager, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: manager})
}

// @Summary Get managers
// @Tags Managers
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListManagers true "Request body"
// @Success 200 {object} []_.Manager
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/managers/list [post]
func (ctrl *ManagerController) List(c *gin.Context) {
	var input it.InputGetListManagers
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}
