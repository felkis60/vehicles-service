package repository

import (
	"errors"
	"log"
	"math"
	"time"

	"gitlab.com/felkis60/vehicles-service/helpers/history"
	logs "gitlab.com/felkis60/vehicles-service/log"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

type VehicleRepository struct {
	Db *gorm.DB
}

const (
	vehTableName = "vehicles"
)

func CheckVehicleStatus(status string) bool {
	switch status {
	case "FREE":
	case "RENT":
	case "RANSOM":
	case "REPAIR":
	case "INACCESSIBLE":
	case "IMPLEMENTED":
	case "":
	default:
		return false
	}
	return true
}

// Валидация обязательных полей автомобиля
func (rep *VehicleRepository) checkVehicle(data *bt.Vehicle) error {
	if data.Number == "" {
		return errors.New(logs.ERRAutoNumber)
	}
	if data.Model == "" {
		return errors.New(logs.ERRAutoModel)
	}
	if data.Mark == "" {
		return errors.New(logs.ERRAutoMark)
	}

	if !CheckVehicleStatus(data.Status) {
		return errors.New("ERROR: Wrong vehicle status")
	}
	return nil
}

func (rep *VehicleRepository) Create(input *it.InputCreateVehicle, accountID int64, authUserID int64) (*bt.Vehicle, error) {

	var data bt.Vehicle

	var tx = rep.Db.Begin()

	data.UID = uuid.Must(uuid.NewV4()).String()
	data.ExtID = input.ExtID
	data.AccountsID = accountID
	data.Number = input.Number
	data.Mark = input.Mark
	data.Model = input.Model
	data.Type = input.Type
	data.Sts = input.Sts
	data.Win = input.Win
	data.Width = input.Width
	data.Height = input.Height
	data.Depth = input.Depth
	data.Capacity = input.Capacity
	data.FuelTank = input.FuelTank
	data.SummerRate = input.SummerRate
	data.WinterRate = input.WinterRate
	data.Ransom = input.Ransom
	data.Description = input.Description
	data.Status = input.Status
	data.DKPAmount = input.DKPAmount
	data.AvatarUri = input.AvatarUri
	data.Bodywork = input.Bodywork
	data.Color = input.Color
	data.CustomFields = input.CustomFields
	data.Drive = input.Drive
	data.Engine = input.Engine
	data.Kpp = input.Kpp
	data.Owner = input.Owner
	data.Salon = input.Salon
	data.Year = input.Year

	if input.BuyAmount != nil {
		data.BuyAmount = input.BuyAmount
	}

	if input.TrackerNumber != nil {
		data.TrackerNumber = *input.TrackerNumber
	}

	if input.LighthouseNumber != nil {
		data.LighthouseNumber = *input.LighthouseNumber
	}

	if input.BuyDate != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.BuyDate); err != nil {
			return nil, err
		} else {
			data.BuyDate = &tempTime
		}
	}

	if input.RegistrationDate != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.RegistrationDate); err != nil {
			return nil, err
		} else {
			data.RegistrationDate = &tempTime
		}
	}

	if input.TrackerInstallDate != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.TrackerInstallDate); err != nil {
			return nil, err
		} else {
			data.TrackerInstallDate = &tempTime
		}
	}

	if input.CreatedAt != nil {
		if tempCreatedAt, err := time.Parse("2006-01-02 15:04:05", *input.CreatedAt); err == nil {
			data.CreatedAt = tempCreatedAt
		} else {
			log.Print(err)
		}
	}

	if input.DateOsagoStart != nil {
		tempTime, err := time.Parse("2006-01-02", *input.DateOsagoStart)
		if err != nil {
			return nil, err
		}
		data.DateOsagoStart = &tempTime
	}
	if input.DateOsagoEnd != nil {
		tempTime, err := time.Parse("2006-01-02", *input.DateOsagoEnd)
		if err != nil {
			return nil, err
		}
		data.DateOsagoEnd = &tempTime
	}

	if err := rep.checkVehicle(&data); err != nil {
		return nil, err
	}

	tx.Create(&data)

	if input.DefaultRansomTariff != nil {
		input.DefaultRansomTariff.VehiclesUID = data.UID
		ranTarRep := RansomTariffRepository{Db: tx}
		ransomTariff, err := ranTarRep.Create(input.DefaultRansomTariff, accountID, authUserID)
		if err != nil {
			tx.Rollback()
			return nil, err
		}
		data.DefaultRansomTariffID = &ransomTariff.ID
	}

	if err := tx.Save(&data).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	tx.Commit()
	if err := history.CreateHistory(data.ID, vehTableName, "create", nil, &data, authUserID, accountID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	//if natsH.EncodedConnection != nil {
	//	if err := natsH.EncodedConnection.Publish("vehicle-created", &data); err != nil {
	//		log.Print("ERROR: " + err.Error())
	//	}
	//} else {
	//	log.Print("INFO: No nats connection. No bill for vehicle")
	//}

	return &data, nil
}

func (rep *VehicleRepository) Delete(uid string, accountToken string, authUserID int64) (*bt.Vehicle, error) {

	data, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var orderCount = int64(0)
	if result := helpers.DbAccountLeftJoin("ransom_orders", accountToken, rep.Db.Model(&bt.RansomOrder{})).Where("ransom_orders.vehicles_id = ? AND ransom_orders.is_active = true", data.ID).Count(&orderCount); result.Error != nil {
		return nil, err
	} else if orderCount > 0 {
		return nil, errors.New(logs.ERRAttachedData)
	}
	if result := helpers.DbAccountLeftJoin("rent_orders", accountToken, rep.Db.Model(&bt.RentOrder{})).Where("rent_orders.vehicles_id = ? AND rent_orders.is_active = true", data.ID).Count(&orderCount); result.Error != nil {
		return nil, err
	} else if orderCount > 0 {
		return nil, errors.New(logs.ERRAttachedData)
	}

	var dataBefore = *data

	rep.Db.Delete(data)

	if err := history.CreateHistory(data.ID, vehTableName, "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}
	return data, nil

}

func (rep *VehicleRepository) Edit(uid string, accountToken string, input *it.InputEditVehicle, authUserID int64) (*bt.Vehicle, error) {

	data, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	if input.ExtID != nil {
		data.ExtID = *input.ExtID
	}
	if input.AvatarUri != nil {
		data.AvatarUri = *input.AvatarUri
	}
	if input.Number != nil {
		data.Number = *input.Number
	}
	if input.Mark != nil {
		data.Mark = *input.Mark
	}
	if input.Model != nil {
		data.Model = *input.Model
	}
	if input.Type != nil {
		data.Type = *input.Type
	}
	if input.Sts != nil {
		data.Sts = *input.Sts
	}
	if input.Win != nil {
		data.Win = *input.Win
	}
	if input.Width != nil {
		data.Width = input.Width
	}
	if input.Height != nil {
		data.Height = input.Height
	}
	if input.Depth != nil {
		data.Depth = input.Depth
	}
	if input.Capacity != nil {
		data.Capacity = input.Capacity
	}
	if input.FuelTank != nil {
		data.FuelTank = input.FuelTank
	}
	if input.SummerRate != nil {
		data.SummerRate = input.SummerRate
	}
	if input.WinterRate != nil {
		data.WinterRate = input.WinterRate
	}
	if input.Ransom != nil {
		data.Ransom = *input.Ransom
	}
	if input.Description != nil {
		data.Description = *input.Description
	}
	if input.Bodywork != nil {
		data.Bodywork = *input.Bodywork
	}
	if input.Color != nil {
		data.Color = *input.Color
	}
	if input.CustomFields != nil {
		data.CustomFields = *input.CustomFields
	}
	if input.Drive != nil {
		data.Drive = *input.Drive
	}
	if input.Engine != nil {
		data.Engine = *input.Engine
	}
	if input.Kpp != nil {
		data.Kpp = *input.Kpp
	}
	if input.Owner != nil {
		data.Owner = *input.Owner
	}
	if input.Salon != nil {
		data.Salon = *input.Salon
	}
	if input.Year != nil {
		data.Year = *input.Year
	}
	if input.Status != nil {
		data.Status = *input.Status
	}
	if input.DateOsagoStart != nil {
		tempTime, err := time.Parse("2006-01-02", *input.DateOsagoStart)
		if err != nil {
			return nil, err
		}
		data.DateOsagoStart = &tempTime
	}
	if input.DateOsagoEnd != nil {
		tempTime, err := time.Parse("2006-01-02", *input.DateOsagoEnd)
		if err != nil {
			return nil, err
		}
		data.DateOsagoEnd = &tempTime
	}

	if input.DkpEnterFiles != nil {
		data.DkpEnterFiles = *input.DkpEnterFiles
	}
	if input.StsFiles != nil {
		data.StsFiles = *input.StsFiles
	}
	if input.OsagoFiles != nil {
		data.OsagoFiles = *input.OsagoFiles
	}
	if input.DutyFiles != nil {
		data.DutyFiles = *input.DutyFiles
	}
	if input.ContractsFiles != nil {
		data.ContractsFiles = *input.ContractsFiles
	}
	if input.OtherFiles != nil {
		data.OtherFiles = *input.OtherFiles
	}
	if input.ImagesFiles != nil {
		data.ImagesFiles = *input.ImagesFiles
	}
	if input.DKPAmount != nil {
		data.DKPAmount = *input.DKPAmount
	}

	if input.BuyAmount != nil {
		data.BuyAmount = input.BuyAmount
	}

	if input.TrackerNumber != nil {
		data.TrackerNumber = *input.TrackerNumber
	}

	if input.LighthouseNumber != nil {
		data.LighthouseNumber = *input.LighthouseNumber
	}

	if input.BuyDate != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.BuyDate); err != nil {
			return nil, err
		} else {
			data.BuyDate = &tempTime
		}
	}

	if input.RegistrationDate != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.RegistrationDate); err != nil {
			return nil, err
		} else {
			data.RegistrationDate = &tempTime
		}
	}

	if input.TrackerInstallDate != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.TrackerInstallDate); err != nil {
			return nil, err
		} else {
			data.TrackerInstallDate = &tempTime
		}
	}

	if err := rep.checkVehicle(data); err != nil {
		return nil, err
	}

	rep.Db.Save(data)

	if err := history.CreateHistory(data.ID, vehTableName, "edit", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil
}

func (rep *VehicleRepository) GetByNumber(number string, accountToken string, withNestedData bool) (*bt.Vehicle, error) {
	var data bt.Vehicle
	if result := helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db).Find(&data, "vehicles.number ILIKE ?", number); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchVehiclesNumber)
	}

	if withNestedData {
		rep.includeNestedData(&data, accountToken)
	}

	return &data, nil
}

func (rep *VehicleRepository) GetByUID(UID string, accountToken string, withNestedData bool) (*bt.Vehicle, error) {
	var data bt.Vehicle
	if result := helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db).Find(&data, "vehicles.uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchVehiclesUID)
	}

	if withNestedData {
		rep.includeNestedData(&data, accountToken)
	}

	return &data, nil
}

func (rep *VehicleRepository) GetByIDs(input *it.InputGetByIDs, accountToken string) (*[]bt.Vehicle, error) {
	var data []bt.Vehicle
	if result := helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db).Find(&data, "vehicles.id = ANY(?)", input.IDs); result.Error != nil {
		return nil, result.Error
	}

	return &data, nil
}

func (rep *VehicleRepository) GetByID(id int64, accountToken string, withNestedData bool) (*bt.Vehicle, error) {
	var data bt.Vehicle
	if result := helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db).Find(&data, id); result.RowsAffected == 0 {
		return nil, errors.New("ERROR: No such id")
	}

	if withNestedData {
		rep.includeNestedData(&data, accountToken)
	}

	return &data, nil
}

func (rep *VehicleRepository) GetBySts(accountToken string, sts string, withNestedData bool) (*bt.Vehicle, error) {
	var data bt.Vehicle
	if result := helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db).Unscoped().Find(&data, "replace(vehicles.sts, ' ', '') ILIKE ?", sts); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchVehiclesNumber)
	}

	if withNestedData {
		rep.includeNestedData(&data, accountToken)
	}

	return &data, nil
}

func (rep *VehicleRepository) includeNestedData(data *bt.Vehicle, accountToken string) {
	var alllRentTariffs []bt.RentTariff
	rep.Db.Find(&alllRentTariffs, "vehicles_id = ?", data.ID)
	if len(alllRentTariffs) > 0 {
		data.RentTariffs = &alllRentTariffs
	}

	var allRansomTariffs []bt.RansomTariff
	rep.Db.Find(&allRansomTariffs, "vehicles_id = ?", data.ID)
	if len(allRansomTariffs) > 0 {
		data.RansomTariffs = &allRansomTariffs
	}

	var activeRansomOrder bt.RansomOrder
	rep.Db.Find(&activeRansomOrder, "vehicles_id = ? AND is_active = 'true'", data.ID)
	if activeRansomOrder.ID > 0 {
		var ransomOrderRep = RansomOrderRepository{Db: rep.Db}
		if order, err := ransomOrderRep.GetByUID(activeRansomOrder.UID, accountToken, true, false); err == nil {
			data.RansomOrder = order
		} else {
			log.Print(err)
		}
	}

	var activeRentOrder bt.RentOrder
	rep.Db.Find(&activeRentOrder, "vehicles_id = ? AND is_active = 'true'", data.ID)
	if activeRentOrder.ID > 0 {
		var rentOrderRep = RentOrderRepository{Db: rep.Db}
		if order, err := rentOrderRep.GetByUID(activeRentOrder.UID, accountToken, true, false); err == nil {
			data.RentOrder = order
		} else {
			log.Print(err)
		}
	}

	{

		var tempRansomOrder []bt.RansomOrder
		rep.Db.Find(&tempRansomOrder, "vehicles_id = ? AND deleted_at IS null", data.ID)
		for i := range tempRansomOrder {
			var tempManager bt.Manager
			var tempUser bt.User
			var tempTariff bt.RansomTariff

			rep.Db.Find(&tempTariff, tempRansomOrder[i].TariffsID)
			if tempTariff.ID > 0 {
				tempRansomOrder[i].RansomTariff = &tempTariff
			}

			rep.Db.Find(&tempUser, tempRansomOrder[i].UsersID)
			if tempUser.ID > 0 {
				tempRansomOrder[i].User = &tempUser
			}

			rep.Db.Find(&tempManager, tempRansomOrder[i].ManagersID)
			if tempManager.ID > 0 {
				tempRansomOrder[i].Manager = &tempManager
			}
		}
		data.RansomOrders = &tempRansomOrder

		var tempRentOrder []bt.RentOrder
		rep.Db.Find(&tempRentOrder, "vehicles_id = ? AND deleted_at IS null", data.ID)
		for i := range tempRentOrder {
			var tempManager bt.Manager
			var tempUser bt.User
			var tempTariff bt.RentTariff

			rep.Db.Find(&tempTariff, tempRentOrder[i].TariffsID)
			if tempTariff.ID > 0 {
				tempRentOrder[i].RentTariff = &tempTariff
			}

			rep.Db.Find(&tempUser, tempRentOrder[i].UsersID)
			if tempUser.ID > 0 {
				tempRentOrder[i].User = &tempUser
			}

			rep.Db.Find(&tempManager, tempRentOrder[i].ManagersID)
			if tempManager.ID > 0 {
				tempRentOrder[i].Manager = &tempManager
			}
		}
		data.RentOrders = &tempRentOrder
	}

}

func (rep *VehicleRepository) GetByUIDForAnalytics(UID string, accountToken string) (*bt.VehicleAnalytics, error) {
	var existingVehicle bt.Vehicle
	if result := helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db).Find(&existingVehicle, "uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchVehiclesUID)
	}

	var output bt.VehicleAnalytics

	var tempUsers []bt.User
	if result := rep.Db.Model(&bt.User{}).Joins("LEFT JOIN ransom_orders ON ransom_orders.users_id = users.id").Find(&tempUsers, "ransom_orders.vehicles_id = ?", existingVehicle.ID); result.Error != nil {
		return nil, result.Error
	}
	output.Users = &tempUsers

	var tempLastRansOrder bt.RansomOrder
	if result := rep.Db.Order("contract_date DESC").First(&tempLastRansOrder, "ransom_orders.vehicles_id = ?", existingVehicle.ID); result.Error != nil {
		return nil, result.Error
	}

	var activeRansomTariff bt.RansomTariff
	if tempLastRansOrder.ID > 0 {
		if result := rep.Db.Find(&activeRansomTariff, "id = ?", tempLastRansOrder.TariffsID); result.Error != nil {
			return nil, result.Error
		}
	}

	var tempRansOrdersUIDs []string
	if result := rep.Db.Model(&bt.RansomOrder{}).Select("uid").Find(&tempRansOrdersUIDs, "vehicles_id = ?", existingVehicle.ID); result.Error != nil {
		return nil, result.Error
	}

	var tempRentOrdersUIDs []string
	rep.Db.Model(&bt.RentOrder{}).Select("uid").Find(&tempRentOrdersUIDs, "vehicles_id = ?", existingVehicle.ID)

	tempRansOrdersUIDs = append(tempRansOrdersUIDs, tempRentOrdersUIDs...)
	output.OrdersUIDs = &tempRansOrdersUIDs

	output.VehiclesVin = existingVehicle.Win
	output.VehiclesID = existingVehicle.ID
	output.VehiclesMark = existingVehicle.Mark
	output.VehiclesModel = existingVehicle.Model
	output.VehiclesNumber = existingVehicle.Number
	output.VehiclesOwner = existingVehicle.Owner
	output.VehiclesSts = existingVehicle.Sts
	output.VehicleAmountForUser = activeRansomTariff.VehicleAmount
	output.AllVehicleAmount = activeRansomTariff.TotalVehicleAmount
	output.YearKoeff = float64(activeRansomTariff.Term) / 12.0
	output.VehicleAmountWithoutInititalFee = activeRansomTariff.VehicleAmount - float64(activeRansomTariff.InitialFee)
	output.RansTariffTotalVehicleWithoutInsuranceAmount = activeRansomTariff.TotalVehicleWithoutInsuranceAmount
	output.RansTariffInsuranceRate = int64(activeRansomTariff.InsuranceRate)
	output.MonthPaymentAmount = activeRansomTariff.DayPayment * 30.4167
	output.DayPaymentAmount = activeRansomTariff.DayPayment
	output.MandatoryPaymentAmount = (activeRansomTariff.TrackerServiceAmount * float64(activeRansomTariff.Term)) + (activeRansomTariff.TrackerAmount) + (activeRansomTariff.VehicleTaxAmount * output.YearKoeff) + (activeRansomTariff.RegistrationAmount) + (activeRansomTariff.InsuranceOsagoAmount * output.YearKoeff)
	output.InvestAmount = (activeRansomTariff.VehicleAmount) + (activeRansomTariff.TrackerServiceAmount * float64(activeRansomTariff.Term)) + (activeRansomTariff.VehicleTaxAmount * output.YearKoeff) + (activeRansomTariff.RegistrationAmount) + (activeRansomTariff.TrackerAmount) + (activeRansomTariff.InsuranceOsagoAmount * output.YearKoeff) - float64(activeRansomTariff.InitialFee)
	return &output, nil
}

func (rep *VehicleRepository) listDbBuilder(accountToken string, input *it.InputGetListVehicles) (*gorm.DB, error) {
	var tx = helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db.Model(&bt.Vehicle{}))

	tx = tx.Select("vehicles.*, COUNT(reminders.*) AS reminders_count").Joins("LEFT JOIN reminders ON reminders.vehicles_id = vehicles.id AND reminders.closed = false AND EXTRACT(DAY FROM reminders.date - now()) <= 1").Group("vehicles.id, reminders.vehicles_id")

	if input.Filters.Search != nil {
		tempString := "%" + *input.Filters.Search + "%"
		tx = tx.Where("(vehicles.model ILIKE ? OR vehicles.mark ILIKE ? OR vehicles.number ILIKE ? OR vehicles.win ILIKE ? OR vehicles.sts ILIKE ?)", tempString, tempString, tempString, tempString, tempString)
	}
	if input.Filters.Color != nil {
		tx = tx.Where("vehicles.color ILIKE ?", *input.Filters.Color)
	}
	if input.Filters.Mark != nil {
		tx = tx.Where("vehicles.mark ILIKE ?", *input.Filters.Mark)
	}
	if input.Filters.Model != nil {
		tx = tx.Where("vehicles.model ILIKE ?", *input.Filters.Model)
	}
	if input.Filters.DateCreatedStart != nil {
		if tempTime, err := time.Parse("2006-01-02T15:04:05Z", *input.Filters.DateCreatedStart); err != nil {
			return nil, err
		} else {
			tx = tx.Where("vehicles.created_at >= ?", tempTime)
		}
	}
	if input.Filters.DateCreatedEnd != nil {
		if tempTime, err := time.Parse("2006-01-02T15:04:05Z", *input.Filters.DateCreatedEnd); err != nil {
			return nil, err
		} else {
			tx = tx.Where("vehicles.created_at <= ?", tempTime)
		}
	}

	if input.Filters.BuyDateStart != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.Filters.BuyDateStart); err != nil {
			return nil, err
		} else {
			tx = tx.Where("vehicles.buy_date >= ?", tempTime)
		}
	}
	if input.Filters.BuyDateEnd != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.Filters.BuyDateEnd); err != nil {
			return nil, err
		} else {
			tempTime.Add(time.Hour * 24)
			tx = tx.Where("vehicles.buy_date <= ?", tempTime)
		}
	}

	if input.Filters.RegistrationDateStart != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.Filters.RegistrationDateStart); err != nil {
			return nil, err
		} else {
			tx = tx.Where("vehicles.registration_date >= ?", tempTime)
		}
	}
	if input.Filters.RegistrationDateEnd != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.Filters.RegistrationDateEnd); err != nil {
			return nil, err
		} else {
			tempTime.Add(time.Hour * 24)
			tx = tx.Where("vehicles.registration_date <= ?", tempTime)
		}
	}

	if input.Filters.TrackerInstallDateStart != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.Filters.TrackerInstallDateStart); err != nil {
			return nil, err
		} else {
			tx = tx.Where("vehicles.tracker_install_date >= ?", tempTime)
		}
	}
	if input.Filters.TrackerInstallDateEnd != nil {
		if tempTime, err := time.Parse("2006-01-02", *input.Filters.TrackerInstallDateEnd); err != nil {
			return nil, err
		} else {
			tempTime.Add(time.Hour * 24)
			tx = tx.Where("vehicles.tracker_install_date <= ?", tempTime)
		}
	}

	if input.Filters.Owner != nil {
		tx = tx.Where("vehicles.owner ILIKE ?", "%"+*input.Filters.Owner+"%")
	}

	if input.Filters.VehiclesUIDs != nil {
		tx = tx.Where("vehicles.uid in (?)", *input.Filters.VehiclesUIDs)
	}
	if input.Filters.VehiclesIDs != nil {
		tx = tx.Where("vehicles.id in (?)", *input.Filters.VehiclesIDs)
	}
	if input.Filters.InStatuses != nil {
		tx = tx.Where("vehicles.status in (?)", *input.Filters.InStatuses)
	}

	if input.Filters.UsersPhone != nil {
		tx = tx.Joins("LEFT JOIN ransom_orders AS rans_o ON rans_o.vehicles_id = vehicles.id")
		tx = tx.Joins("LEFT JOIN rent_orders AS rent_o ON rent_o.vehicles_id = vehicles.id")
		tx = tx.Joins("LEFT JOIN users AS usr ON usr.id = rans_o.users_id OR usr.id = rent_o.users_id")
		tx = tx.Where("usr.phone ILIKE ?", "%"+helpers.CutPhone(helpers.CleanPhone(*input.Filters.UsersPhone))+"%")
		tx = tx.Group("vehicles.id")
	}

	if input.Filters.Public != nil {
		if *input.Filters.Public {
			tx = tx.Where("vehicles.status = 'FREE'")
		}
	}

	return tx, nil
}

func (rep *VehicleRepository) List(accountToken string, input *it.InputGetListVehicles) (*helpers.Pagination, error) {

	tx, err := rep.listDbBuilder(accountToken, input)
	if err != nil {
		return nil, err
	}
	tx = tx.Session(&gorm.Session{})

	var totalItems int64
	if result := tx.Count(&totalItems); result.Error != nil {
		return nil, result.Error
	}

	var orderString = ""
	if input.OrderBy != "" {
		switch input.OrderBy {
		case "reminders_count":
			orderString = "reminders_count"
		default:
			orderString = vehTableName + "." + input.OrderBy
		}
	}

	if input.OrderDir != "" {
		orderString += " " + input.OrderDir
	}

	if orderString != "" {
		tx = tx.Order(orderString)
	}
	var dataMap []map[string]interface{}
	if result := helpers.PaginateManual(&input.Page, &input.PageSize, 5, 1000)(tx).Scan(&dataMap); result.Error != nil {
		return nil, result.Error
	}

	for i := range dataMap {

		var tempRansTariff []bt.RansomTariff
		if result := rep.Db.Joins("LEFT JOIN ransom_orders ON ransom_tariffs.id = ransom_orders.tariffs_id").Where("ransom_orders.is_active = true AND ransom_orders.vehicles_id = ?", dataMap[i]["id"]).Find(&tempRansTariff); result.Error != nil {
			return nil, result.Error
		}
		if len(tempRansTariff) > 0 {
			dataMap[i]["ransom_tariffs"] = &tempRansTariff
		}
		var tempRentTariff []bt.RentTariff
		if result := rep.Db.Joins("LEFT JOIN rent_orders ON rent_tariffs.id = rent_orders.tariffs_id").Where("rent_orders.is_active = true AND rent_orders.vehicles_id = ?", dataMap[i]["id"]).Find(&tempRentTariff); result.Error != nil {
			return nil, result.Error
		}
		if len(tempRentTariff) > 0 {
			dataMap[i]["rent_tariffs"] = &tempRentTariff
		}
	}

	var totalPages = int(math.Ceil(float64(totalItems) / float64(input.PageSize)))

	return &helpers.Pagination{Page: input.Page, Items: &dataMap, TotalItems: totalItems, TotalPages: totalPages}, nil
}

func (rep *VehicleRepository) History(UID string, accountToken string, input *it.InputGetHistoryList) (*helpers.Pagination, error) {

	//Getting base model data
	dataByUID, err := rep.GetByUID(UID, accountToken, false)
	if err != nil {
		return nil, err
	}

	//Filters
	tx := rep.Db.Model(&bt.History{}).Where("source_table = ? AND source_id = ?", vehTableName, dataByUID.ID)

	if input.Filters.OperationType != nil {
		tx = tx.Where("operation_type = ?", *input.Filters.OperationType)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.History
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, "histories", &data)(tx)
}

func (rep *VehicleRepository) OrdersList(accountToken string, uid string) (*map[string]interface{}, error) {
	vehData, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var allRentOrders []bt.RentOrder
	if result := helpers.DbAccountLeftJoin("rent_orders", accountToken, rep.Db.Unscoped()).Order("date_start DESC").Find(&allRentOrders, "vehicles_id = ?", vehData.ID); result.Error != nil {
		return nil, result.Error
	}
	for i := range allRentOrders {
		var tempUser bt.User
		var tempTariff bt.RentTariff
		rep.Db.Find(&tempUser, allRentOrders[i].UsersID)
		rep.Db.Find(&tempTariff, allRentOrders[i].TariffsID)
		allRentOrders[i].User = &tempUser
		allRentOrders[i].RentTariff = &tempTariff
		allRentOrders[i].VehicleUID = &vehData.UID
		allRentOrders[i].UserUID = &tempUser.UID

	}

	var allRansomOrders []bt.RansomOrder
	if result := helpers.DbAccountLeftJoin("ransom_orders", accountToken, rep.Db.Unscoped()).Order("contract_date DESC").Find(&allRansomOrders, "vehicles_id = ?", vehData.ID); result.Error != nil {
		return nil, result.Error
	}
	for i := range allRansomOrders {
		var tempUser bt.User
		var tempTariff bt.RansomTariff
		rep.Db.Find(&tempUser, allRansomOrders[i].UsersID)
		rep.Db.Find(&tempTariff, allRansomOrders[i].TariffsID)
		allRansomOrders[i].User = &tempUser
		allRansomOrders[i].RansomTariff = &tempTariff
		allRansomOrders[i].VehicleUID = &vehData.UID
		allRansomOrders[i].UserUID = &tempUser.UID

	}

	return &map[string]interface{}{"rent_orders": &allRentOrders, "ransom_orders": &allRansomOrders}, nil
}
