package routines

import (
	"log"
	"time"

	"gitlab.com/felkis60/vehicles-service/database"

	"gitlab.com/felkis60/vehicles-service/repository"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"
)

func CreateMandatoryPayments() {
	var tempCurrentDate = time.Now()

	var account bt.Account
	if result := database.Db.First(&account, "name = 'REX'"); result.RowsAffected == 0 {
		log.Println("ERROR: SYNC: No rex account!")
		return
	}

	//var tempDateString = strconv.Itoa(tempCurrentDate.Year()) + "-"
	//if tempCurrentDate.Month() < 10{
	//	tempDateString += "0"
	//}
	//tempDateString += strconv.Itoa(int(tempCurrentDate.Month())) + "-"
	//if tempCurrentDate.Day() < 10{
	//	tempDateString += "0"
	//}
	//tempDateString += strconv.Itoa(tempCurrentDate.Day())

	var tempInput = it.InputContributeIntoRansomOrder{Date: tempCurrentDate.Format("2006-01-02"), ContributionAmount: -1}

	var tempRep = repository.RansomOrderRepository{Db: database.Db}

	var allOrders []bt.RansomOrder
	tempRep.Db.Model(&bt.RansomOrder{}).Find(&allOrders, "ransom_orders.accounts_id = ?", account.ID)

	for i := range allOrders {
		tempRep.Contribute(allOrders[i].UID, &tempInput, account.Token, 0)
	}

}
