package repository

import (
	"log"
	"math"
	"strconv"
	"time"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"gorm.io/gorm"
)

type OrderRepository struct {
	Db *gorm.DB
}

type PairOfStrings struct {
	First, Second string
}

func (rep *OrderRepository) List1(accountToken string, input *it.InputGetListOrders, withAccount bool) (*helpers.Pagination, error) {

	start := time.Now()
	var rentOrderSelectRaw = "SELECT rent_orders.id, rent_orders.uid, rent_orders.is_active, rent_orders.status, CONCAT(u.last_name, ' ', u.first_name, ' ', u.middle_name) AS user_full_name, u.uid AS user_uid, CONCAT(v.mark, ' ', v.model, ' ', v.number) AS vehicle_full_name, v.uid AS vehicle_uid, u.phone AS user_phone, null AS ransom_term, t.day_payment AS day_payment, rent_orders.date_start AS date_start, rent_orders.date_end AS date_end, null AS contract_date, null AS contract_date_end, 'rent' AS type, null AS ransom_year_rate FROM rent_orders LEFT JOIN users AS u ON rent_orders.users_id = u.id LEFT JOIN rent_tariffs AS t ON rent_orders.tariffs_id = t.id LEFT JOIN vehicles AS v ON rent_orders.vehicles_id = v.id"

	var ransomOrderSelectRaw = " UNION SELECT ransom_orders.id, ransom_orders.uid, ransom_orders.is_active, ransom_orders.status, CONCAT(u.last_name, ' ', u.first_name, ' ', u.middle_name) AS user_full_name, u.uid as user_uid, CONCAT(v.mark, ' ', v.model, ' ', v.number) AS vehicle_full_name, v.uid AS vehicle_uid, u.phone AS user_phone, t.term AS ransom_term, t.day_payment AS day_payment, null AS date_start, null AS date_end, ransom_orders.contract_date AS contract_date, ransom_orders.contract_date_end AS contract_date_end, 'ransom' AS type, t.year_rate AS ransom_year_rate FROM ransom_orders LEFT JOIN users AS u ON ransom_orders.users_id = u.id LEFT JOIN ransom_tariffs AS t ON ransom_orders.tariffs_id = t.id LEFT JOIN vehicles AS v ON ransom_orders.vehicles_id = v.id"

	if withAccount {
		rentOrderSelectRaw += " LEFT JOIN accounts AS a ON rent_orders.accounts_id = a.id WHERE a.token = '" + accountToken + "'"
		ransomOrderSelectRaw += " LEFT JOIN accounts AS a ON ransom_orders.accounts_id = a.id WHERE a.token = '" + accountToken + "'"
	}

	var tempSearch string
	if input.Filters.Search != nil {
		tempSearch = " WHERE (sel.user_full_name ILIKE '%" + *input.Filters.Search + "%' OR sel.vehicle_full_name ILIKE '%" + *input.Filters.Search + "%' OR sel.user_phone LIKE '%" + *input.Filters.Search + "%')"
	}
	if input.Filters.IsActive != nil {
		if tempSearch == "" {
			if *input.Filters.IsActive {
				tempSearch += " WHERE is_active = true"
			} else {
				tempSearch += " WHERE is_active = false"
			}
		} else {
			if *input.Filters.IsActive {
				tempSearch += " AND is_active = true"
			} else {
				tempSearch += " AND is_active = false"
			}
		}
	}

	if input.Filters.UsersPhone != nil {
		var userRep = UserRepository{Db: rep.Db}
		user, err := userRep.GetByPhone(accountToken, *input.Filters.UsersPhone)
		if err != nil {
			return nil, err
		}
		if tempSearch == "" {
			tempSearch += " WHERE ransom_orders.users_id = " + strconv.FormatInt(user.ID, 10)
		} else {
			tempSearch += " AND ransom_orders.users_id = " + strconv.FormatInt(user.ID, 10)
		}
	}

	var AllSelectCountRaw = "SELECT COUNT(*) FROM (" + rentOrderSelectRaw + ransomOrderSelectRaw + ") AS sel" + tempSearch
	var AllSelectRaw = "SELECT * FROM (" + rentOrderSelectRaw + ransomOrderSelectRaw + ") AS sel" + tempSearch

	log.Print("INFO: strings: ", time.Since(start))
	start = time.Now()

	var tempCount int64
	if result := rep.Db.Raw(AllSelectCountRaw).Count(&tempCount); result.Error != nil {
		return nil, result.Error
	}

	log.Print("INFO: count: ", time.Since(start))
	start = time.Now()

	var tempOrder = ""
	if input.OrderBy != "" {
		tempOrder = input.OrderBy
	}
	if input.OrderDir != "" {
		tempOrder += " " + input.OrderDir
	}
	if tempOrder != "" {
		AllSelectRaw += " ORDER BY " + tempOrder
	}

	if input.Page <= 0 {
		input.Page = 1
	}
	switch {
	case input.PageSize > 100:
		input.PageSize = 100
	case input.PageSize < 5:
		input.PageSize = 5
	}
	offset := (input.Page - 1) * input.PageSize

	AllSelectRaw += " LIMIT " + strconv.Itoa(input.PageSize) + " OFFSET " + strconv.Itoa(offset)

	var data []bt.Order
	if result := rep.Db.Raw(AllSelectRaw).Scan(&data); result.Error != nil {
		return nil, result.Error
	}
	log.Print("INFO: list: ", time.Since(start))

	var tempPages = int(math.Ceil(float64(tempCount) / float64(input.PageSize)))

	return &helpers.Pagination{Page: input.Page, Items: data, TotalItems: tempCount, TotalPages: tempPages}, nil
}

func (rep *OrderRepository) PeriodSummary(accountToken string, input *it.InputGetPeriodSummary) (*bt.OrdersPeriodSummary, error) {
	var data bt.OrdersPeriodSummary

	//All
	{
		tx := helpers.DbAccountLeftJoin("histories", accountToken, rep.Db.Model(&bt.History{})).Where("(histories.source_table = 'rent_orders' OR histories.source_table = 'ransom_orders')")
		if input.DateStart != nil {
			if startTime, err := time.Parse("2006-01-02", *input.DateStart); err == nil {
				tx = tx.Where("histories.created_at >= ?", startTime)
			} else {
				return nil, err
			}
		}
		if input.DateEnd != nil {
			if endTime, err := time.Parse("2006-01-02", *input.DateEnd); err == nil {
				tx = tx.Where("histories.created_at <= ?", endTime)
			} else {
				return nil, err
			}
		}

		tx = tx.Session(&gorm.Session{})

		if result := tx.Where("(CAST(histories.after::jsonb->'is_active' AS bool) = TRUE AND histories.operation_type = 'create')").Count(&data.TotalNewOrders); result.Error != nil {
			return nil, result.Error
		}

		if result := tx.Where("(CAST(histories.after::jsonb->'is_active' AS bool) = false AND histories.operation_type = 'edit' AND COALESCE(CAST(histories.before::jsonb->'is_active' AS bool), false) != CAST(histories.after::jsonb->'is_active' AS bool))").Count(&data.TotalEndedOrders); result.Error != nil {
			return nil, result.Error
		}

		if result := tx.Where("(COALESCE(CAST(histories.before::jsonb->'is_active' AS bool), false) != CAST(histories.after::jsonb->'is_active' AS bool))").Count(&data.TotalNewAndEndedOrders); result.Error != nil {
			return nil, result.Error
		}
	}

	//Rent
	{
		tx := helpers.DbAccountLeftJoin("histories", accountToken, rep.Db.Model(&bt.History{})).Where("(histories.source_table = 'rent_orders')")
		if input.DateStart != nil {
			if startTime, err := time.Parse("2006-01-02", *input.DateStart); err == nil {
				tx = tx.Where("histories.created_at >= ?", startTime)
			} else {
				return nil, err
			}
		}
		if input.DateEnd != nil {
			if endTime, err := time.Parse("2006-01-02", *input.DateEnd); err == nil {
				tx = tx.Where("histories.created_at <= ?", endTime)
			} else {
				return nil, err
			}
		}

		tx = tx.Session(&gorm.Session{})

		if result := tx.Where("(CAST(histories.after::jsonb->'is_active' AS bool) = TRUE AND histories.operation_type = 'create')").Count(&data.TotalNewRentOrders); result.Error != nil {
			return nil, result.Error
		}

		if result := tx.Where("(CAST(histories.after::jsonb->'is_active' AS bool) = false AND histories.operation_type = 'edit' AND COALESCE(CAST(histories.before::jsonb->'is_active' AS bool), false) != CAST(histories.after::jsonb->'is_active' AS bool))").Count(&data.TotalEndedRentOrders); result.Error != nil {
			return nil, result.Error
		}

		if result := tx.Where("(COALESCE(CAST(histories.before::jsonb->'is_active' AS bool), false) != CAST(histories.after::jsonb->'is_active' AS bool))").Count(&data.TotalNewAndEndedRentOrders); result.Error != nil {
			return nil, result.Error
		}
	}
	//Ransom
	{
		tx := helpers.DbAccountLeftJoin("histories", accountToken, rep.Db.Model(&bt.History{})).Where("(histories.source_table = 'ransom_orders')")
		if input.DateStart != nil {
			if startTime, err := time.Parse("2006-01-02", *input.DateStart); err == nil {
				tx = tx.Where("histories.created_at >= ?", startTime)
			} else {
				return nil, err
			}
		}
		if input.DateEnd != nil {
			if endTime, err := time.Parse("2006-01-02", *input.DateEnd); err == nil {
				tx = tx.Where("histories.created_at <= ?", endTime)
			} else {
				return nil, err
			}
		}

		tx = tx.Session(&gorm.Session{})

		if result := tx.Where("(CAST(histories.after::jsonb->'is_active' AS bool) = TRUE AND histories.operation_type = 'create')").Count(&data.TotalNewRansomOrders); result.Error != nil {
			return nil, result.Error
		}

		if result := tx.Where("(CAST(histories.after::jsonb->'is_active' AS bool) = false AND histories.operation_type = 'edit' AND COALESCE(CAST(histories.before::jsonb->'is_active' AS bool), false) != CAST(histories.after::jsonb->'is_active' AS bool))").Count(&data.TotalEndedRansomOrders); result.Error != nil {
			return nil, result.Error
		}

		if result := tx.Where("(COALESCE(CAST(histories.before::jsonb->'is_active' AS bool), false) != CAST(histories.after::jsonb->'is_active' AS bool))").Count(&data.TotalNewAndEndedRansomOrders); result.Error != nil {
			return nil, result.Error
		}
	}

	return &data, nil
}

func (rep *OrderRepository) List2(accountToken string, input *it.InputGetListOrders, withAccount bool) (*helpers.Pagination, error) {

	//null AS ransom_term,
	//t.day_payment AS day_payment,
	//rent_orders.date_start AS date_start,
	//rent_orders.date_end AS date_end,
	//null AS contract_date,
	//null AS contract_date_end,
	//'rent' AS type
	//
	//t.term AS ransom_term,
	//ransom_orders.day_payment AS day_payment,
	//null AS date_start,
	//null AS date_end,
	//ransom_orders.contract_date AS contract_date,
	//ransom_orders.contract_date_end AS contract_date_end,
	//'ransom' AS type

	var fieldsToSelect = map[string]string{
		"id":                ".id",
		"uid":               ".uid",
		"is_active":         ".is_active",
		"status":            ".status",
		"user_full_name":    "CONCAT(u.last_name, ' ', u.first_name, ' ', u.middle_name)",
		"user_uid":          "u.uid",
		"vehicle_full_name": "CONCAT(v.mark, ' ', v.model, ' ', v.number)",
		"vehicle_uid":       "v.uid",
		"user_phone":        "u.phone",
		"day_payment":       "t.day_payment",
	}

	//First - rent, Second - ransom
	var doubleFieldsToSelect = map[string]PairOfStrings{
		"ransom_term":       {"null", "t.term"},
		"date_start":        {".date_start", "null"},
		"date_end":          {".date_end", "null"},
		"contract_date":     {"null", ".contract_date"},
		"contract_date_end": {"null", ".contract_date_end"},
		"type":              {"'rent'", "'ransom'"},
	}

	var rentSelectStrings = make([]string, 0, 20)
	var ransomSelectStrings = make([]string, 0, 20)

	for as, what := range fieldsToSelect {
		if len(as) > 0 && len(what) > 0 {
			if what[0] == '.' {
				rentSelectStrings = append(rentSelectStrings, "rent_orders"+what+" AS "+as)
				ransomSelectStrings = append(ransomSelectStrings, "ransom_orders"+what+" AS "+as)
			} else {
				rentSelectStrings = append(rentSelectStrings, what+" AS "+as)
				ransomSelectStrings = append(ransomSelectStrings, what+" AS "+as)
			}
		}
	}

	for as, what := range doubleFieldsToSelect {

		if what.First[0] == '.' {
			rentSelectStrings = append(rentSelectStrings, "rent_orders"+what.First+" AS "+as)
		} else {
			rentSelectStrings = append(rentSelectStrings, what.First+" AS "+as)
		}
		if what.Second[0] == '.' {
			ransomSelectStrings = append(ransomSelectStrings, "ransom_orders"+what.Second+" AS "+as)
		} else {
			ransomSelectStrings = append(ransomSelectStrings, what.Second+" AS "+as)
		}
	}

	var txRent = rep.Db.Model(&bt.RentOrder{}).Select(rentSelectStrings)
	txRent = txRent.Joins("LEFT JOIN users AS u ON rent_orders.users_id = u.id")
	txRent = txRent.Joins("LEFT JOIN rent_tariffs AS t ON rent_orders.tariffs_id = t.id")
	txRent = txRent.Joins("LEFT JOIN vehicles AS v ON rent_orders.vehicles_id = v.id")

	var txRansom = rep.Db.Model(&bt.RansomOrder{}).Select(ransomSelectStrings)
	txRansom = txRansom.Joins("LEFT JOIN users AS u ON ransom_orders.users_id = u.id")
	txRansom = txRansom.Joins("LEFT JOIN ransom_tariffs AS t ON ransom_orders.tariffs_id = t.id")
	txRansom = txRansom.Joins("LEFT JOIN vehicles AS v ON ransom_orders.vehicles_id = v.id")

	if withAccount {
		txRent = txRent.Joins("LEFT JOIN accounts AS a ON rent_orders.accounts_id = a.id").Where("a.token = ?", accountToken)
		txRansom = txRansom.Joins("LEFT JOIN accounts AS a ON ransom_orders.accounts_id = a.id").Where("a.token = ?", accountToken)
	}

	var searchQueryString string
	if input.Filters.Search != nil {
		searchQueryString = " WHERE (sel.user_full_name ILIKE '%" + *input.Filters.Search + "%' OR sel.vehicle_full_name ILIKE '%" + *input.Filters.Search + "%' OR sel.user_phone LIKE '%" + *input.Filters.Search + "%')"
	}
	if input.Filters.IsActive != nil {
		if searchQueryString == "" {
			searchQueryString += " WHERE"
		} else {
			searchQueryString += " AND"
		}
		if *input.Filters.IsActive {
			searchQueryString += " is_active = true"
		} else {
			searchQueryString += " is_active = false"
		}
	}

	if input.Filters.UsersPhone != nil {
		var userRep = UserRepository{Db: rep.Db}
		user, err := userRep.GetByPhone(accountToken, *input.Filters.UsersPhone)
		if err != nil {
			return nil, err
		}
		if searchQueryString == "" {
			searchQueryString += " WHERE"
		} else {
			searchQueryString += " AND"
		}
		searchQueryString += " ransom_orders.users_id = " + strconv.FormatInt(user.ID, 10)
	}

	if input.Filters.UsersUID != nil {
		if searchQueryString == "" {
			searchQueryString += " WHERE"
		} else {
			searchQueryString += " AND"
		}
		searchQueryString += " u.uid = " + *input.Filters.UsersUID
	}

	var totalItems int64
	if result := rep.Db.Raw("SELECT count(*) FROM (? UNION ?) as sel "+searchQueryString, txRent, txRansom).Count(&totalItems); result.Error != nil {
		return nil, result.Error
	}

	var orderString = ""
	if input.OrderBy != "" {
		orderString = input.OrderBy
	}
	if input.OrderDir != "" {
		orderString += " " + input.OrderDir
	}
	if orderString != "" {
		orderString = " ORDER BY " + orderString
	}

	if input.Page <= 0 {
		input.Page = 1
	}
	switch {
	case input.PageSize > 100:
		input.PageSize = 100
	case input.PageSize < 5:
		input.PageSize = 5
	}
	offset := (input.Page - 1) * input.PageSize

	var paginationQueryString = " LIMIT " + strconv.Itoa(input.PageSize) + " OFFSET " + strconv.Itoa(offset)

	var data []map[string]interface{}
	if result := rep.Db.Raw("SELECT * FROM (? UNION ?) as sel "+searchQueryString+orderString+paginationQueryString, txRent, txRansom).Scan(&data); result.Error != nil {
		return nil, result.Error
	}

	var totalPages = int(math.Ceil(float64(totalItems) / float64(input.PageSize)))

	return &helpers.Pagination{Page: input.Page, Items: data, TotalItems: totalItems, TotalPages: totalPages}, nil
}
