package routines

import (
	"log"

	db "gitlab.com/felkis60/vehicles-service/database"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
)

func UpdateFreeVehiclesStatus(accountToken string) {

	var allFreeVehicles []bt.Vehicle
	if result := db.Db.Model(&bt.Vehicle{}).Find(&allFreeVehicles, "vehicles.status IS NULL OR vehicles.status = 'FREE' OR vehicles.status = ''"); result.Error != nil {
		log.Print(result.Error)
		return
	}

	for i := range allFreeVehicles {

		//rent
		var rentOrder bt.RentOrder
		if result := db.Db.Model(&bt.RentOrder{}).Find(&rentOrder, "is_active = true AND vehicles_id = ?", allFreeVehicles[i].ID); result.Error != nil {
			log.Print(result.Error)
			return
		} else if result.RowsAffected > 0 {
			allFreeVehicles[i].Status = "RENT"
			if result := db.Db.Save(&allFreeVehicles[i]); result.Error != nil {
				log.Print(result.Error)
				return
			}
			log.Print(allFreeVehicles[i].ID, " RENT")
			continue
		}

		//ransom
		var ransomOrder bt.RansomOrder
		if result := db.Db.Model(&bt.RansomOrder{}).Find(&ransomOrder, "is_active = true AND vehicles_id = ?", allFreeVehicles[i].ID); result.Error != nil {
			log.Print(result.Error)
			return
		} else if result.RowsAffected > 0 {
			allFreeVehicles[i].Status = "RANSOM"
			if result := db.Db.Save(&allFreeVehicles[i]); result.Error != nil {
				log.Print(result.Error)
				return
			}
			log.Print(allFreeVehicles[i].ID, " RANSOM")
			continue
		}

	}

}
