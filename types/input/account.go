package models

type InputCreateAccount struct {
	Name  string `json:"name"`
	Token string `json:"token"`
}

type InputEditAccount struct {
	Name     string `json:"name"`
	NewToken string `json:"new_token"`
}

type InputGetAccount struct {
	Token string `json:"token"`
}
