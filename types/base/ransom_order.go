package models

import (
	"time"

	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type RansomOrder struct {
	ID                          int64          `json:"id" gorm:"primarykey"`
	UID                         string         `json:"uid"`
	ExtID                       int64          `json:"ext_id"`
	AccountsID                  int64          `json:"accounts_id"`
	ManagersID                  int64          `json:"managers_id"`
	UsersID                     int64          `json:"users_id"`
	VehiclesID                  int64          `json:"vehicles_id"`
	TariffsID                   int64          `json:"tariffs_id"`
	ContractDate                time.Time      `json:"contract_date"`
	ContractDateEnd             *time.Time     `json:"contract_date_end"`
	ContractDateEndDescription  *string        `json:"contract_date_end_description"`
	Status                      string         `json:"status"  default:"PRELIMINARY" example:"PRELIMINARY/RESERVE/SUCCESS/CANCEL"`
	IsActive                    bool           `json:"is_active"`
	IsMandatoryPaymentsIncluded bool           `json:"is_mandatory_payments_included"`
	IsInitialPay                bool           `json:"is_initial_pay"`
	WriteOffPriority            datatypes.JSON `json:"write_off_priority"`
	DepositPayout               float64        `json:"deposit_payout"`
	DKPAmount                   float64        `json:"dkp_amount"`
	AdditionalEquipment         float64        `json:"additional_equipment"`
	DayPayment                  float64        `json:"day_payment"`
	RansomTariff                *RansomTariff  `json:"ransom_tariffs,omitempty" gorm:"-"`
	User                        *User          `json:"user,omitempty" gorm:"-"`
	Manager                     *Manager       `json:"manager,omitempty" gorm:"-"`
	Vehicle                     *Vehicle       `json:"vehicle,omitempty" gorm:"-"`
	UserFullName                *string        `json:"user_full_name,omitempty" gorm:"-"`
	VehicleFullName             *string        `json:"vehicle_full_name,omitempty" gorm:"-"`
	UserUID                     *string        `json:"user_uid,omitempty" gorm:"-"`
	VehicleUID                  *string        `json:"vehicle_uid,omitempty" gorm:"-"`
	LastPayment                 *interface{}   `json:"last_payment,omitempty" gorm:"-"`
	Bill                        *Bill          `json:"bill,omitempty" gorm:"-"`
	CreatedAt                   time.Time      `json:"created_at"`
	UpdatedAt                   time.Time      `json:"updated_at"`
	DeletedAt                   gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}

type RansomOrderFromSearch struct {
	RansomOrderID       int64   `json:"ransom_order_id"`
	RansomOrderUID      string  `json:"ransom_order_uid"`
	RansomUsersID       int64   `json:"ransom_users_id"`
	RansomUsersUID      string  `json:"ransom_users_uid"`
	VehiclesUID         string  `json:"vehicles_uid"`
	VehiclesNumber      string  `json:"vehicles_number"`
	RansomUsersPhone    string  `json:"ransom_users_phone"`
	RansomUsersFullname string  `json:"ransom_users_fullname"`
	VehiclesFullname    string  `json:"vehicles_fullname"`
	DayPayment          float64 `json:"day_payment"`

	Term          int     `json:"term"`
	YearRate      float64 `json:"year_rate"`
	InsuranceRate int     `json:"insurance_rate"`
	MonthPayment  float64 `json:"month_payment"`
	InitialFee    int     `json:"initial_fee"`
	VehicleAmount float64 `json:"vehicle_amount"`

	YearMandatoryAmount       float64 `json:"year_mandatory_amount"`
	YearMandatoryPayoutAmount float64 `json:"year_mandatory_payout_amount"`

	ExtID                       int64          `json:"ext_id"`
	AccountsID                  int64          `json:"accounts_id"`
	ManagersID                  int64          `json:"managers_id"`
	UsersID                     int64          `json:"users_id"`
	VehiclesID                  int64          `json:"vehicles_id"`
	TariffsID                   int64          `json:"tariffs_id"`
	ContractDate                time.Time      `json:"contract_date"`
	ContractEndAt               *time.Time     `json:"contract_end_at"`
	ContractEndDescription      *string        `json:"contract_end_description"`
	Status                      string         `json:"status" example:"PRELIMINARY/RESERVE/SUCCESS/CANCEL"`
	IsActive                    bool           `json:"is_active"`
	IsMandatoryPaymentsIncluded bool           `json:"is_mandatory_payments_included"`
	IsInitialPay                bool           `json:"is_initial_pay"`
	WriteOffPriority            datatypes.JSON `json:"write_off_priority"`
	DepositPayout               float64        `json:"deposit_payout"`
	DKPAmount                   float64        `json:"dkp_amount"`
	AdditionalEquipment         float64        `json:"additional_equipment"`
	LastPayment                 *interface{}   `json:"last_payment"`
	Bill                        *Bill          `json:"bill"`
	CreatedAt                   time.Time      `json:"created_at"`
	UpdatedAt                   time.Time      `json:"updated_at"`
	DeletedAt                   gorm.DeletedAt `json:"deleted_at" swaggertype:"string"`
}

type PaginationRansomOrder struct {
	Page       int            `json:"page"`
	Items      *[]RansomOrder `json:"items"`
	TotalItems int64          `json:"total_items"`
	TotalPages int            `json:"total_pages"`
}
