package database

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"os"
	"time"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esutil"
)

const (
	numberOfWorker = 2
	flushBytes     = 5 * 1024 * 1024
	flushInterval  = 10 * time.Second
)

var (
	esClient *elasticsearch.Client
)

func ElasticSearchStart() error {

	//retryBackoff := backoff.NewExponentialBackOff()

	cfg := elasticsearch.Config{
		Addresses: []string{
			"http://" + os.Getenv("DBHOST") + ":" + os.Getenv("ELASTICSEARCHPORT"),
		},
		// Retry on 429 TooManyRequests statuses
		//
		RetryOnStatus: []int{502, 503, 504, 429},

		// Configure the backoff function
		//
		//RetryBackoff: func(i int) time.Duration {
		//	if i == 1 {
		//		retryBackoff.Reset()
		//	}
		//	return retryBackoff.NextBackOff()
		//},

		MaxRetries: 5,
	}
	var err error
	esClient, err = elasticsearch.NewClient(cfg)
	if err != nil {
		esClient = nil
		return err
	}

	if _, err := esClient.Ping(); err != nil {
		esClient = nil
		return err
	}

	log.Println(elasticsearch.Version)
	log.Println(esClient.Info())

	return nil
}

func ESAddEdit(index string, data interface{}) error {
	if esClient == nil {
		return errors.New("ElasitcSearch not inited")
	}
	_, err := esClient.Index(index, esutil.NewJSONReader(&data))
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func ESBulkCreateEdit(command string, data interface{}) {

	//bi, err := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
	//	Index:         os.Getenv("ELASTICSEARCHINDEX"), // The default index name
	//	Client:        esClient,                       	// The Elasticsearch client
	//	NumWorkers:    numberOfWorker,                  // The number of worker goroutines
	//	FlushBytes:    flushBytes,                      // The flush threshold in bytes
	//	FlushInterval: flushInterval,                   // The periodic flush interval
	//})

	//for _, a := range all {
	//
	//	data, err := json.Marshal(a)
	//	if err != nil {
	//		log.Print("Cannot encode bill %d: %s", a.ID, err)
	//		continue
	//	}
	//
	//	err = bi.Add(
	//		context.Background(),
	//		esutil.BulkIndexerItem{
	//			// Action field configures the operation to perform (index, create, delete, update)
	//			Action: "index",
	//
	//			// DocumentID is the (optional) document ID
	//			DocumentID: strconv.FormatUint(a.ID, 10),
	//
	//			// Body is an `io.Reader` with the payload
	//			Body: bytes.NewReader(data),
	//
	//			// OnSuccess is called for each successful operation
	//			OnSuccess: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem) {
	//				atomic.AddUint64(&countSuccessful, 1)
	//			},
	//
	//			// OnFailure is called for each failed operation
	//			OnFailure: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem, err error) {
	//				if err != nil {
	//					log.Printf("ERROR: %s", err)
	//				} else {
	//					log.Printf("ERROR: %s: %s", res.Error.Type, res.Error.Reason)
	//				}
	//			},
	//		},
	//	)
	//	if err != nil {
	//		log.Print("Unexpected error: %s", err)
	//		continue
	//	}
	//}
	//
	//if err := bi.Close(context.Background()); err != nil {
	//	log.Fatalf("Unexpected error: %s", err)
	//}
	//
	//biStats := bi.Stats()
	//
	//
	//if biStats.NumFailed > 0 {
	//	log.Fatalf(
	//		"Indexed [%s] documents with [%s] errors in %s (%s docs/sec)",
	//		humanize.Comma(int64(biStats.NumFlushed)),
	//		humanize.Comma(int64(biStats.NumFailed)),
	//		dur.Truncate(time.Millisecond),
	//		humanize.Comma(int64(1000.0/float64(dur/time.Millisecond)*float64(biStats.NumFlushed))),
	//	)
	//} else {
	//	log.Printf(
	//		"Sucessfuly indexed [%s] documents in %s (%s docs/sec)",
	//		humanize.Comma(int64(biStats.NumFlushed)),
	//		dur.Truncate(time.Millisecond),
	//		humanize.Comma(int64(1000.0/float64(dur/time.Millisecond)*float64(biStats.NumFlushed))),
	//	)
	//}
	//

}

func ESPrintAll() error {

	res, err := esClient.Search(
		esClient.Search.WithContext(context.Background()),
		esClient.Search.WithIndex("rent_sevice-user"),
		esClient.Search.WithTrackTotalHits(true),
		esClient.Search.WithPretty(),
		//esClient.Search.WithQuery("*t"),
	)
	if err != nil {
		return errors.New("Error getting response: " + err.Error())
	}
	defer res.Body.Close()
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			return errors.New("Error parsing the response body: " + err.Error())
		} else {
			// Print the response status and error information.
			return errors.New("[" + res.Status() + "]" + e["error"].(map[string]interface{})["type"].(string) + ": " + e["error"].(map[string]interface{})["reason"].(string))
		}
	}
	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		return errors.New("Error parsing the response body: " + err.Error())
	}

	// Print the response status, number of results, and request duration.
	log.Printf(
		"[%s] %d hits; took: %dms",
		res.Status(),
		int(r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)),
		int(r["took"].(float64)),
	)
	// Print the ID and document source for each hit.
	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		log.Printf(" * ID=%s, %s", hit.(map[string]interface{})["_id"], hit.(map[string]interface{})["_source"])
	}
	return nil
}
