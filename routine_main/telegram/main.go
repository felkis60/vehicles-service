package main

import (
	"log"

	startup "gitlab.com/felkis60/vehicles-service/init"
	"gitlab.com/felkis60/vehicles-service/routines"

	db "gitlab.com/felkis60/vehicles-service/database"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
)

func main() {
	startup.SystemStartup(true, true, false)

	var account bt.Account
	if result := db.Db.First(&account, "name = 'REX'"); result.RowsAffected == 0 {
		log.Println("ERROR: SYNC: No rex account!")
		return
	}

	routines.PostReminders(account.ID)
	routines.FindOsagoVehiclesAndRemind(account.ID)
}
