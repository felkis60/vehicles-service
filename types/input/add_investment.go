package models

type InputAddInvestment struct {
	Title          *string  `json:"title"`
	Description    *string  `json:"description"`
	Type           *string  `json:"Type"`
	Amount         *float64 `json:"amount"`
	DayAmount      *float64 `json:"day_amount"`
	RelationsID    *int64   `json:"relations_id"`
	RelationsTable *string  `json:"relations_table"`
	Date           *string  `json:"date"`
	DateEnd        *string  `json:"date_end"`
}

type AddInvestmentFilters struct {
	RelationsID    *int64  `json:"relations_id"`
	RelationsUID   *string `json:"relations_uid"`
	RelationsTable *string `json:"relations_table"`
}

type InputGetListAddInvestment struct {
	Page     int                  `json:"page"`
	PageSize int                  `json:"page_size"`
	OrderBy  string               `json:"order_by"`
	OrderDir string               `json:"order_direction" example:"asc|desc"`
	Filters  AddInvestmentFilters `json:"filters"`
}
