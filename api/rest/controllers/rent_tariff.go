package controllers

import (
	"gitlab.com/felkis60/vehicles-service/repository"
	_ "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type RentTariffController struct {
	Repository repository.RentTariffRepository
}

// @Summary Create rent tariff
// @Tags Rent Tariffs
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param   _     body it.InputCreateRentTariff true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-tariffs/ [post]
func (ctrl *RentTariffController) Create(c *gin.Context) {
	var input it.InputCreateRentTariff
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Delete rent tariff
// @Tags Rent Tariffs
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Rent Tariff UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-tariffs/{uid}/delete [post]
func (ctrl *RentTariffController) Delete(c *gin.Context) {

	data, err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get one rent tariff
// @Tags Rent Tariffs
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Rent Tariff UID"
// @Success 200 {object} _.RentTariff
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-tariffs/{uid} [post]
func (ctrl *RentTariffController) GetByUID(c *gin.Context) {

	rentTariff, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string), true)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: rentTariff})
}

// @Summary Get rent tariffs
// @Tags Rent Tariffs
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetRentTariffList true "Request body"
// @Success 200 {object} []_.RentTariff
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-tariffs/list [post]
func (ctrl *RentTariffController) List(c *gin.Context) {
	var input it.InputGetRentTariffList
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}

// @Summary Edit rent tariff
// @Tags Rent Tariffs
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputEditRentTariff true "Request body"
// @Param uid path string true "Rent Tariff UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-tariffs/{uid}/edit [post]
func (ctrl *RentTariffController) Edit(c *gin.Context) {
	var input it.InputEditRentTariff
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), &input, c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}
