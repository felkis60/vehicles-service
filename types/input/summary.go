package models

type InputGetPeriodSummary struct {
	DateStart *string `json:"date_start" example:"YYYY-MM-DD"`
	DateEnd   *string `json:"date_end" example:"YYYY-MM-DD"`
}
