package models

import (
	"time"

	db "gitlab.com/felkis60/vehicles-service/database/elastic_search"

	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type User struct {
	ID                   int64          `json:"id" gorm:"primarykey"`
	UID                  string         `json:"uid"`
	ExtID                int64          `json:"ext_id"`
	AccountsID           int64          `json:"accounts_id"`
	FirstName            string         `json:"first_name"`
	LastName             string         `json:"last_name"`
	MiddleName           string         `json:"middle_name"`
	Phone                string         `json:"phone" gorm:"index"`
	FirebaseUID          string         `json:"firebase_uid"`
	Firebase             datatypes.JSON `json:"firebase" swaggertype:"string"`
	PartnerType          string         `json:"partner_type" default:"PHYSICAL"`
	PassportNumber       string         `json:"passport_number"`
	PassportSeries       string         `json:"passport_series"`
	PassportIssuedBy     string         `json:"passport_issued_by"`
	PassportDateOfIssue  time.Time      `json:"passport_date_of_issue"`
	PassportPlaceOfIssue string         `json:"passport_place_of_issue"`
	Birthday             time.Time      `json:"birthday"`
	Email                string         `json:"email"`
	ContactPerson        string         `json:"contact_person"`
	Comment              string         `json:"comment"`
	AddressOfLiving      string         `json:"address_of_living"`
	LegalAddress         string         `json:"legal_address"`
	INN                  string         `json:"inn"`
	OGRN                 string         `json:"ogrn"`
	Bank                 string         `json:"bank"`
	BIK                  string         `json:"bik"`
	KPP                  string         `json:"kpp"`
	CheckingBill         string         `json:"checking_bill"`
	CorrespondentBill    string         `json:"correspondent_bill"`

	RentOrders   *[]RentOrder   `json:"rent_orders,omitempty" gorm:"-"`
	RansomOrders *[]RansomOrder `json:"ransom_orders,omitempty" gorm:"-"`
	Bill         *Bill          `json:"bill,omitempty" gorm:"-"`
	LastPayment  *interface{}   `json:"last_payment,omitempty" gorm:"-"`

	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}

//func (u *User) AfterCreate(tx *gorm.DB) (err error) {
//
//	db.ESAddEdit("rent_sevice-user", u)
//
//	return
//}

func (u *User) AfterSave(tx *gorm.DB) (err error) {

	db.ESAddEdit("rent_sevice-user", u)

	return
}
