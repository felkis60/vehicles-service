package routines

import (
	"log"
	"strconv"

	db "gitlab.com/felkis60/vehicles-service/database"
	logs "gitlab.com/felkis60/vehicles-service/log"
	p "gitlab.com/felkis60/vehicles-service/providers"
	"gitlab.com/felkis60/vehicles-service/repository"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"
)

func syncUsers(accountID int64) {

	log.Print("INFO: User sync started")

	users, err := p.RexProvider.UserList()
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Print("INFO: Users recived: " + strconv.Itoa(len(*users)))

	var tempCreated = 0
	var tempRep = repository.UserRepository{Db: db.Db}

	for _, us := range *users {
		var tempUser bt.User
		if result := db.Db.Find(&tempUser, "ext_id = ?", us.ID); result.RowsAffected > 0 {
			continue
		}

		var tempInput it.InputCreateUser
		us.ToInputCreateUser(&tempInput)

		if _, err := tempRep.Create(&tempInput, accountID, 0); err == nil {
			tempCreated++
		} else {
			log.Print("ERROR: " + err.Error())
		}

	}

	log.Print("INFO: Users created: " + strconv.Itoa(tempCreated))
	log.Println(logs.INFUsersSyncedREX)
}
