package startup

import (
	"log"
	"path"
	"runtime"

	db "gitlab.com/felkis60/vehicles-service/database"
	//es "gitlab.com/felkis60/vehicles-service/database/elastic_search"
	l "gitlab.com/felkis60/vehicles-service/log"

	"github.com/joho/godotenv"
)

var (
	mainDBInited        = false
	envInited           = false
	logsInited          = false
	elasticSearchInited = false
	natsInited          = false
)

func SystemStartup(notTestEnv_ bool, logs_ bool, elasticSearch_ bool) {

	if logs_ {
		if !logsInited {
			l.StartLogs()
			logsInited = true
		}
	}

	if !envInited {
		if notTestEnv_ {
			if err := godotenv.Load("configs/.env"); err != nil {
				log.Fatal("ERROR: " + err.Error())
			}
		} else {
			_, filename, _, _ := runtime.Caller(0)
			dir := path.Join(path.Dir(filename), "..")
			log.Print(dir)
			if err := godotenv.Load(dir + "/configs/t.env"); err != nil {
				log.Fatal("ERROR: " + err.Error())
			}
		}
		envInited = true
	}

	if !mainDBInited {
		db.Start()
		db.Migrate()
		mainDBInited = true
	}

	if elasticSearch_ {
		//	if !elasticSearchInited {
		//		if err := es.ElasticSearchStart(); err != nil {
		//			//log.Fatal("ERROR: " + err.Error())
		//		}
		//	}
	}

	if notTestEnv_ {
		db.InitRedis()
	}

	//if !natsInited {
	//	if nats_ {
	//		if err := natsH.Init(); err != nil {
	//			log.Fatal("ERROR: " + err.Error())
	//		}
	//		natsInited = true
	//	}
	//}

}
