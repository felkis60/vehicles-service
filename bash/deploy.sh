#!/bin/bash

docker build -f dockerfile.rest -t vehicles-service-rest . --network=host
docker build -f dockerfile.grpc -t vehicles-service-grpc . --network=host
docker stop vehicles-service-rest || true
docker stop vehicles-service-grpc || true
docker rm vehicles-service-rest || true
docker rm vehicles-service-grpc || true
docker run --network host -d -p 6302:6302 -v `pwd`:/srv/app --name vehicles-service-rest vehicles-service-rest
docker run --network host -d -p 6303:6303 -v `pwd`:/srv/app --name vehicles-service-grpc vehicles-service-grpc