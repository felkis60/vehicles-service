package repository

import (
	"errors"
	"log"
	"time"

	"gitlab.com/felkis60/vehicles-service/helpers/history"
	logs "gitlab.com/felkis60/vehicles-service/log"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

const (
	managerTableName = "managers"
)

type ManagerRepository struct {
	Db *gorm.DB
}

func writeCreateManager(data *bt.Manager, input *it.InputCreateManager) error {
	data.ExtID = input.ExtID
	data.FirstName = input.FirstName
	data.LastName = input.LastName
	data.MiddleName = input.MiddleName
	data.Phone = helpers.CutPhone(helpers.CleanPhone(input.Phone))
	data.Email = input.Email
	if input.Birthday != "" {
		layout := "2006-01-02"
		tempBirth, err := time.Parse(layout, input.Birthday)
		if err != nil {
			log.Print(err.Error())
		} else {
			data.Birthday = tempBirth
		}
	}
	return checkManager(data)
}

func writeEditManager(data *bt.Manager, input *it.InputEditManager) error {
	if input.ExtID != nil {
		data.ExtID = *input.ExtID
	}
	if input.FirstName != nil {
		data.FirstName = *input.FirstName
	}
	if input.LastName != nil {
		data.LastName = *input.LastName
	}
	if input.MiddleName != nil {
		data.MiddleName = *input.MiddleName
	}
	if input.Phone != nil {
		data.Phone = helpers.CutPhone(helpers.CleanPhone(*input.Phone))
	}
	if input.Email != nil {
		data.Email = *input.Email
	}
	if input.Birthday != nil {
		if *input.Birthday != "" {
			layout := "2006-01-02"
			tempBirth, err := time.Parse(layout, *input.Birthday)
			if err != nil {
				log.Print(err.Error())
			} else {
				data.Birthday = tempBirth
			}
		}
	}
	return checkManager(data)
}

func checkManager(data *bt.Manager) error {
	if data.FirstName == "" {
		return errors.New(logs.ErrFirstNameReq)
	}
	if data.LastName == "" {
		return errors.New(logs.ErrLastNameReq)
	}
	if data.Phone == "" {
		return errors.New(logs.ERRPhoneReq)
	}
	if data.Email == "" {
		return errors.New(logs.ErrEmailReq)
	}
	return nil
}

func (rep *ManagerRepository) Create(input *it.InputCreateManager, accountID int64, authUserID int64) (*bt.Manager, error) {

	var data bt.Manager

	writeCreateManager(&data, input)

	if result := rep.Db.Find(&data, "phone = ? AND accounts_id = ?", data.Phone, accountID); result.RowsAffected != 0 {
		return nil, errors.New(logs.ERRManagerExistingPhoNum)
	}

	data.UID = uuid.Must(uuid.NewV4()).String()
	data.AccountsID = accountID

	rep.Db.Create(&data)

	if err := history.CreateHistory(data.ID, managerTableName, "create", nil, &data, authUserID, accountID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return &data, nil

}

func (rep *ManagerRepository) Edit(uid string, accountToken string, input *it.InputEditManager, authUserID int64) (*bt.Manager, error) {

	data, err := rep.GetByUID(uid, accountToken)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	var managers []bt.Manager
	if result := rep.Db.Find(&managers, "phone = ? AND id != ? AND accounts_id = ?", input.Phone, data.ID, data.AccountsID); result.RowsAffected != 0 {
		return nil, errors.New(logs.ERRExistingPhoNum)
	}

	writeEditManager(data, input)

	rep.Db.Save(data)

	if err := history.CreateHistory(data.ID, managerTableName, "edit", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil

}

func (rep *ManagerRepository) Delete(uid string, accountToken string, authUserID int64) (*bt.Manager, error) {

	data, err := rep.GetByUID(uid, accountToken)
	if err != nil {
		return nil, err
	}

	var orderCount = int64(0)
	if result := helpers.DbAccountLeftJoin("ransom_orders", accountToken, rep.Db.Model(&bt.RansomOrder{})).Where("ransom_orders.managers_id = ? AND ransom_orders.is_active = true", data.ID).Count(&orderCount); result.Error != nil {
		return nil, err
	} else if orderCount > 0 {
		return nil, errors.New(logs.ERRAttachedData)
	}
	if result := helpers.DbAccountLeftJoin("rent_orders", accountToken, rep.Db.Model(&bt.RentOrder{})).Where("rent_orders.managers_id = ? AND rent_orders.is_active = true", data.ID).Count(&orderCount); result.Error != nil {
		return nil, err
	} else if orderCount > 0 {
		return nil, errors.New(logs.ERRAttachedData)
	}

	var dataBefore = *data

	rep.Db.Delete(data)

	if err := history.CreateHistory(data.ID, managerTableName, "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil

}

func (rep *ManagerRepository) GetByUID(UID string, accountToken string) (*bt.Manager, error) {
	var data bt.Manager
	if result := helpers.DbAccountLeftJoin(managerTableName, accountToken, rep.Db).Find(&data, "uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchManagersUID)
	}

	var ransOrders []map[string]interface{}
	var rentOrders []map[string]interface{}

	rep.Db.Table("ransom_orders").Select("ransom_orders.*, CONCAT(u.last_name, ' ', u.first_name, ' ', u.middle_name) AS user_full_name, CONCAT(v.mark, ' ', v.model, ' ', v.number) AS vehicle_full_name, u.uid as user_uid, v.uid as vehicle_uid").Joins("LEFT JOIN users AS u ON ransom_orders.users_id = u.id ").Joins("LEFT JOIN vehicles AS v ON ransom_orders.vehicles_id = v.id").Where("ransom_orders.managers_id = ?", data.ID).Find(&ransOrders)
	rep.Db.Table("rent_orders").Select("rent_orders.*, CONCAT(u.last_name, ' ', u.first_name, ' ', u.middle_name) AS user_full_name, CONCAT(v.mark, ' ', v.model, ' ', v.number) AS vehicle_full_name, u.uid as user_uid, v.uid as vehicle_uid").Joins("LEFT JOIN users AS u ON rent_orders.users_id = u.id").Joins("LEFT JOIN vehicles AS v ON rent_orders.vehicles_id = v.id").Where("rent_orders.managers_id = ?", data.ID).Find(&rentOrders)

	data.RansomOrders = &ransOrders
	data.RentOrders = &rentOrders

	return &data, nil
}

func (rep *ManagerRepository) List(accountToken string, input *it.InputGetListManagers) (*helpers.Pagination, error) {

	var tx = helpers.DbAccountLeftJoin(managerTableName, accountToken, rep.Db.Model(&bt.Manager{}))

	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.Manager
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, managerTableName, &data)(tx)

}

func (rep *ManagerRepository) History(UID string, accountToken string, input *it.InputGetHistoryList) (*helpers.Pagination, error) {

	//Getting base model data
	dataByUID, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return nil, err
	}

	//Filters
	tx := rep.Db.Model(&bt.History{}).Where("source_table = ? AND source_id = ?", managerTableName, dataByUID.ID)

	if input.Filters.OperationType != nil {
		tx = tx.Where("operation_type = ?", *input.Filters.OperationType)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.History
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, "histories", &data)(tx)
}
