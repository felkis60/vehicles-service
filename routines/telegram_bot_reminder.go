package routines

import (
	"log"
	"math"
	"os"
	"strconv"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	db "gitlab.com/felkis60/vehicles-service/database"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
)

func PostReminders(accountID int64) error {

	bot, err := tgbotapi.NewBotAPI(os.Getenv("telegram_bot_token"))
	if err != nil {
		return err
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	var stringChatID = os.Getenv("telegram_chat_id")
	chatID, err := strconv.ParseInt(stringChatID, 10, 64)
	if err != nil {
		return err
	}

	var reminders []bt.Reminder
	db.Db.Find(&reminders, "accounts_id = ? AND closed = false AND ? >= date", accountID, time.Now().Add(time.Hour*72))

	for i := range reminders {
		var vehicle *bt.Vehicle
		var message string
		if reminders[i].VehiclesID > 0 {
			db.Db.Find(&vehicle, "id = ?", reminders[i].VehiclesID)
			if vehicle != nil {
				message = "Уведомление по \"" + vehicle.Mark + " " + vehicle.Model + " " + vehicle.Number + "\": "
			}
		}
		message += reminders[i].Text
		msg := tgbotapi.NewMessage(chatID, message)
		if _, err := bot.Send(msg); err != nil {
			log.Print("ERROR: " + err.Error())
			continue
		} else {
			// reminders[i].Closed = true  // TODO: Узнать нужно ли закрывать после отправки
			// db.Db.Save(&reminders[i])
		}
	}

	return nil
}

func FindOsagoVehiclesAndRemind(accountID int64) error {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("telegram_bot_token"))
	if err != nil {
		return err
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	var stringChatID = os.Getenv("telegram_chat_id")
	chatID, err := strconv.ParseInt(stringChatID, 10, 64)
	if err != nil {
		return err
	}

	var tempNow = time.Now()
	var vehicles []bt.Vehicle
	var fullMessage string
	db.Db.Find(&vehicles, "accounts_id = ? AND date_osago_end IS NOT NULL AND ? >= date_osago_end AND ? <= date_osago_end", accountID, tempNow.Add(time.Hour*168), tempNow)
	for i := range vehicles {
		var vehicleFullName = "\"" + vehicles[i].Mark + " " + vehicles[i].Model + " " + vehicles[i].Number + "\""
		var daysLeft = int64(0)
		if vehicles[i].DateOsagoEnd != nil {
			daysLeft = int64(math.Ceil(vehicles[i].DateOsagoEnd.Sub(tempNow).Hours() / 24.0))
		}
		fullMessage += "У " + vehicleFullName + " истекает осаго через " + strconv.FormatInt(daysLeft, 10) + " дней!\n"

	}

	if fullMessage != "" {
		msg := tgbotapi.NewMessage(chatID, fullMessage)
		if _, err := bot.Send(msg); err != nil {
			log.Print("ERROR: " + err.Error())
		}
	}

	return nil
}
