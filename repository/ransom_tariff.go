package repository

import (
	"errors"
	"log"
	"time"

	"gitlab.com/felkis60/vehicles-service/helpers/history"
	logs "gitlab.com/felkis60/vehicles-service/log"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

const (
	ransomTariifTableName = "ransom_tariffs"
)

type RansomTariffRepository struct {
	Db *gorm.DB
}

func (rep *RansomTariffRepository) Create(input *it.InputCreateRansomTariff, accountID int64, authUserID int64) (*bt.RansomTariff, error) {

	var data bt.RansomTariff

	var account bt.Account
	rep.Db.First(&account, accountID)

	var vehRep = VehicleRepository{Db: rep.Db}
	vehicle, err := vehRep.GetByUID(input.VehiclesUID, account.Token, false)
	if err != nil {
		return nil, err
	}

	data.UID = uuid.Must(uuid.NewV4()).String()
	data.ExtID = input.ExtID
	data.AccountsID = accountID
	data.VehiclesID = vehicle.ID
	data.InitialFee = input.InitialFee
	data.VehicleAmount = input.VehicleAmount
	data.TrackerAmount = input.TrackerAmount
	data.VehicleTaxAmount = input.VehicleTaxAmount
	data.RegistrationAmount = input.RegistrationAmount
	data.InsuranceOsagoAmount = input.InsuranceOsagoAmount
	data.Term = input.Term
	data.YearRate = input.YearRate
	data.InsuranceRate = input.InsuranceRate
	data.MonthPayment = input.MonthPayment
	data.DayPayment = input.DayPayment
	data.TotalVehicleAmount = input.TotalVehicleAmount
	data.TotalVehicleWithoutInsuranceAmount = input.TotalVehicleWithoutInsuranceAmount
	data.TrackerServiceAmount = input.TrackerServiceAmount
	data.SourceTrackerMonthAmount = input.SourceTrackerMonthAmount

	if input.CreatedAt != nil {
		if tempCreatedAt, err := time.Parse("2006-01-02 15:04:05", *input.CreatedAt); err == nil {
			data.CreatedAt = tempCreatedAt
		} else {
			log.Print(err)
		}
	}

	rep.Db.Create(&data)

	if err := history.CreateHistory(data.ID, ransomTariifTableName, "create", nil, &data, authUserID, accountID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return &data, nil
}

func (rep *RansomTariffRepository) Edit(uid string, input *it.InputEditRansomTariff, accountToken string, authUserID int64) (*bt.RansomTariff, error) {

	data, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	if input.ExtID != nil {
		data.ExtID = *input.ExtID
	}
	if input.InitialFee != nil {
		data.InitialFee = *input.InitialFee
	}
	if input.VehicleAmount != nil {
		data.VehicleAmount = *input.VehicleAmount
	}
	if input.TrackerAmount != nil {
		data.TrackerAmount = *input.TrackerAmount
	}
	if input.VehicleTaxAmount != nil {
		data.VehicleTaxAmount = *input.VehicleTaxAmount
	}
	if input.RegistrationAmount != nil {
		data.RegistrationAmount = *input.RegistrationAmount
	}
	if input.InsuranceOsagoAmount != nil {
		data.InsuranceOsagoAmount = *input.InsuranceOsagoAmount
	}
	if input.Term != nil {
		data.Term = *input.Term
	}
	if input.YearRate != nil {
		data.YearRate = *input.YearRate
	}
	if input.InsuranceRate != nil {
		data.InsuranceRate = *input.InsuranceRate
	}
	if input.MonthPayment != nil {
		data.MonthPayment = *input.MonthPayment
	}
	if input.DayPayment != nil {
		data.DayPayment = *input.DayPayment
	}
	if input.TotalVehicleAmount != nil {
		data.TotalVehicleAmount = *input.TotalVehicleAmount
	}
	if input.TotalVehicleWithoutInsuranceAmount != nil {
		data.TotalVehicleWithoutInsuranceAmount = *input.TotalVehicleWithoutInsuranceAmount
	}
	if input.TrackerServiceAmount != nil {
		data.TrackerServiceAmount = *input.TrackerServiceAmount
	}
	if input.SourceTrackerMonthAmount != nil {
		data.SourceTrackerMonthAmount = *input.SourceTrackerMonthAmount
	}

	rep.Db.Save(data)

	var allConnectedOrders []bt.RansomOrder
	if result := helpers.DbAccountLeftJoin("ransom_orders", accountToken, rep.Db.Model(&bt.RansomOrder{})).Find(&allConnectedOrders, "ransom_orders.tariffs_id = ?", data.ID); result.Error != nil {
		log.Print(result.Error)
	}
	//for i := range allConnectedOrders {
	//	tools.CalculateRansomOrderDayPayment(&allConnectedOrders[i], data)
	//	if result := rep.Db.Save(&allConnectedOrders[i]); result.Error != nil {
	//		log.Print(result.Error)
	//	}
	//}

	if err := history.CreateHistory(data.ID, ransomTariifTableName, "edit", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {

		log.Print("ERROR: " + err.Error())
	}

	return data, nil
}

func (rep *RansomTariffRepository) Delete(uid string, accountToken string, authUserID int64) (*bt.RansomTariff, error) {
	data, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var orderCount = int64(0)
	if result := helpers.DbAccountLeftJoin("ransom_orders", accountToken, rep.Db.Model(&bt.RansomOrder{})).Where("ransom_orders.tariffs_id = ? AND ransom_orders.is_active = true", data.ID).Count(&orderCount); result.Error != nil {
		return nil, err
	} else if orderCount > 0 {
		return nil, errors.New(logs.ERRAttachedData)
	}

	var dataBefore = *data

	rep.Db.Delete(data)

	if err := history.CreateHistory(data.ID, ransomTariifTableName, "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil
}

func (rep *RansomTariffRepository) GetByUID(UID string, accountToken string, withInternalData bool) (*bt.RansomTariff, error) {
	var data bt.RansomTariff
	if result := helpers.DbAccountLeftJoin(ransomTariifTableName, accountToken, rep.Db).Find(&data, "uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchTariffsUID)
	}
	if withInternalData {
		var tempVehicle bt.Vehicle
		if result := rep.Db.Find(&tempVehicle, data.VehiclesID); result.RowsAffected != 0 {
			data.Vehicle = &tempVehicle
		}
	}
	return &data, nil
}

func (rep *RansomTariffRepository) List(accountToken string, input *it.InputGetRansomTariffList) (*helpers.Pagination, error) {

	//Additional data
	var tempVehicle *bt.Vehicle
	var err error
	if input.Filters.VehiclesUID != nil {
		var tempRep = VehicleRepository{Db: rep.Db}
		tempVehicle, err = tempRep.GetByUID(*input.Filters.VehiclesUID, accountToken, false)
		if err != nil {
			return nil, err
		}
	}

	//Filters
	var tx = helpers.DbAccountLeftJoin(ransomTariifTableName, accountToken, rep.Db.Model(&bt.RansomTariff{}))

	if tempVehicle != nil {
		tx = tx.Where("vehicles_id = ?", tempVehicle.ID)
	}

	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.RansomTariff
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, ransomTariifTableName, &data)(tx)
}

func (rep *RansomTariffRepository) History(UID string, accountToken string, input *it.InputGetHistoryList) (*helpers.Pagination, error) {

	//Getting base model data
	dataByUID, err := rep.GetByUID(UID, accountToken, false)
	if err != nil {
		return nil, err
	}

	//Filters
	tx := rep.Db.Model(&bt.History{}).Where("source_table = ? AND source_id = ?", ransomTariifTableName, dataByUID.ID)

	if input.Filters.OperationType != nil {
		tx = tx.Where("operation_type = ?", *input.Filters.OperationType)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.History
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, "histories", &data)(tx)
}
