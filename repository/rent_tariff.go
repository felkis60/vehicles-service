package repository

import (
	"errors"
	"log"

	"gitlab.com/felkis60/vehicles-service/helpers/history"
	logs "gitlab.com/felkis60/vehicles-service/log"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

const (
	rentTariffTableName = "rent_tariffs"
)

type RentTariffRepository struct {
	Db *gorm.DB
}

func (rep *RentTariffRepository) checkRentTariff(data *bt.RentTariff, accountToken string) error {

	if data.VehiclesID == 0 {
		return errors.New(logs.ERRVehicleUIDReq)
	}

	if data.ToDays > 365 {
		data.ToDays = 365
	}
	if data.FromDays > data.ToDays {
		return errors.New("ERROR: 'to_days' must be greater than 'from_days'")
	}

	//var intersectTariffs []bt.RentTariff
	//if result := db.DbDaysIntersect(data.FromDays, data.ToDays, db.DbAccountLeftJoin(rentTariffTableName, accountToken, rep.Db), "from_days", "to_days").Where("vehicles_id = ? AND rent_tariffs.id != ?", data.VehiclesID, data.ID).Find(&intersectTariffs); result.RowsAffected != 0 {
	//	return errors.New(logs.ERRRentTariffIntersect)
	//}

	return nil
}

func (rep *RentTariffRepository) Create(input *it.InputCreateRentTariff, accountID int64, authUserID int64) (*bt.RentTariff, error) {

	var data bt.RentTariff

	var account bt.Account
	rep.Db.First(&account, accountID)

	var vehRep = VehicleRepository{Db: rep.Db}
	vehicle, err := vehRep.GetByUID(input.VehiclesUID, account.Token, false)
	if err != nil {
		return nil, err
	}

	data.UID = uuid.Must(uuid.NewV4()).String()
	data.ExtID = input.ExtID
	data.AccountsID = accountID
	data.VehiclesID = vehicle.ID
	data.FromDays = input.FromDays
	data.ToDays = input.ToDays
	data.DayPayment = input.DayPayment

	if err := rep.checkRentTariff(&data, account.Token); err != nil {
		return nil, err
	}

	rep.Db.Create(&data)

	if err := history.CreateHistory(data.ID, rentTariffTableName, "create", nil, &data, authUserID, accountID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return &data, nil
}

func (rep *RentTariffRepository) Edit(uid string, input *it.InputEditRentTariff, accountToken string, authUserID int64) (*bt.RentTariff, error) {

	data, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	if input.ToDays != nil {
		data.ToDays = *input.ToDays
	}
	if input.FromDays != nil {
		data.FromDays = *input.FromDays
	}
	if input.ExtID != nil {
		data.ExtID = *input.ExtID
	}
	if input.DayPayment != nil {
		data.DayPayment = *input.DayPayment
	}

	if err := rep.checkRentTariff(data, accountToken); err != nil {
		return nil, err
	}

	rep.Db.Save(data)

	if err := history.CreateHistory(data.ID, rentTariffTableName, "edit", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil
}

func (rep *RentTariffRepository) Delete(uid string, accountToken string, authUserID int64) (*bt.RentTariff, error) {

	data, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var orderCount = int64(0)
	if result := helpers.DbAccountLeftJoin("rent_orders", accountToken, rep.Db.Model(&bt.RentOrder{})).Where("rent_orders.tariffs_id = ? AND rent_orders.is_active = true", data.ID).Count(&orderCount); result.Error != nil {
		return nil, err
	} else if orderCount > 0 {
		return nil, errors.New(logs.ERRAttachedData)
	}

	var dataBefore = *data

	rep.Db.Delete(data)

	if err := history.CreateHistory(data.ID, rentTariffTableName, "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil
}

func (rep *RentTariffRepository) GetByUID(UID string, accountToken string, withInternalData bool) (*bt.RentTariff, error) {
	var data bt.RentTariff
	if result := helpers.DbAccountLeftJoin(rentTariffTableName, accountToken, rep.Db).Find(&data, "uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchTariffsUID)
	}
	if withInternalData {
		var tempVehicle bt.Vehicle
		if result := rep.Db.Find(&tempVehicle, data.VehiclesID); result.RowsAffected != 0 {
			data.Vehicle = &tempVehicle
		}
	}

	return &data, nil
}

func (rep *RentTariffRepository) List(accountToken string, input *it.InputGetRentTariffList) (*helpers.Pagination, error) {

	var tempVehicle *bt.Vehicle
	var err error
	if input.Filters.VehiclesUID != nil {
		var tempRep = VehicleRepository{Db: rep.Db}
		tempVehicle, err = tempRep.GetByUID(*input.Filters.VehiclesUID, accountToken, false)
		if err != nil {
			return nil, err
		}
	}

	var tx = helpers.DbAccountLeftJoin(rentTariffTableName, accountToken, rep.Db.Model(&bt.RentTariff{}))

	//Filters
	if tempVehicle != nil {
		tx = tx.Where("vehicles_id = ?", tempVehicle.ID)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.RentTariff
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, rentTariffTableName, &data)(tx)

}

func (rep *RentTariffRepository) History(UID string, accountToken string, input *it.InputGetHistoryList) (*helpers.Pagination, error) {

	//Getting base model data
	dataByUID, err := rep.GetByUID(UID, accountToken, false)
	if err != nil {
		return nil, err
	}

	//Filters
	tx := rep.Db.Model(&bt.History{}).Where("source_table = ? AND source_id = ?", rentTariffTableName, dataByUID.ID)

	if input.Filters.OperationType != nil {
		tx = tx.Where("operation_type = ?", *input.Filters.OperationType)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.History
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, "histories", &data)(tx)
}
