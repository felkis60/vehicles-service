package routines

import (
	"log"
	"strconv"

	db "gitlab.com/felkis60/vehicles-service/database"
	logs "gitlab.com/felkis60/vehicles-service/log"
	p "gitlab.com/felkis60/vehicles-service/providers"
	"gitlab.com/felkis60/vehicles-service/repository"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"
)

func syncVehicles(accountID int64) {

	log.Print("INFO: Vehicle sync started")

	vehicles, err := p.RexProvider.VehicleList()
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Print("INFO: Vehicles recived: " + strconv.Itoa(len(*vehicles)))

	var tempCreated = 0
	var tempRep = repository.VehicleRepository{Db: db.Db}
	for _, veh := range *vehicles {
		var tempVehicle bt.Vehicle
		if result := db.Db.Find(&tempVehicle, "ext_id = ? AND accounts_id = ?", veh.ID, accountID); result.RowsAffected > 0 {
			continue
		}
		if result := db.Db.Find(&tempVehicle, "number ILIKE ? AND accounts_id = ?", veh.Number, accountID); result.RowsAffected > 0 {
			if !helpers.FindInt64(int64(veh.ID), tempVehicle.OtherExtIDs) {
				tempVehicle.OtherExtIDs = append(tempVehicle.OtherExtIDs, int64(veh.ID))
				db.Db.Save(&tempVehicle)
			}
		} else {
			var tempInput it.InputCreateVehicle
			veh.ToInputCreateVehicle(&tempInput)

			if _, err := tempRep.Create(&tempInput, accountID, 0); err != nil {
				log.Print("ERROR: " + err.Error())
			} else {
				tempCreated++
			}
		}

	}

	log.Print("INFO: Vehicles created: " + strconv.Itoa(tempCreated))
	log.Println(logs.INFVehiclesSyncedREX)
}

func unloadingToRentLandingProvider() {
	var vehicles []bt.Vehicle
	db.Db.Find(&vehicles)

	for _, veh := range vehicles {
		landVehicle, err := p.RentProvider.Get(veh.UID)
		if err != nil {
			log.Println(err.Error())
			continue
		}
		if landVehicle == nil {
			landVehicle = &bt.RentLandingVehicle{}
		}

		bt.VehicleToRentLandingVehicle(&veh, landVehicle)

		if landVehicle.ID != 0 {

			if err = p.RentProvider.Edit(landVehicle); err != nil {
				log.Println(err.Error())
				continue
			}

		} else {

			if err = p.RentProvider.Create(landVehicle); err != nil {
				log.Println(err.Error())
				continue
			}

		}

	}

}
