package repository

import (
	"encoding/json"
	"log"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"
	tools "gitlab.com/felkis60/vehicles-service/helpers"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	"gorm.io/gorm"
)

type SysInfoRepository struct {
	Db *gorm.DB
}

func (rep *SysInfoRepository) GetTotalInfo(accountToken string) (*bt.FullSysInfo, error) {
	var data bt.FullSysInfo
	if result := helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db.Model(&bt.Vehicle{}).Select("COUNT(*)")).Find(&data.TotalVehicles, vehTableName+".deleted_at IS NULL"); result.Error != nil {
		return nil, result.Error
	}

	if result := helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, rep.Db.Model(&bt.RansomOrder{}).Select("COUNT(*)")).Find(&data.TotalActiveRansomOrders, ransomOrderTableName+".deleted_at IS NULL AND "+ransomOrderTableName+".is_active IS TRUE"); result.Error != nil {
		return nil, result.Error
	}

	if result := helpers.DbAccountLeftJoin(rentOrderTableName, accountToken, rep.Db.Model(&bt.RentOrder{}).Select("COUNT(*)")).Find(&data.TotalActiveRentOrders, rentOrderTableName+".deleted_at IS NULL AND "+rentOrderTableName+".is_active IS TRUE"); result.Error != nil {
		return nil, result.Error
	}

	var statusCountSlice []struct {
		VehStatus string `json:"veh_status"`
		Total     int64  `json:"total"`
	}
	if result := helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db.Model(&bt.Vehicle{}).Select("vehicles.status as veh_status, COUNT(vehicles.status) as total")).Group("vehicles.status").Scan(&statusCountSlice); result.Error != nil {
		return nil, result.Error
	}
	data.TotalVehiclesByStatuses = make(map[string]int64)
	for i := range statusCountSlice {
		data.TotalVehiclesByStatuses[statusCountSlice[i].VehStatus] = statusCountSlice[i].Total
	}

	if result := helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, rep.Db.Model(&bt.RansomOrder{}).Select("COALESCE(SUM (rt.total_vehicle_amount), 0)")).Joins("LEFT JOIN ransom_tariffs AS rt ON ransom_orders.tariffs_id = rt.id").Where(ransomOrderTableName + ".deleted_at IS NULL AND " + ransomOrderTableName + ".is_active IS TRUE").Scan(&data.SumOfTotalVehicleAmountActive); result.Error != nil {
		return nil, result.Error
	}

	if result := helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, rep.Db.Model(&bt.RansomOrder{}).Select("COALESCE(SUM (rt.total_vehicle_amount), 0)")).Joins("LEFT JOIN ransom_tariffs AS rt ON ransom_orders.tariffs_id = rt.id").Where(ransomOrderTableName + ".deleted_at IS NULL ").Scan(&data.SumOfTotalVehicleAmount); result.Error != nil {
		return nil, result.Error
	}

	var ordersTariffs []bt.RansomTariff
	if result := helpers.DbAccountLeftJoin(ransomTariifTableName, accountToken, rep.Db.Model(&bt.RansomTariff{})).Joins("INNER JOIN ransom_orders AS ro ON ro.tariffs_id = ransom_tariffs.id").Where("ro.deleted_at IS NULL").Find(&ordersTariffs); result.Error != nil {
		return nil, result.Error
	}

	for i := range ordersTariffs {
		var tempTermDays = ((float64(ordersTariffs[i].Term) / 12.0) * 365)
		if tempTermDays != 0 {
			data.PlannedIncomeAmount += (ordersTariffs[i].TotalVehicleWithoutInsuranceAmount - tools.InvestAmount(&ordersTariffs[i])) / tempTermDays
		}
	}

	if result := helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, rep.Db.Model(&bt.RansomOrder{}).Select("ransom_orders.uid")).Where(ransomOrderTableName + ".deleted_at IS NULL AND " + ransomOrderTableName + ".is_active IS TRUE").Scan(&data.ActiveRansomOrdersUIDs); result.Error != nil {
		return nil, result.Error
	}

	if result := helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, rep.Db.Model(&bt.RansomOrder{}).Select("ransom_orders.uid")).Where(ransomOrderTableName + ".deleted_at IS NULL").Scan(&data.RansomOrdersUIDs); result.Error != nil {
		return nil, result.Error
	}

	if result := helpers.DbAccountLeftJoin(rentOrderTableName, accountToken, rep.Db.Model(&bt.RentOrder{}).Select("rent_orders.uid")).Where("rent_orders.deleted_at IS NULL AND rent_orders.is_active IS TRUE").Scan(&data.ActiveRentOrdersUIDs); result.Error != nil {
		return nil, result.Error
	}

	if result := helpers.DbAccountLeftJoin(rentOrderTableName, accountToken, rep.Db.Model(&bt.RentOrder{}).Select("rent_orders.uid")).Where("rent_orders.deleted_at IS NULL").Scan(&data.RentOrdersUIDs); result.Error != nil {
		return nil, result.Error
	}

	return &data, nil
}

func (rep *SysInfoRepository) GetInfoByDate(accountToken string, accountID int64, input *it.InputGetSysInfoByDate) (*bt.SysInfoByDate, error) {

	if input.DateStart != nil {
		if err := helpers.CheckSimpleDateString(*input.DateStart); err != nil {
			return nil, err
		}
	}

	if input.DateEnd != nil {
		if err := helpers.CheckSimpleDateString(*input.DateEnd); err != nil {
			return nil, err
		}
	}

	txV := helpers.DbAccountLeftJoin(vehTableName, accountToken, rep.Db.Model(&bt.Vehicle{}).Select("COUNT(*)").Where(vehTableName+".deleted_at IS NULL"))
	txRanO := helpers.DbAccountLeftJoin(ransomOrderTableName, accountToken, rep.Db.Model(&bt.RansomOrder{}).Select("COUNT(*)").Where(ransomOrderTableName+".deleted_at IS NULL"))
	txRenO := helpers.DbAccountLeftJoin(rentOrderTableName, accountToken, rep.Db.Model(&bt.RentOrder{}).Select("COUNT(*)").Where(rentOrderTableName+".deleted_at IS NULL"))
	txH := helpers.DbAccountLeftJoin("histories", accountToken, rep.Db.Model(&bt.History{}))
	txNewCompOrders := helpers.DbAccountLeftJoin("histories", accountToken, rep.Db.Model(&bt.History{})).Where("(histories.source_table = 'rent_order' OR histories.source_table = 'ransom_order')")

	txH = txH.Where("histories.operation_type = 'edit' AND histories.source_table = 'vehicles'").Where("CAST(histories.before::json->>'accounts_id' AS INTEGER) = ?", accountID)
	txH = txH.Where("histories.before::json->>'status' != histories.after::json->>'status'")

	if input.DateStart != nil {
		txV = txV.Where(vehTableName+".created_at >= '?'::date", *input.DateStart)
		txRanO = txRanO.Where(ransomOrderTableName+".contract_date_end >= '?'::date", *input.DateStart)
		txRenO = txRenO.Where(ransomOrderTableName+".date_end >= '?'::date", *input.DateStart)
		txH = txH.Where("histories.created_at >= '?'::date", *input.DateStart)
		txNewCompOrders = txV.Where(vehTableName+".created_at >= '?'::date", *input.DateStart)
	}

	if input.DateEnd != nil {
		txV = txV.Where(vehTableName+".created_at <= '?'::date", *input.DateEnd)
		txRanO = txRanO.Where(ransomOrderTableName+".contract_date_end <= '?'::date", *input.DateEnd)
		txRenO = txRenO.Where(ransomOrderTableName+".date_end <= '?'::date", *input.DateEnd)
		txH = txH.Where("histories.created_at <= '?'::date", *input.DateEnd)
		txNewCompOrders = txH.Where("histories.created_at <= '?'::date", *input.DateEnd)
	}

	txH = txH.Order("histories.created_at asc")
	txNewCompOrders = txNewCompOrders.Where("(COALESCE(CAST(histories.before::jsonb->'is_active' AS bool), false) != CAST(histories.after::jsonb->'is_active' AS bool))")

	var selectedVehiclesHistory []bt.History
	if result := txH.Scan(&selectedVehiclesHistory); result.Error != nil {
		return nil, result.Error
	}

	var vehiclesStatusesMap = make(map[int64]string)
	for i := range selectedVehiclesHistory {
		var tempVehicle bt.Vehicle
		var err = json.Unmarshal(selectedVehiclesHistory[i].After, &tempVehicle)
		if err != nil {
			log.Print(err)
			continue
		}
		vehiclesStatusesMap[tempVehicle.ID] = tempVehicle.Status
	}

	var data bt.SysInfoByDate
	data.TotalVehiclesStatuses = make(map[string]int64)
	for i := range vehiclesStatusesMap {
		data.TotalVehiclesStatuses[vehiclesStatusesMap[i]]++
	}

	if result := txV.Scan(&data.TotalCreatedVehicles); result.Error != nil {
		return nil, result.Error
	}
	if result := txRanO.Scan(&data.TotalCompletedRansomOrders); result.Error != nil {
		return nil, result.Error
	}
	if result := txRenO.Scan(&data.TotalCompletedRentOrders); result.Error != nil {
		return nil, result.Error
	}
	if result := txNewCompOrders.Count(&data.TotalNewAndCompletedOrders); result.Error != nil {
		return nil, result.Error
	}

	return &data, nil
}
