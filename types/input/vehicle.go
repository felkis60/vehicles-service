package models

import (
	"gorm.io/datatypes"
)

type InputCreateVehicle struct {
	ExtID               int64                    `json:"ext_id"`
	Number              string                   `json:"number"`
	Mark                string                   `json:"mark"`
	Model               string                   `json:"model"`
	Type                string                   `json:"type"`
	Sts                 string                   `json:"sts"`
	Win                 string                   `json:"win"`
	Width               *int                     `json:"widht"`
	Height              *int                     `json:"height"`
	Depth               *int                     `json:"depth"`
	Capacity            *float32                 `json:"capacity"`
	FuelTank            *int                     `json:"fuel_tank"`
	SummerRate          *float32                 `json:"summer_rate"`
	WinterRate          *float32                 `json:"winter_rate"`
	AvatarUri           string                   `json:"avatar_uri"`
	Ransom              bool                     `json:"ransom"`
	Description         string                   `json:"description"`
	Bodywork            string                   `json:"bodywork"` //Кузов
	Color               string                   `json:"color"`    //цвет
	CustomFields        datatypes.JSON           `json:"custom_fields" swaggertype:"string"`
	Drive               string                   `json:"drive"` //Привод
	Engine              string                   `json:"engine"`
	Kpp                 string                   `json:"kpp"` //Коробка передач
	Owner               string                   `json:"owner"`
	Salon               string                   `json:"salon"`
	Year                string                   `json:"year"`
	DefaultRansomTariff *InputCreateRansomTariff `json:"default_ransom_tariff"`
	Status              string                   `json:"status"`
	DKPAmount           float64                  `json:"dkp_amount"`
	DateOsagoStart      *string                  `json:"date_osago_start" example:"YYYY-MM-DD"`
	DateOsagoEnd        *string                  `json:"date_osago_end" example:"YYYY-MM-DD"`
	CreatedAt           *string                  `json:"created_at"`

	BuyDate            *string  `json:"buy_date" example:"YYYY-MM-DD"`             // Дата покупки
	BuyAmount          *float64 `json:"buy_amount"`                                // Цена покупки
	RegistrationDate   *string  `json:"registration_date" example:"YYYY-MM-DD"`    // Дата постановки на учет
	TrackerInstallDate *string  `json:"tracker_install_date" example:"YYYY-MM-DD"` // Дата установки трекера
	TrackerNumber      *string  `json:"tracker_number"`                            // Номер трекера
	LighthouseNumber   *string  `json:"lighthouse_number"`                         // Номер маяка
}

type InputEditVehicle struct {
	ExtID          *int64          `json:"ext_id"`
	AvatarUri      *string         `json:"avatar_uri"`
	Number         *string         `json:"number,omitempty"`
	Mark           *string         `json:"mark,omitempty"`
	Model          *string         `json:"model,omitempty"`
	Type           *string         `json:"type,omitempty"`
	Sts            *string         `json:"sts,omitempty"`
	Win            *string         `json:"win,omitempty"`
	Width          *int            `json:"widht,omitempty"`
	Height         *int            `json:"height,omitempty"`
	Depth          *int            `json:"depth,omitempty"`
	Capacity       *float32        `json:"capacity,omitempty"`
	FuelTank       *int            `json:"fuel_tank,omitempty"`
	SummerRate     *float32        `json:"summer_rate,omitempty"`
	WinterRate     *float32        `json:"winter_rate,omitempty"`
	Ransom         *bool           `json:"ransom"`
	Description    *string         `json:"description,omitempty"`
	Bodywork       *string         `json:"bodywork"` //Кузов
	Color          *string         `json:"color"`
	CustomFields   *datatypes.JSON `json:"custom_fields" swaggertype:"string"`
	Drive          *string         `json:"drive"` //Привод
	Engine         *string         `json:"engine"`
	Kpp            *string         `json:"kpp"` //Коробка передач
	Owner          *string         `json:"owner"`
	Salon          *string         `json:"salon"`
	Year           *string         `json:"year"`
	Status         *string         `json:"status"`
	DateOsagoStart *string         `json:"date_osago_start" example:"YYYY-MM-DD"`
	DateOsagoEnd   *string         `json:"date_osago_end" example:"YYYY-MM-DD"`
	DKPAmount      *float64        `json:"dkp_amount"`

	DkpEnterFiles  *datatypes.JSON `json:"dkp_enter_files" swaggertype:"string"`
	StsFiles       *datatypes.JSON `json:"sts_files" swaggertype:"string"`
	OsagoFiles     *datatypes.JSON `json:"osago_files" swaggertype:"string"`
	DutyFiles      *datatypes.JSON `json:"duty_files" swaggertype:"string"`
	ContractsFiles *datatypes.JSON `json:"contracts_files" swaggertype:"string"`
	OtherFiles     *datatypes.JSON `json:"other_files" swaggertype:"string"`
	ImagesFiles    *datatypes.JSON `json:"images_files" swaggertype:"string"`

	BuyDate            *string  `json:"buy_date" example:"YYYY-MM-DD"`             // Дата покупки
	BuyAmount          *float64 `json:"buy_amount"`                                // Цена покупки
	RegistrationDate   *string  `json:"registration_date" example:"YYYY-MM-DD"`    // Дата постановки на учет
	TrackerInstallDate *string  `json:"tracker_install_date" example:"YYYY-MM-DD"` // Дата установки трекера
	TrackerNumber      *string  `json:"tracker_number"`                            // Номер трекера
	LighthouseNumber   *string  `json:"lighthouse_number"`                         // Номер маяка
}
