package main

import (
	"bytes"
	"encoding/json"
	"math"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	routes "gitlab.com/felkis60/vehicles-service/api/rest/router"
	db "gitlab.com/felkis60/vehicles-service/database"
	tools "gitlab.com/felkis60/vehicles-service/helpers"
	start "gitlab.com/felkis60/vehicles-service/init"
	logs "gitlab.com/felkis60/vehicles-service/log"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
	convey "github.com/smartystreets/goconvey/convey"
)

var router *gin.Engine

const rightToken = "123"
const rightToken2 = "1234"

var inited = false

func makeRequest(method string, url string, accountToken string, body interface{}, additionalHeaders *map[string]string) (int, *helpers.RestRespone) {
	var resp helpers.RestRespone
	w := httptest.NewRecorder()

	var marshaledBody []byte
	var req *http.Request
	var err error
	if body != nil {
		marshaledBody, err = json.Marshal(body)
		if err != nil {
			return 0, nil
		}
		req, err = http.NewRequest(method, url, bytes.NewReader(marshaledBody))
		if err != nil {
			return 0, nil
		}
	} else {
		req, err = http.NewRequest(method, url, nil)
		if err != nil {
			return 0, nil
		}
	}
	req.Header.Set("Token", accountToken)
	if additionalHeaders != nil {
		for key, value := range *additionalHeaders {
			req.Header.Set(key, value)
		}
	}
	router.ServeHTTP(w, req)
	err = json.Unmarshal(w.Body.Bytes(), &resp)
	if err != nil {
		return 0, nil
	}

	return w.Code, &resp
}

func setup() {
	if !inited {
		start.SystemStartup(false, true, false)
		router = routes.SetupRouter()
		inited = true

	}

	var result int
	db.Db.Raw("DELETE FROM accounts").Scan(&result)
	db.Db.Raw("DELETE FROM users").Scan(&result)
	db.Db.Raw("DELETE FROM rent_orders").Scan(&result)
	db.Db.Raw("DELETE FROM rent_tariffs").Scan(&result)
	db.Db.Raw("DELETE FROM ransom_orders").Scan(&result)
	db.Db.Raw("DELETE FROM ransom_tariffs").Scan(&result)
	db.Db.Raw("DELETE FROM vehicles").Scan(&result)
	db.Db.Raw("DELETE FROM managers").Scan(&result)
	db.Db.Raw("DELETE FROM reminders").Scan(&result)

	db.Db.Create(&bt.Account{Name: "test", Token: rightToken})
	db.Db.Create(&bt.Account{Name: "test2", Token: rightToken2})

}

func TestAll(t *testing.T) {
	convey.Convey("Vehicles-Service Tests", t, func() {

		setup()

		convey.Convey("Try create all", func() {
			//User
			code, resp := makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", LastName: "tamp", Phone: "123454321"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var user bt.User
			db.Db.Last(&user)

			//Manager
			code, resp = makeRequest("POST", "/v1/managers/", rightToken, &it.InputCreateManager{FirstName: "temp", LastName: "tamp", Phone: "tomp", Email: "timp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var manager bt.Manager
			db.Db.Last(&manager)

			//Vehicles
			code, resp = makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp", Mark: "tamp", Model: "tomp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var vehicle1 bt.Vehicle
			db.Db.Last(&vehicle1)

			code, resp = makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp1", Mark: "tamp1", Model: "tomp1"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var vehicle2 bt.Vehicle
			db.Db.Last(&vehicle2)

			//Rent tariffs
			code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle1.UID, DayPayment: 100, FromDays: 3, ToDays: 10}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentTariff11 bt.RentTariff
			db.Db.Last(&rentTariff11)

			code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle1.UID, DayPayment: 85, FromDays: 11, ToDays: 20}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentTariff12 bt.RentTariff
			db.Db.Last(&rentTariff12)

			code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle2.UID, DayPayment: 100, FromDays: 2, ToDays: 10}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentTariff21 bt.RentTariff
			db.Db.Last(&rentTariff21)

			code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle2.UID, DayPayment: 85, FromDays: 11, ToDays: 20}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentTariff22 bt.RentTariff
			db.Db.Last(&rentTariff22)

			//Ransom tariffs
			code, resp = makeRequest("POST", "/v1/ransom-tariffs/", rightToken, &it.InputCreateRansomTariff{VehiclesUID: vehicle1.UID, DayPayment: 100, VehicleAmount: 555, TrackerAmount: 666, TrackerServiceAmount: 777, VehicleTaxAmount: 888, TotalVehicleAmount: 99999, TotalVehicleWithoutInsuranceAmount: 99, Term: 10, YearRate: 50}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var ransomTariff11 bt.RansomTariff
			db.Db.Last(&ransomTariff11)

			code, resp = makeRequest("POST", "/v1/ransom-tariffs/", rightToken, &it.InputCreateRansomTariff{VehiclesUID: vehicle1.UID, DayPayment: 85, VehicleAmount: 555, TrackerAmount: 666, TrackerServiceAmount: 777, VehicleTaxAmount: 888, TotalVehicleAmount: 99999, TotalVehicleWithoutInsuranceAmount: 99, Term: 10, YearRate: 50}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var ransomTariff12 bt.RansomTariff
			db.Db.Last(&ransomTariff12)

			code, resp = makeRequest("POST", "/v1/ransom-tariffs/", rightToken, &it.InputCreateRansomTariff{VehiclesUID: vehicle2.UID, DayPayment: 100, VehicleAmount: 555, TrackerAmount: 666, TrackerServiceAmount: 777, VehicleTaxAmount: 888, TotalVehicleAmount: 99999, TotalVehicleWithoutInsuranceAmount: 99, Term: 10, YearRate: 50}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var ransomTariff21 bt.RansomTariff
			db.Db.Last(&ransomTariff21)

			code, resp = makeRequest("POST", "/v1/ransom-tariffs/", rightToken, &it.InputCreateRansomTariff{VehiclesUID: vehicle2.UID, DayPayment: 85, VehicleAmount: 555, TrackerAmount: 666, TrackerServiceAmount: 777, VehicleTaxAmount: 888, TotalVehicleAmount: 99999, TotalVehicleWithoutInsuranceAmount: 99, Term: 10, YearRate: 50}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var ransomTariff22 bt.RansomTariff
			db.Db.Last(&ransomTariff22)

			//Rent orders
			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 2, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle1.UID, UsersUID: user.UID, TariffsUID: rentTariff11.UID, IsActive: true}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentOrder11 bt.RentOrder
			db.Db.Last(&rentOrder11)
			convey.So(rentOrder11.IsActive, convey.ShouldEqual, true)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 2, 2, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 3, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle1.UID, UsersUID: user.UID, TariffsUID: rentTariff12.UID, IsActive: true}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentOrder12 bt.RentOrder
			db.Db.Last(&rentOrder12)
			convey.So(rentOrder12.IsActive, convey.ShouldEqual, true)
			db.Db.Find(&rentOrder11, "uid = ?", rentOrder11.UID)
			convey.So(rentOrder11.IsActive, convey.ShouldEqual, false)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 2, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle2.UID, UsersUID: user.UID, TariffsUID: rentTariff21.UID, IsActive: true}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentOrder21 bt.RentOrder
			db.Db.Last(&rentOrder21)
			convey.So(rentOrder21.IsActive, convey.ShouldEqual, true)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 2, 2, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 3, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle2.UID, UsersUID: user.UID, TariffsUID: rentTariff22.UID, IsActive: true}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentOrder22 bt.RentOrder
			db.Db.Last(&rentOrder22)
			convey.So(rentOrder22.IsActive, convey.ShouldEqual, true)
			db.Db.Find(&rentOrder21, "uid = ?", rentOrder21.UID)
			convey.So(rentOrder21.IsActive, convey.ShouldEqual, false)

			db.Db.Find(&rentOrder11, "uid = ?", rentOrder11.UID)
			convey.So(rentOrder11.IsActive, convey.ShouldEqual, false)
			db.Db.Find(&rentOrder12, "uid = ?", rentOrder12.UID)
			convey.So(rentOrder12.IsActive, convey.ShouldEqual, true)

			//Ransom orders
			contractDateEnd1 := time.Date(2000, 2, 1, 0, 0, 0, 0, time.UTC)
			//contractDateEnd2 := time.Date(2000, 3, 1, 0, 0, 0, 0, time.UTC)
			code, resp = makeRequest("POST", "/v1/ransom-orders/", rightToken, &it.InputCreateRansomOrder{ContractDate: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), ContractDateEnd: &contractDateEnd1, ManagersUID: manager.UID, VehiclesUID: vehicle1.UID, UsersUID: user.UID, TariffsUID: ransomTariff11.UID, IsActive: true}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var ransomOrder11 bt.RansomOrder
			db.Db.Last(&ransomOrder11)
			convey.So(ransomOrder11.IsActive, convey.ShouldEqual, true)

			//code, resp = makeRequest("POST", "/v1/ransom-orders/", rightToken, &it.InputCreateRansomOrder{ContractDate: time.Date(2000, 2, 2, 0, 0, 0, 0, time.UTC), ContractDateEnd: &contractDateEnd2, ManagersUID: manager.UID, VehiclesUID: vehicle1.UID, UsersUID: user.UID, TariffsUID: ransomTariff12.UID, IsActive: true}, nil)
			//convey.So(code, convey.ShouldEqual, 200)
			//var ransomOrder12 bt.RansomOrder
			//db.Db.Last(&ransomOrder12)
			//convey.So(ransomOrder12.IsActive, convey.ShouldEqual, true)
			//db.Db.Find(&ransomOrder11, "uid = ?", ransomOrder11.UID)
			//convey.So(ransomOrder11.IsActive, convey.ShouldEqual, false)

			code, resp = makeRequest("POST", "/v1/ransom-orders/", rightToken, &it.InputCreateRansomOrder{ContractDate: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), ContractDateEnd: &contractDateEnd1, ManagersUID: manager.UID, VehiclesUID: vehicle2.UID, UsersUID: user.UID, TariffsUID: ransomTariff21.UID, IsActive: true}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var ransomOrder21 bt.RansomOrder
			db.Db.Last(&ransomOrder21)
			convey.So(ransomOrder21.IsActive, convey.ShouldEqual, true)

			//code, resp = makeRequest("POST", "/v1/ransom-orders/", rightToken, &it.InputCreateRansomOrder{ContractDate: time.Date(2000, 2, 2, 0, 0, 0, 0, time.UTC), ContractDateEnd: &contractDateEnd2, ManagersUID: manager.UID, VehiclesUID: vehicle2.UID, UsersUID: user.UID, TariffsUID: ransomTariff22.UID, IsActive: true}, nil)
			//convey.So(code, convey.ShouldEqual, 200)
			//var ransomOrder22 bt.RansomOrder
			//db.Db.Last(&ransomOrder22)
			//convey.So(ransomOrder22.IsActive, convey.ShouldEqual, true)
			//db.Db.Find(&ransomOrder21, "uid = ?", ransomOrder21.UID)
			//convey.So(ransomOrder21.IsActive, convey.ShouldEqual, false)
			convey.Convey("Try edit all", func() {

				//User
				var tempExtID = int64(100)
				var tempFirstName = "temp100"
				var tempLastName = "timp100"
				var tempMiddleName = "tomp100"
				var tempPhone = "9999900009"
				code, resp = makeRequest("POST", "/v1/users/"+user.UID+"/edit", rightToken, &it.InputEditUser{ExtID: &tempExtID, FirstName: &tempFirstName, LastName: &tempLastName, MiddleName: &tempMiddleName, Phone: &tempPhone}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Last(&user, "uid = ?", user.UID)
				convey.So(user.FirstName, convey.ShouldEqual, tempFirstName)
				convey.So(user.LastName, convey.ShouldEqual, tempLastName)
				convey.So(user.MiddleName, convey.ShouldEqual, tempMiddleName)
				convey.So(user.ExtID, convey.ShouldEqual, tempExtID)
				convey.So(user.Phone, convey.ShouldEqual, tempPhone)

				//Manager
				var tempEmail = "asdf@gmail.com"
				var tempBirthday = "2000-01-01"
				code, resp = makeRequest("POST", "/v1/managers/"+manager.UID+"/edit", rightToken, &it.InputEditManager{ExtID: &tempExtID, FirstName: &tempFirstName, LastName: &tempLastName, MiddleName: &tempMiddleName, Phone: &tempPhone, Email: &tempEmail, Birthday: &tempBirthday}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Last(&manager, "uid = ?", manager.UID)
				convey.So(manager.FirstName, convey.ShouldEqual, tempFirstName)
				convey.So(manager.LastName, convey.ShouldEqual, tempLastName)
				convey.So(manager.MiddleName, convey.ShouldEqual, tempMiddleName)
				convey.So(manager.ExtID, convey.ShouldEqual, tempExtID)
				convey.So(manager.Phone, convey.ShouldEqual, tempPhone)
				convey.So(manager.Email, convey.ShouldEqual, tempEmail)

				//Vehicle
				var tempAvatarUri = "url"
				var tempNumber = "g100ut"
				var tempMark = "FromSoftware"
				var tempModel = "Elden Ring"
				var tempType = "Sosalik"
				code, resp = makeRequest("POST", "/v1/vehicles/"+vehicle1.UID+"/edit", rightToken, &it.InputEditVehicle{ExtID: &tempExtID, AvatarUri: &tempAvatarUri, Number: &tempNumber, Mark: &tempMark, Model: &tempModel, Type: &tempType}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Last(&vehicle1, "uid = ?", vehicle1.UID)
				convey.So(vehicle1.AvatarUri, convey.ShouldEqual, tempAvatarUri)
				convey.So(vehicle1.Number, convey.ShouldEqual, tempNumber)
				convey.So(vehicle1.Mark, convey.ShouldEqual, tempMark)
				convey.So(vehicle1.ExtID, convey.ShouldEqual, tempExtID)
				convey.So(vehicle1.Model, convey.ShouldEqual, tempModel)
				convey.So(vehicle1.Type, convey.ShouldEqual, tempType)

				//Rent tariff
				var tempFromDays = 30
				var tempToDays = 40
				var tempDayPayment = 98765.0
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff11.UID+"/edit", rightToken, &it.InputEditRentTariff{ExtID: &tempExtID, DayPayment: &tempDayPayment, FromDays: &tempFromDays, ToDays: &tempToDays}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Last(&rentTariff11, "uid = ?", rentTariff11.UID)
				convey.So(rentTariff11.ExtID, convey.ShouldEqual, tempExtID)
				convey.So(rentTariff11.DayPayment, convey.ShouldEqual, tempDayPayment)
				convey.So(rentTariff11.FromDays, convey.ShouldEqual, tempFromDays)
				convey.So(rentTariff11.ToDays, convey.ShouldEqual, tempToDays)

				//Ransom tariff
				var tempInitialFee = 9999
				var tempVehicleAmount = 9999.0
				var tempTrackerAmount = 9999.1
				var tempTrackerServiceAmount = 9999.2
				var tempVehicleTaxAmount = 9999.3
				var tempRegistrationAmount = 9999.4
				var tempInsuranceOsagoAmount = 9999.5
				var tempTerm = 99999
				var tempYearRate = 999999.0
				var tempInsuranceRate = 9999999
				var tempMonthPayment = 9999.6
				var tempTotalVehicleAmount = 9999.8
				var tempTotalVehicleWithoutInsuranceAmount = 9999.9
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff11.UID+"/edit", rightToken, &it.InputEditRansomTariff{InitialFee: &tempInitialFee, VehicleAmount: &tempVehicleAmount, TrackerAmount: &tempTrackerAmount, TrackerServiceAmount: &tempTrackerServiceAmount, VehicleTaxAmount: &tempVehicleTaxAmount, TotalVehicleAmount: &tempTotalVehicleAmount, TotalVehicleWithoutInsuranceAmount: &tempTotalVehicleWithoutInsuranceAmount, Term: &tempTerm, YearRate: &tempYearRate, RegistrationAmount: &tempRegistrationAmount, InsuranceOsagoAmount: &tempInsuranceOsagoAmount, InsuranceRate: &tempInsuranceRate, MonthPayment: &tempMonthPayment, DayPayment: &tempDayPayment}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Last(&ransomTariff11, "uid = ?", ransomTariff11.UID)
				convey.So(ransomTariff11.DayPayment, convey.ShouldEqual, tempDayPayment)
				convey.So(ransomTariff11.InitialFee, convey.ShouldEqual, tempInitialFee)
				convey.So(ransomTariff11.VehicleAmount, convey.ShouldEqual, tempVehicleAmount)
				convey.So(ransomTariff11.TrackerAmount, convey.ShouldEqual, tempTrackerAmount)
				convey.So(ransomTariff11.TrackerServiceAmount, convey.ShouldEqual, tempTrackerServiceAmount)
				convey.So(ransomTariff11.VehicleTaxAmount, convey.ShouldEqual, tempVehicleTaxAmount)
				convey.So(ransomTariff11.TotalVehicleAmount, convey.ShouldEqual, tempTotalVehicleAmount)
				convey.So(ransomTariff11.TotalVehicleWithoutInsuranceAmount, convey.ShouldEqual, tempTotalVehicleWithoutInsuranceAmount)
				convey.So(ransomTariff11.Term, convey.ShouldEqual, tempTerm)
				convey.So(ransomTariff11.YearRate, convey.ShouldEqual, tempYearRate)
				convey.So(ransomTariff11.RegistrationAmount, convey.ShouldEqual, tempRegistrationAmount)
				convey.So(ransomTariff11.InsuranceOsagoAmount, convey.ShouldEqual, tempInsuranceOsagoAmount)
				convey.So(ransomTariff11.InsuranceRate, convey.ShouldEqual, tempInsuranceRate)
				convey.So(ransomTariff11.MonthPayment, convey.ShouldEqual, tempMonthPayment)

				//Rent orders
				var tempDateStart = time.Date(2000, 1, 15, 0, 0, 0, 0, time.UTC)
				var tempDateEnd = time.Date(2000, 1, 25, 0, 0, 0, 0, time.UTC)
				var tempTrue = true
				var tempFalse = false
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder11.UID+"/edit", rightToken, &it.InputEditRentOrder{DateStart: &tempDateStart, DateEnd: &tempDateEnd, IsActive: &tempTrue}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Find(&rentOrder11, "uid = ?", rentOrder11.UID)
				convey.So(rentOrder11.IsActive, convey.ShouldEqual, true)
				db.Db.Find(&rentOrder12, "uid = ?", rentOrder12.UID)
				convey.So(rentOrder12.IsActive, convey.ShouldEqual, false)

				//Ransom orders
				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder11.UID+"/edit", rightToken, &it.InputEditRansomOrder{IsActive: &tempFalse}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Last(&ransomOrder11)
				convey.So(ransomOrder11.IsActive, convey.ShouldEqual, false)

			})

			convey.Convey("Try edit all from another account, must fail", func() {

				//User
				var tempExtID = int64(100)
				var tempFirstName = "temp100"
				var tempLastName = "timp100"
				var tempMiddleName = "tomp100"
				var tempPhone = "9999900009"
				code, resp = makeRequest("POST", "/v1/users/"+user.UID+"/edit", rightToken2, &it.InputEditUser{ExtID: &tempExtID, FirstName: &tempFirstName, LastName: &tempLastName, MiddleName: &tempMiddleName, Phone: &tempPhone}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchUsersUID)

				//Manager
				var tempEmail = "asdf@gmail.com"
				var tempBirthday = "2000-01-01"
				code, resp = makeRequest("POST", "/v1/managers/"+manager.UID+"/edit", rightToken2, &it.InputEditManager{ExtID: &tempExtID, FirstName: &tempFirstName, LastName: &tempLastName, MiddleName: &tempMiddleName, Phone: &tempPhone, Email: &tempEmail, Birthday: &tempBirthday}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchManagersUID)

				//Vehicle
				var tempAvatarUri = "url"
				var tempNumber = "g100ut"
				var tempMark = "FromSoftware"
				var tempModel = "Elden Ring"
				var tempType = "Sosalik"
				code, resp = makeRequest("POST", "/v1/vehicles/"+vehicle1.UID+"/edit", rightToken2, &it.InputEditVehicle{ExtID: &tempExtID, AvatarUri: &tempAvatarUri, Number: &tempNumber, Mark: &tempMark, Model: &tempModel, Type: &tempType}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchVehiclesUID)

				//Rent tariff
				var tempFromDays = 30
				var tempToDays = 40
				var tempDayPayment = 98765.0
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff11.UID+"/edit", rightToken2, &it.InputEditRentTariff{ExtID: &tempExtID, DayPayment: &tempDayPayment, FromDays: &tempFromDays, ToDays: &tempToDays}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)

				//Ransom tariff
				var tempInitialFee = 9999
				var tempVehicleAmount = 9999.0
				var tempTrackerAmount = 9999.1
				var tempTrackerServiceAmount = 9999.2
				var tempVehicleTaxAmount = 9999.3
				var tempRegistrationAmount = 9999.4
				var tempInsuranceOsagoAmount = 9999.5
				var tempTerm = 99999
				var tempYearRate = float64(999999)
				var tempInsuranceRate = 9999999
				var tempMonthPayment = 9999.6
				var tempTotalVehicleAmount = 9999.8
				var tempTotalVehicleWithoutInsuranceAmount = 9999.9
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff11.UID+"/edit", rightToken2, &it.InputEditRansomTariff{InitialFee: &tempInitialFee, VehicleAmount: &tempVehicleAmount, TrackerAmount: &tempTrackerAmount, TrackerServiceAmount: &tempTrackerServiceAmount, VehicleTaxAmount: &tempVehicleTaxAmount, TotalVehicleAmount: &tempTotalVehicleAmount, TotalVehicleWithoutInsuranceAmount: &tempTotalVehicleWithoutInsuranceAmount, Term: &tempTerm, YearRate: &tempYearRate, RegistrationAmount: &tempRegistrationAmount, InsuranceOsagoAmount: &tempInsuranceOsagoAmount, InsuranceRate: &tempInsuranceRate, MonthPayment: &tempMonthPayment, DayPayment: &tempDayPayment}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)

				//Rent orders
				var tempDateStart = time.Date(2000, 1, 15, 0, 0, 0, 0, time.UTC)
				var tempDateEnd = time.Date(2000, 1, 25, 0, 0, 0, 0, time.UTC)
				var tempTrue = true
				var tempFalse = false
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder11.UID+"/edit", rightToken2, &it.InputEditRentOrder{DateStart: &tempDateStart, DateEnd: &tempDateEnd, IsActive: &tempTrue}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)

				//Ransom orders
				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder11.UID+"/edit", rightToken2, &it.InputEditRansomOrder{IsActive: &tempFalse}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)

			})

			convey.Convey("Try delete all", func() {

				//Rent orders
				db.Db.Unscoped().Last(&rentOrder11, "uid = ?", rentOrder11.UID)
				convey.So(rentOrder11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder11.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&rentOrder11, "uid = ?", rentOrder11.UID)
				convey.So(rentOrder11.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&rentOrder12, "uid = ?", rentOrder12.UID)
				convey.So(rentOrder12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder12.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&rentOrder12, "uid = ?", rentOrder12.UID)
				convey.So(rentOrder12.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&rentOrder21, "uid = ?", rentOrder21.UID)
				convey.So(rentOrder21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder21.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&rentOrder21, "uid = ?", rentOrder21.UID)
				convey.So(rentOrder21.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&rentOrder22, "uid = ?", rentOrder22.UID)
				convey.So(rentOrder22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder22.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&rentOrder22, "uid = ?", rentOrder22.UID)
				convey.So(rentOrder22.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				//Ransom orders
				db.Db.Unscoped().Last(&ransomOrder11, "uid = ?", ransomOrder11.UID)
				convey.So(ransomOrder11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder11.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&ransomOrder11, "uid = ?", ransomOrder11.UID)
				convey.So(ransomOrder11.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&ransomOrder21, "uid = ?", ransomOrder21.UID)
				convey.So(ransomOrder21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder21.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&ransomOrder21, "uid = ?", ransomOrder21.UID)
				convey.So(ransomOrder21.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				//User
				db.Db.Unscoped().Last(&user, "uid = ?", user.UID)
				convey.So(user.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/users/"+user.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&user, "uid = ?", user.UID)
				convey.So(user.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				//Manager
				db.Db.Unscoped().Last(&manager, "uid = ?", manager.UID)
				convey.So(manager.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/managers/"+manager.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&manager, "uid = ?", manager.UID)
				convey.So(manager.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				//Vehicles
				db.Db.Unscoped().Last(&vehicle1, "uid = ?", vehicle1.UID)
				convey.So(vehicle1.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/vehicles/"+vehicle1.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&vehicle1, "uid = ?", vehicle1.UID)
				convey.So(vehicle1.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&vehicle2, "uid = ?", vehicle2.UID)
				convey.So(vehicle2.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/vehicles/"+vehicle2.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&vehicle2, "uid = ?", vehicle2.UID)
				convey.So(vehicle2.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				//Rent tariffs
				db.Db.Unscoped().Last(&rentTariff11, "uid = ?", rentTariff11.UID)
				convey.So(rentTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff11.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&rentTariff11, "uid = ?", rentTariff11.UID)
				convey.So(rentTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&rentTariff12, "uid = ?", rentTariff12.UID)
				convey.So(rentTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff12.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&rentTariff12, "uid = ?", rentTariff12.UID)
				convey.So(rentTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&rentTariff21, "uid = ?", rentTariff21.UID)
				convey.So(rentTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff21.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&rentTariff21, "uid = ?", rentTariff21.UID)
				convey.So(rentTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&rentTariff22, "uid = ?", rentTariff22.UID)
				convey.So(rentTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff22.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&rentTariff22, "uid = ?", rentTariff22.UID)
				convey.So(rentTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&ransomTariff11, "uid = ?", ransomTariff11.UID)
				convey.So(ransomTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff11.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&ransomTariff11, "uid = ?", ransomTariff11.UID)
				convey.So(ransomTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&ransomTariff12, "uid = ?", ransomTariff12.UID)
				convey.So(ransomTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff12.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&ransomTariff12, "uid = ?", ransomTariff12.UID)
				convey.So(ransomTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&ransomTariff21, "uid = ?", ransomTariff21.UID)
				convey.So(ransomTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff21.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&ransomTariff21, "uid = ?", ransomTariff21.UID)
				convey.So(ransomTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

				db.Db.Unscoped().Last(&ransomTariff22, "uid = ?", ransomTariff22.UID)
				convey.So(ransomTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff22.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Unscoped().Last(&ransomTariff22, "uid = ?", ransomTariff22.UID)
				convey.So(ransomTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, false)

			})

			convey.Convey("Try delete from amother account, must fail", func() {

				//Rent orders
				db.Db.Unscoped().Last(&rentOrder11, "uid = ?", rentOrder11.UID)
				convey.So(rentOrder11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder11.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)
				db.Db.Unscoped().Last(&rentOrder11, "uid = ?", rentOrder11.UID)
				convey.So(rentOrder11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&rentOrder12, "uid = ?", rentOrder12.UID)
				convey.So(rentOrder12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder12.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)
				db.Db.Unscoped().Last(&rentOrder12, "uid = ?", rentOrder12.UID)
				convey.So(rentOrder12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&rentOrder21, "uid = ?", rentOrder21.UID)
				convey.So(rentOrder21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder21.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)
				db.Db.Unscoped().Last(&rentOrder21, "uid = ?", rentOrder21.UID)
				convey.So(rentOrder21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&rentOrder22, "uid = ?", rentOrder22.UID)
				convey.So(rentOrder22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder22.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)
				db.Db.Unscoped().Last(&rentOrder22, "uid = ?", rentOrder22.UID)
				convey.So(rentOrder22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				//Ransom orders
				db.Db.Unscoped().Last(&ransomOrder11, "uid = ?", ransomOrder11.UID)
				convey.So(ransomOrder11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder11.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)
				db.Db.Unscoped().Last(&ransomOrder11, "uid = ?", ransomOrder11.UID)
				convey.So(ransomOrder11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&ransomOrder21, "uid = ?", ransomOrder21.UID)
				convey.So(ransomOrder21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder21.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)
				db.Db.Unscoped().Last(&ransomOrder21, "uid = ?", ransomOrder21.UID)
				convey.So(ransomOrder21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				///User
				db.Db.Unscoped().Last(&user, "uid = ?", user.UID)
				convey.So(user.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/users/"+user.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchUsersUID)
				db.Db.Unscoped().Last(&user, "uid = ?", user.UID)
				convey.So(user.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				//Manager
				db.Db.Unscoped().Last(&manager, "uid = ?", manager.UID)
				convey.So(manager.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/managers/"+manager.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchManagersUID)
				db.Db.Unscoped().Last(&manager, "uid = ?", manager.UID)
				convey.So(manager.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				//Vehicles
				db.Db.Unscoped().Last(&vehicle1, "uid = ?", vehicle1.UID)
				convey.So(vehicle1.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/vehicles/"+vehicle1.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchVehiclesUID)
				db.Db.Unscoped().Last(&vehicle1, "uid = ?", vehicle1.UID)
				convey.So(vehicle1.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&vehicle2, "uid = ?", vehicle2.UID)
				convey.So(vehicle2.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/vehicles/"+vehicle2.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchVehiclesUID)
				db.Db.Unscoped().Last(&vehicle2, "uid = ?", vehicle2.UID)
				convey.So(vehicle2.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				//Rent tariffs
				db.Db.Unscoped().Last(&rentTariff11, "uid = ?", rentTariff11.UID)
				convey.So(rentTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff11.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
				db.Db.Unscoped().Last(&rentTariff11, "uid = ?", rentTariff11.UID)
				convey.So(rentTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&rentTariff12, "uid = ?", rentTariff12.UID)
				convey.So(rentTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff12.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
				db.Db.Unscoped().Last(&rentTariff12, "uid = ?", rentTariff12.UID)
				convey.So(rentTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&rentTariff21, "uid = ?", rentTariff21.UID)
				convey.So(rentTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff21.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
				db.Db.Unscoped().Last(&rentTariff21, "uid = ?", rentTariff21.UID)
				convey.So(rentTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&rentTariff22, "uid = ?", rentTariff22.UID)
				convey.So(rentTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff22.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
				db.Db.Unscoped().Last(&rentTariff22, "uid = ?", rentTariff22.UID)
				convey.So(rentTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				//Ransom tariffs
				db.Db.Unscoped().Last(&ransomTariff11, "uid = ?", ransomTariff11.UID)
				convey.So(ransomTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff11.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
				db.Db.Unscoped().Last(&ransomTariff11, "uid = ?", ransomTariff11.UID)
				convey.So(ransomTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&ransomTariff12, "uid = ?", ransomTariff12.UID)
				convey.So(ransomTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff12.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
				db.Db.Unscoped().Last(&ransomTariff12, "uid = ?", ransomTariff12.UID)
				convey.So(ransomTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&ransomTariff21, "uid = ?", ransomTariff21.UID)
				convey.So(ransomTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff21.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
				db.Db.Unscoped().Last(&ransomTariff21, "uid = ?", ransomTariff21.UID)
				convey.So(ransomTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&ransomTariff22, "uid = ?", ransomTariff22.UID)
				convey.So(ransomTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff22.UID+"/delete", rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
				db.Db.Unscoped().Last(&ransomTariff22, "uid = ?", ransomTariff22.UID)
				convey.So(ransomTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

			})

			convey.Convey("Try delete dependent data, must fail", func() {
				//User
				db.Db.Unscoped().Last(&user, "uid = ?", user.UID)
				convey.So(user.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/users/"+user.UID+"/delete", rightToken, nil, nil)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRAttachedData)
				convey.So(code, convey.ShouldEqual, 400)
				db.Db.Unscoped().Last(&user, "uid = ?", user.UID)
				convey.So(user.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				//Manager
				db.Db.Unscoped().Last(&manager, "uid = ?", manager.UID)
				convey.So(manager.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/managers/"+manager.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRAttachedData)
				db.Db.Unscoped().Last(&manager, "uid = ?", manager.UID)
				convey.So(manager.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				//Vehicles
				db.Db.Unscoped().Last(&vehicle1, "uid = ?", vehicle1.UID)
				convey.So(vehicle1.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/vehicles/"+vehicle1.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRAttachedData)
				db.Db.Unscoped().Last(&vehicle1, "uid = ?", vehicle1.UID)
				convey.So(vehicle1.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&vehicle2, "uid = ?", vehicle2.UID)
				convey.So(vehicle2.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/vehicles/"+vehicle2.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRAttachedData)
				db.Db.Unscoped().Last(&vehicle2, "uid = ?", vehicle2.UID)
				convey.So(vehicle2.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				//Rent tariffs

				db.Db.Unscoped().Last(&rentTariff12, "uid = ?", rentTariff12.UID)
				convey.So(rentTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff12.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRAttachedData)
				db.Db.Unscoped().Last(&rentTariff12, "uid = ?", rentTariff12.UID)
				convey.So(rentTariff12.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&rentTariff22, "uid = ?", rentTariff22.UID)
				convey.So(rentTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/rent-tariffs/"+rentTariff22.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRAttachedData)
				db.Db.Unscoped().Last(&rentTariff22, "uid = ?", rentTariff22.UID)
				convey.So(rentTariff22.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				//Ransom tariffs
				db.Db.Unscoped().Last(&ransomTariff11, "uid = ?", ransomTariff11.UID)
				convey.So(ransomTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff11.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRAttachedData)
				db.Db.Unscoped().Last(&ransomTariff11, "uid = ?", ransomTariff11.UID)
				convey.So(ransomTariff11.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

				db.Db.Unscoped().Last(&ransomTariff21, "uid = ?", ransomTariff21.UID)
				convey.So(ransomTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/"+ransomTariff21.UID+"/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRAttachedData)
				db.Db.Unscoped().Last(&ransomTariff21, "uid = ?", ransomTariff21.UID)
				convey.So(ransomTariff21.DeletedAt.Time.IsZero(), convey.ShouldEqual, true)

			})

			convey.Convey("Try create ransom order without some data, must fail", func() {

				var tempFalse = false
				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder11.UID+"/edit", rightToken, &it.InputEditRansomOrder{IsActive: &tempFalse}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				db.Db.Last(&ransomOrder11)
				convey.So(ransomOrder11.IsActive, convey.ShouldEqual, false)

				contractDateEnd1 := time.Date(2000, 2, 1, 0, 0, 0, 0, time.UTC)
				//contractDateEnd2 := time.Date(2000, 3, 1, 0, 0, 0, 0, time.UTC)
				code, resp = makeRequest("POST", "/v1/ransom-orders/", rightToken, &it.InputCreateRansomOrder{ContractDate: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), ContractDateEnd: &contractDateEnd1, VehiclesUID: vehicle1.UID, UsersUID: user.UID, TariffsUID: ransomTariff11.UID, IsActive: true}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchManagersUID)

				code, resp = makeRequest("POST", "/v1/ransom-orders/", rightToken, &it.InputCreateRansomOrder{ContractDate: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), ContractDateEnd: &contractDateEnd1, ManagersUID: manager.UID, UsersUID: user.UID, TariffsUID: ransomTariff11.UID, IsActive: true}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchVehiclesUID)

				code, resp = makeRequest("POST", "/v1/ransom-orders/", rightToken, &it.InputCreateRansomOrder{ContractDate: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), ContractDateEnd: &contractDateEnd1, ManagersUID: manager.UID, VehiclesUID: vehicle1.UID, TariffsUID: ransomTariff11.UID, IsActive: true}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchUsersUID)

				code, resp = makeRequest("POST", "/v1/ransom-orders/", rightToken, &it.InputCreateRansomOrder{ContractDate: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), ContractDateEnd: &contractDateEnd1, ManagersUID: manager.UID, VehiclesUID: vehicle1.UID, UsersUID: user.UID, IsActive: true}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)

			})

			convey.Convey("Try create rent order without some data, must fail", func() {

				code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 2, 1, 0, 0, 0, 0, time.UTC), VehiclesUID: vehicle1.UID, UsersUID: user.UID, TariffsUID: rentTariff11.UID, IsActive: true}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchManagersUID)

				code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 2, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, UsersUID: user.UID, TariffsUID: rentTariff11.UID, IsActive: true}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchVehiclesUID)

				code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 2, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle1.UID, TariffsUID: rentTariff11.UID, IsActive: true}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchUsersUID)

				code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 2, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle1.UID, UsersUID: user.UID, IsActive: true}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)

			})

			convey.Convey("Try edit orders tariff from different vehicle and vice versa, must fail", func() {
				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder11.UID+"/edit", rightToken, &it.InputEditRentOrder{TariffsUID: &rentTariff21.UID}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRWrongTariff)

				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder11.UID+"/edit", rightToken, &it.InputEditRentOrder{VehiclesUID: &vehicle2.UID}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRWrongTariff)

				code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder11.UID+"/edit", rightToken, &it.InputEditRentOrder{VehiclesUID: &vehicle2.UID, TariffsUID: &rentTariff12.UID}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRWrongTariff)

				//Ransom orders
				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder11.UID+"/edit", rightToken, &it.InputEditRansomOrder{TariffsUID: &ransomTariff22.UID}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRWrongTariff)

				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder11.UID+"/edit", rightToken, &it.InputEditRansomOrder{VehiclesUID: &vehicle2.UID}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRWrongTariff)

				code, resp = makeRequest("POST", "/v1/ransom-orders/"+ransomOrder11.UID+"/edit", rightToken, &it.InputEditRansomOrder{VehiclesUID: &vehicle2.UID, TariffsUID: &ransomTariff12.UID}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRWrongTariff)
			})

			convey.Convey("Try lists", func() {
				//Users
				code, resp = makeRequest("POST", "/v1/users/list", rightToken, &it.InputGetListUsers{}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				convey.So(resp.Payload.(map[string]interface{})["total_items"], convey.ShouldNotBeNil)
				convey.So(int64(resp.Payload.(map[string]interface{})["total_items"].(float64)), convey.ShouldEqual, 1)

				//Managers
				code, resp = makeRequest("POST", "/v1/managers/list", rightToken, &it.InputGetList{}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				convey.So(resp.Payload.(map[string]interface{})["total_items"], convey.ShouldNotBeNil)
				convey.So(int64(resp.Payload.(map[string]interface{})["total_items"].(float64)), convey.ShouldEqual, 1)

				//Vehicles
				code, resp = makeRequest("POST", "/v1/vehicles/list", rightToken, &it.InputGetListVehicles{}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				convey.So(resp.Payload.(map[string]interface{})["total_items"], convey.ShouldNotBeNil)
				convey.So(int64(resp.Payload.(map[string]interface{})["total_items"].(float64)), convey.ShouldEqual, 2)

				//Rent tariffs
				code, resp = makeRequest("POST", "/v1/rent-tariffs/list", rightToken, &it.InputGetRentTariffList{}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				convey.So(resp.Payload.(map[string]interface{})["total_items"], convey.ShouldNotBeNil)
				convey.So(int64(resp.Payload.(map[string]interface{})["total_items"].(float64)), convey.ShouldEqual, 4)

				//Ransom tariffs
				code, resp = makeRequest("POST", "/v1/ransom-tariffs/list", rightToken, &it.InputGetRansomTariffList{}, nil)
				convey.So(code, convey.ShouldEqual, 200)
				convey.So(resp.Payload.(map[string]interface{})["total_items"], convey.ShouldNotBeNil)
				convey.So(int64(resp.Payload.(map[string]interface{})["total_items"].(float64)), convey.ShouldEqual, 4)

				////Rent orders
				//code, resp = makeRequest("POST", "/v1/rent-orders/list", rightToken, &it.InputGetRentOrders{}, nil)
				//convey.So(resp.Message, convey.ShouldEqual, "")
				//convey.So(code, convey.ShouldEqual, 200)
				//convey.So(resp.Payload.(map[string]interface{})["total_items"], convey.ShouldNotBeNil)
				//convey.So(int64(resp.Payload.(map[string]interface{})["total_items"].(float64)), convey.ShouldEqual, 4)
				//
				////Ransom orders
				//code, resp = makeRequest("POST", "/v1/ransom-orders/list", rightToken, &it.InputGetListRansomOrders{}, nil)
				//convey.So(code, convey.ShouldEqual, 200)
				//convey.So(resp.Payload.(map[string]interface{})["total_items"], convey.ShouldNotBeNil)
				//convey.So(int64(resp.Payload.(map[string]interface{})["total_items"].(float64)), convey.ShouldEqual, 2)
			})

		})

		convey.Convey("Try make request without Token, must fail", func() {

			convey.Convey("User", func() {
				code, resp := makeRequest("POST", "/v1/users/", "", &it.InputCreateUser{FirstName: "temp", LastName: "tamp", MiddleName: "tomp", Phone: "123454321"}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/users/temp", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				var tempFName = "temp"
				var tempLName = "tamp"
				var tempMName = "tomp"
				var tempPhone = "1234567912342"
				code, resp = makeRequest("POST", "/v1/users/temp/edit", "", &it.InputEditUser{FirstName: &tempFName, LastName: &tempLName, MiddleName: &tempMName, Phone: &tempPhone}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/users/temp/delete", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)
			})

			convey.Convey("Vehicle", func() {
				code, resp := makeRequest("POST", "/v1/vehicles/", "", &it.InputCreateVehicle{Number: "temp", Mark: "tamp", Model: "tomp"}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/vehicles/temp", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/vehicles/temp/edit", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/vehicles/temp/delete", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)
			})

			convey.Convey("Rent Order", func() {
				code, resp := makeRequest("POST", "/v1/rent-orders/", "", &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/rent-orders/temp", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/rent-orders/temp/delete", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)
			})

			convey.Convey("Rent Order(Multiple)", func() {
				code, resp := makeRequest("POST", "/v1/rent-orders/multiple", "", &[]it.InputCreateRentOrder{{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)}, {DateStart: time.Date(2002, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC)}}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				var tempStats = "PRELIMINARY"
				var tempDateStart = time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)
				var tempDateEnd = time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)
				var tempVehicleUID = "temp"
				code, resp = makeRequest("POST", "/v1/rent-orders/list", "", &it.InputGetListRentOrders{Filters: it.RentOrderFilters{Status: &tempStats, DateStart: &tempDateStart, DateEnd: &tempDateEnd, VehiclesUID: &tempVehicleUID}}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)
			})

			convey.Convey("Rent Tariff", func() {
				code, resp := makeRequest("POST", "/v1/rent-tariffs/", "", &it.InputCreateRentTariff{ExtID: 0, VehiclesUID: "e8fn87egf782b783fb", FromDays: 10, ToDays: 12, DayPayment: 100.0}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/rent-tariffs/temp", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/rent-tariffs/temp/delete", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)
			})

			convey.Convey("Ransom Order", func() {
				var tempEndDate = time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)
				code, resp := makeRequest("POST", "/v1/ransom-orders/", "", &it.InputCreateRansomOrder{ExtID: 0, VehiclesUID: "e8fn87egf782b783fb", UsersUID: "aegih0egeiweg", TariffsUID: "fwehfuefhn293nf3xh9", ContractDate: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), ContractDateEnd: &tempEndDate}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/ransom-orders/temp", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/ransom-orders/temp/delete", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)
			})

			convey.Convey("Ransom Tariff", func() {
				code, resp := makeRequest("POST", "/v1/ransom-tariffs/", "", &it.InputCreateRansomTariff{ExtID: 0, VehiclesUID: "e8fn87egf782b783fb", DayPayment: 100.0}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/ransom-tariffs/temp", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)

				code, resp = makeRequest("POST", "/v1/ransom-tariffs/temp/delete", "", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenReq)
			})
		})

		convey.Convey("Try make request with wrong Token, must fail", func() {

			convey.Convey("User", func() {
				code, resp := makeRequest("POST", "/v1/users/", "definitelywrongtoken", &it.InputCreateUser{FirstName: "temp", LastName: "tamp", MiddleName: "tomp", Phone: "123454321"}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/users/temp", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				var tempFName = "temp"
				var tempLName = "tamp"
				var tempMName = "tomp"
				var tempPhone = "1234567912342"
				code, resp = makeRequest("POST", "/v1/users/temp/edit", "definitelywrongtoken", &it.InputEditUser{FirstName: &tempFName, LastName: &tempLName, MiddleName: &tempMName, Phone: &tempPhone}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/users/temp/delete", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)
			})

			convey.Convey("Vehicle", func() {
				code, resp := makeRequest("POST", "/v1/vehicles/", "definitelywrongtoken", &it.InputCreateVehicle{Number: "temp", Mark: "tamp", Model: "tomp"}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/vehicles/temp", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/vehicles/temp/edit", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/vehicles/temp/delete", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)
			})

			convey.Convey("Rent Order", func() {
				code, resp := makeRequest("POST", "/v1/rent-orders/", "definitelywrongtoken", &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/rent-orders/temp", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/rent-orders/temp/delete", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)
			})

			convey.Convey("Rent Order(Multiple)", func() {
				code, resp := makeRequest("POST", "/v1/rent-orders/multiple", "definitelywrongtoken", &[]it.InputCreateRentOrder{{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)}, {DateStart: time.Date(2002, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC)}}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				var tempStats = "PRELIMINARY"
				var tempDateStart = time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)
				var tempDateEnd = time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)
				var tempVehicleUID = "temp"
				code, resp = makeRequest("POST", "/v1/rent-orders/list", "definitelywrongtoken", &it.InputGetListRentOrders{Filters: it.RentOrderFilters{Status: &tempStats, DateStart: &tempDateStart, DateEnd: &tempDateEnd, VehiclesUID: &tempVehicleUID}}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)
			})

			convey.Convey("Rent Tariff", func() {
				code, resp := makeRequest("POST", "/v1/rent-tariffs/", "definitelywrongtoken", &it.InputCreateRentTariff{ExtID: 0, VehiclesUID: "e8fn87egf782b783fb", FromDays: 10, ToDays: 12, DayPayment: 100.0}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/rent-tariffs/temp", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/rent-tariffs/temp/delete", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)
			})

			convey.Convey("Ransom Order", func() {
				var tempEndDate = time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)
				code, resp := makeRequest("POST", "/v1/ransom-orders/", "definitelywrongtoken", &it.InputCreateRansomOrder{ExtID: 0, VehiclesUID: "e8fn87egf782b783fb", UsersUID: "aegih0egeiweg", TariffsUID: "fwehfuefhn293nf3xh9", ContractDate: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), ContractDateEnd: &tempEndDate}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/ransom-orders/temp", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/ransom-orders/temp/delete", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)
			})

			convey.Convey("Ransom Tariff", func() {
				code, resp := makeRequest("POST", "/v1/ransom-tariffs/", "definitelywrongtoken", &it.InputCreateRansomTariff{ExtID: 0, VehiclesUID: "e8fn87egf782b783fb", DayPayment: 100.0}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/ransom-tariffs/temp", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)

				code, resp = makeRequest("POST", "/v1/ransom-tariffs/temp/delete", "definitelywrongtoken", nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchToken)
			})
		})

		convey.Convey("Try make request with wrong UID, must fail", func() {

			convey.Convey("User", func() {

				code, resp := makeRequest("POST", "/v1/users/temp", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchUsersUID)

				var tempFName = "temp"
				var tempLName = "tamp"
				var tempMName = "tomp"
				var tempPhone = "1234567912342"
				code, resp = makeRequest("POST", "/v1/users/temp/edit", rightToken, &it.InputEditUser{FirstName: &tempFName, LastName: &tempLName, MiddleName: &tempMName, Phone: &tempPhone}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchUsersUID)

				code, resp = makeRequest("POST", "/v1/users/temp/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchUsersUID)
			})

			convey.Convey("Vehicle", func() {

				code, resp := makeRequest("POST", "/v1/vehicles/temp", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchVehiclesUID)

				code, resp = makeRequest("POST", "/v1/vehicles/temp/edit", rightToken, &it.InputCreateVehicle{Number: "temp", Mark: "tamp", Model: "tomp"}, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchVehiclesUID)

				code, resp = makeRequest("POST", "/v1/vehicles/temp/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchVehiclesUID)
			})

			convey.Convey("Rent Order", func() {

				code, resp := makeRequest("POST", "/v1/rent-orders/temp", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)

				code, resp = makeRequest("POST", "/v1/rent-orders/temp/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)
			})

			convey.Convey("Rent Tariff", func() {

				code, resp := makeRequest("POST", "/v1/rent-tariffs/temp", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)

				code, resp = makeRequest("POST", "/v1/rent-tariffs/temp/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
			})

			convey.Convey("Ransom Order", func() {

				code, resp := makeRequest("POST", "/v1/ransom-orders/temp", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)

				code, resp = makeRequest("POST", "/v1/ransom-orders/temp/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchOrdersUID)
			})

			convey.Convey("Ransom Tariff", func() {

				code, resp := makeRequest("POST", "/v1/ransom-tariffs/temp", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)

				code, resp = makeRequest("POST", "/v1/ransom-tariffs/temp/delete", rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)
				convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchTariffsUID)
			})
		})

		convey.Convey("Try create with wrong input, must fail", func() {

			convey.Convey("Account", func() {
				code, _ := makeRequest("POST", "/v1/accounts/", rightToken, &it.InputCreateAccount{Name: "tamp"}, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/accounts/", rightToken, &it.InputCreateAccount{Token: "4312"}, nil)
				convey.So(code, convey.ShouldEqual, 400)
			})

			convey.Convey("User", func() {

				//code, _ := makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{LastName: "tamp", Phone: "123454321"}, nil)
				//convey.So(code, convey.ShouldEqual, 400)

				//code, _ = makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", Phone: "123454321"}, nil)
				//convey.So(code, convey.ShouldEqual, 400)

				code, _ := makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", LastName: "tamp"}, nil)
				convey.So(code, convey.ShouldEqual, 400)
			})

			convey.Convey("Vehicle", func() {

				code, _ := makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Mark: "tamp", Model: "tomp"}, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp", Model: "tomp"}, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp", Mark: "tamp"}, nil)
				convey.So(code, convey.ShouldEqual, 400)

			})

			convey.Convey("Rent Order", func() {

				code, _ := makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC), VehiclesUID: "test"}, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC), VehiclesUID: "test"}, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateEnd: time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC), VehiclesUID: "test"}, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), VehiclesUID: "test"}, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)}, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), VehiclesUID: "test"}, nil)
				convey.So(code, convey.ShouldEqual, 400)

			})
		})

		convey.Convey("Try right Create", func() {

			convey.Convey("Account", func() {
				code, _ := makeRequest("POST", "/v1/accounts/", rightToken, &it.InputCreateAccount{Name: "tamp", Token: "4321"}, nil)
				convey.So(code, convey.ShouldEqual, 200)
			})

			convey.Convey("User", func() {
				code, _ := makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", LastName: "tamp", Phone: "123454321"}, nil)
				convey.So(code, convey.ShouldEqual, 200)

				var user bt.User
				db.Db.Last(&user)

				convey.Convey("Vehicle", func() {
					code, _ := makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp", Mark: "tamp", Model: "tomp"}, nil)
					convey.So(code, convey.ShouldEqual, 200)

					var vehicle bt.Vehicle
					db.Db.Last(&vehicle)

					convey.Convey("Rent Tariff", func() {
						code, _ := makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, DayPayment: 10, ToDays: 10, FromDays: 3}, nil)
						convey.So(code, convey.ShouldEqual, 200)

						var rentTariff bt.RentTariff
						db.Db.Last(&rentTariff)

						convey.Convey("Manager ", func() {
							code, _ := makeRequest("POST", "/v1/managers/", rightToken, &it.InputCreateManager{FirstName: "temp", LastName: "tamp", Phone: "tomp", Email: "timp"}, nil)
							convey.So(code, convey.ShouldEqual, 200)

							var manager bt.Manager
							db.Db.Last(&manager)

							convey.Convey("Rent Order", func() {

								code, _ := makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 2, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID, UsersUID: user.UID, TariffsUID: rentTariff.UID}, nil)
								convey.So(code, convey.ShouldEqual, 200)

							})
						})
					})
				})
			})

		})

		convey.Convey("Try create/edit Users with same Phone, must fail", func() {

			code, resp := makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", LastName: "tamp", Phone: "987673567456754321"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			code, resp = makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "tomp", LastName: "timp", Phone: "987673567456754321"}, nil)
			convey.So(code, convey.ShouldEqual, 400)
			convey.So(resp.Message, convey.ShouldEqual, logs.ERRExistingPhoNum)

			code, resp = makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "tomp", LastName: "timp", Phone: "134257123905559743"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var user bt.User
			db.Db.Last(&user)

			var tempFName = "tomp"
			var tempLName = "timp"
			var tempPhone = "987673567456754321"
			code, resp = makeRequest("POST", "/v1/users/"+user.UID+"/edit", rightToken, &it.InputEditUser{FirstName: &tempFName, LastName: &tempLName, Phone: &tempPhone}, nil)
			convey.So(code, convey.ShouldEqual, 400)
			convey.So(resp.Message, convey.ShouldEqual, logs.ERRExistingPhoNum)
		})

		convey.Convey("Try create/edit Accounts with same Token, must fail", func() {

			code, resp := makeRequest("POST", "/v1/accounts/", rightToken, &it.InputCreateAccount{Name: "tamp", Token: "54321"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			code, resp = makeRequest("POST", "/v1/accounts/", rightToken, &it.InputCreateAccount{Name: "tamp", Token: "54321"}, nil)
			convey.So(code, convey.ShouldEqual, 400)
			convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenIsOccupied)

			code, resp = makeRequest("POST", "/v1/accounts/54321/edit", rightToken, &it.InputEditAccount{Name: "timp", NewToken: rightToken}, nil)
			convey.So(code, convey.ShouldEqual, 400)
			convey.So(resp.Message, convey.ShouldEqual, logs.ERRTokenIsOccupied)

			code, resp = makeRequest("POST", "/v1/accounts/54321/edit", rightToken, &it.InputEditAccount{Name: "timp", NewToken: "5432154321"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
		})

		convey.Convey("Get List of Rent Orders", func() {

			code, resp := makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp", Mark: "tamp", Model: "tomp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var vehicle bt.Vehicle
			db.Db.Last(&vehicle)

			code, _ = makeRequest("POST", "/v1/managers/", rightToken, &it.InputCreateManager{FirstName: "temp", LastName: "tamp", Phone: "tomp", Email: "timp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var manager bt.Manager
			db.Db.Last(&manager)

			code, _ = makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", LastName: "tamp", Phone: "123454321"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var user bt.User
			db.Db.Last(&user)

			code, _ = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, DayPayment: 100, ToDays: 2, FromDays: 1}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var rentTariff bt.RentTariff
			db.Db.Last(&rentTariff)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user.UID, TariffsUID: rentTariff.UID, DateStart: time.Date(2010, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2010, 1, 2, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user.UID, TariffsUID: rentTariff.UID, DateStart: time.Date(2010, 1, 3, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2010, 1, 4, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user.UID, TariffsUID: rentTariff.UID, DateStart: time.Date(2010, 1, 5, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2010, 1, 5, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user.UID, TariffsUID: rentTariff.UID, DateStart: time.Date(2010, 1, 6, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2010, 1, 9, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var tempDateStart = time.Date(2010, 1, 1, 0, 0, 0, 0, time.UTC)
			var tempDateEnd = time.Date(2010, 1, 5, 0, 0, 0, 0, time.UTC)
			code, resp = makeRequest("POST", "/v1/rent-orders/list", rightToken, &it.InputGetListRentOrders{Filters: it.RentOrderFilters{DateStart: &tempDateStart, DateEnd: &tempDateEnd, VehiclesUID: &vehicle.UID}}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			convey.So(len(resp.Payload.(map[string]interface{})["items"].([]interface{})), convey.ShouldEqual, 3)
		})

		convey.Convey("Try Create Rent Orders with intersected data, must fail", func() {

			code, resp := makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp", Mark: "tamp", Model: "tomp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var vehicle bt.Vehicle
			db.Db.Last(&vehicle)

			code, _ = makeRequest("POST", "/v1/managers/", rightToken, &it.InputCreateManager{FirstName: "temp", LastName: "tamp", Phone: "tomp", Email: "timp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var manager bt.Manager
			db.Db.Last(&manager)

			code, _ = makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", LastName: "tamp", Phone: "123454321"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var user bt.User
			db.Db.Last(&user)

			code, _ = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, DayPayment: 100, ToDays: 2, FromDays: 1}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var rentTariff bt.RentTariff
			db.Db.Last(&rentTariff)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user.UID, TariffsUID: rentTariff.UID, DateStart: time.Date(2010, 1, 2, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2011, 1, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user.UID, TariffsUID: rentTariff.UID, DateStart: time.Date(2009, 6, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2010, 4, 4, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID}, nil)
			convey.So(code, convey.ShouldEqual, 400)
			convey.So(resp.Message, convey.ShouldEqual, logs.ERRBusyVehicle)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user.UID, TariffsUID: rentTariff.UID, DateStart: time.Date(2010, 5, 5, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2010, 5, 5, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID}, nil)
			convey.So(code, convey.ShouldEqual, 400)
			convey.So(resp.Message, convey.ShouldEqual, logs.ERRBusyVehicle)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user.UID, TariffsUID: rentTariff.UID, DateStart: time.Date(2010, 2, 6, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2010, 9, 9, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID}, nil)
			convey.So(code, convey.ShouldEqual, 400)
			convey.So(resp.Message, convey.ShouldEqual, logs.ERRBusyVehicle)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user.UID, TariffsUID: rentTariff.UID, DateStart: time.Date(2020, 1, 2, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var rentOrder bt.RentOrder
			db.Db.Last(&rentOrder)

			var tempDate1 = time.Date(2010, 5, 1, 0, 0, 0, 0, time.UTC)
			var tempDate2 = time.Date(2011, 4, 1, 0, 0, 0, 0, time.UTC)
			var tempSatatus = "preliminary"
			code, resp = makeRequest("POST", "/v1/rent-orders/"+rentOrder.UID+"/edit", rightToken, &it.InputEditRentOrder{DateStart: &tempDate1, DateEnd: &tempDate2, Status: &tempSatatus}, nil)
			convey.So(code, convey.ShouldEqual, 400)
			convey.So(resp.Message, convey.ShouldEqual, logs.ERRBusyVehicle)

		})

		convey.Convey("Try Create Rent Tariffs with intersected days, must fail", func() {
			code, _ := makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp", Mark: "tamp", Model: "tomp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var vehicle bt.Vehicle
			db.Db.Last(&vehicle)

			code, _ = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, FromDays: 4, ToDays: 8, DayPayment: 1000.0}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			//code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, FromDays: 4, ToDays: 8, DayPayment: 1000.0}, nil)
			//convey.So(code, convey.ShouldEqual, 400)
			//convey.So(resp.Message, convey.ShouldEqual, logs.ERRRentTariffIntersect)
			//
			//code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, FromDays: 2, ToDays: 4, DayPayment: 1000.0}, nil)
			//convey.So(code, convey.ShouldEqual, 400)
			//convey.So(resp.Message, convey.ShouldEqual, logs.ERRRentTariffIntersect)
			//
			//code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, FromDays: 2, ToDays: 5, DayPayment: 1000.0}, nil)
			//convey.So(code, convey.ShouldEqual, 400)
			//convey.So(resp.Message, convey.ShouldEqual, logs.ERRRentTariffIntersect)
			//
			//code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, FromDays: 7, ToDays: 9, DayPayment: 1000.0}, nil)
			//convey.So(code, convey.ShouldEqual, 400)
			//convey.So(resp.Message, convey.ShouldEqual, logs.ERRRentTariffIntersect)
			//
			//code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, FromDays: 8, ToDays: 10, DayPayment: 1000.0}, nil)
			//convey.So(code, convey.ShouldEqual, 400)
			//convey.So(resp.Message, convey.ShouldEqual, logs.ERRRentTariffIntersect)
			//
			//code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, FromDays: 5, ToDays: 6, DayPayment: 1000.0}, nil)
			//convey.So(code, convey.ShouldEqual, 400)
			//convey.So(resp.Message, convey.ShouldEqual, logs.ERRRentTariffIntersect)
			//
			//code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, FromDays: 1, ToDays: 10, DayPayment: 1000.0}, nil)
			//convey.So(code, convey.ShouldEqual, 400)
			//convey.So(resp.Message, convey.ShouldEqual, logs.ERRRentTariffIntersect)

		})

		convey.Convey("Create Multiple Rent Orders, some with intersected data or errors, must fail", func() {

			code, _ := makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp1", Mark: "tamp", Model: "tomp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			code, _ = makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp2", Mark: "tamp", Model: "tomp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			code, _ = makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp3", Mark: "tamp", Model: "tomp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			code, _ = makeRequest("POST", "/v1/managers/", rightToken, &it.InputCreateManager{FirstName: "temp", LastName: "tamp", Phone: "tomp", Email: "timp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)

			var manager bt.Manager
			db.Db.Last(&manager)

			var firstUsersVehicle bt.Vehicle
			db.Db.Find(&firstUsersVehicle, "number = 'temp1'")
			var secondUsersVehicle bt.Vehicle
			db.Db.Find(&secondUsersVehicle, "number = 'temp2'")
			var thirdUsersVehicle bt.Vehicle
			db.Db.Find(&thirdUsersVehicle, "number = 'temp3'")

			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2002, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2004, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2005, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: thirdUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2014, 9, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2015, 4, 28, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2015, 8, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2016, 4, 28, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2014, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2015, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2006, 1, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2006, 4, 28, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2006, 5, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2006, 7, 28, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2006, 8, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2006, 9, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			//wrong create

			//empty input

			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{{}, {}, {}}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{},
				{

					DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: thirdUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{},
				{

					DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: thirdUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{},
				{

					DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: thirdUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			//no VehiclesUID
			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC),
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: thirdUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			// rent time > 365 days
			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2008, 1, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2009, 8, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2008, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2009, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2008, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2009, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: thirdUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			//  DateStart > DateEnd
			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2009, 1, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2008, 8, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2008, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2009, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2008, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2009, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: thirdUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			// intersection of the first and second
			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2010, 1, 2, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2011, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2009, 8, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2010, 4, 11, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: thirdUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			// intersection of the first and third
			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2010, 1, 2, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2011, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2009, 8, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2010, 4, 11, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2010, 9, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2011, 2, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			// intersection of the second and third
			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2010, 1, 2, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2011, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2009, 8, 2, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2010, 6, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2010, 1, 2, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2011, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			// intersection first with already created rent order
			code, _ = makeRequest("POST", "/v1/rent-orders/multiple", rightToken, &[]it.InputCreateRentOrder{
				{

					DateStart:   time.Date(2001, 5, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2002, 3, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: firstUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2012, 1, 2, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2013, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: secondUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
				{

					DateStart:   time.Date(2012, 2, 1, 0, 0, 0, 0, time.UTC),
					DateEnd:     time.Date(2013, 1, 1, 0, 0, 0, 0, time.UTC),
					VehiclesUID: thirdUsersVehicle.UID,
					ManagersUID: manager.UID,
				},
			}, nil)
			convey.So(code, convey.ShouldEqual, 400)

			// Reserved time for firstUsersVehicle
			// DateStart:   time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC),
			// DateEnd:     time.Date(2002, 1, 1, 0, 0, 0, 0, time.UTC),
			// Reserved time for secondUsersVehicle
			// DateStart:   time.Date(2002, 2, 1, 0, 0, 0, 0, time.UTC),
			// DateEnd:     time.Date(2003, 1, 1, 0, 0, 0, 0, time.UTC),
			// Reserved time for thirdUsersVehicle
			// DateStart:   time.Date(2004, 2, 1, 0, 0, 0, 0, time.UTC),
			// DateEnd:     time.Date(2005, 1, 1, 0, 0, 0, 0, time.UTC),
		})

		convey.Convey("Try Get/Edit data from another Account, must fail", func() {

			convey.Convey("Vehicle", func() {

				code, _ := makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp1", Mark: "tamp", Model: "tomp"}, nil)
				convey.So(code, convey.ShouldEqual, 200)

				var vehicle1 bt.Vehicle
				db.Db.Last(&vehicle1, "number = ?", "temp1")

				code, _ = makeRequest("POST", "/v1/vehicles/", rightToken2, &it.InputCreateVehicle{Number: "temp1", Mark: "tamp", Model: "tomp"}, nil)
				convey.So(code, convey.ShouldEqual, 200)

				var vehicle2 bt.Vehicle
				db.Db.Last(&vehicle2, "number = ?", "temp1")

				convey.So(vehicle1.UID, convey.ShouldNotEqual, vehicle2.UID)

				code, _ = makeRequest("POST", "/v1/vehicles/"+vehicle1.UID, rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)

				code, _ = makeRequest("POST", "/v1/vehicles/"+vehicle2.UID, rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)

				code, _ = makeRequest("POST", "/v1/vehicles/"+vehicle1.UID, rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/vehicles/"+vehicle2.UID, rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)

			})

			convey.Convey("User", func() {

				code, _ := makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", LastName: "temp", Phone: "87878787"}, nil)
				convey.So(code, convey.ShouldEqual, 200)

				var user1 bt.User
				db.Db.Last(&user1, "phone = ?", "87878787")

				code, _ = makeRequest("POST", "/v1/users/", rightToken2, &it.InputCreateUser{FirstName: "temp", LastName: "temp", Phone: "87878787"}, nil)
				convey.So(code, convey.ShouldEqual, 200)

				var user2 bt.User
				db.Db.Last(&user2, "phone = ?", "87878787")

				convey.So(user1.UID, convey.ShouldNotEqual, user2.UID)

				code, _ = makeRequest("POST", "/v1/users/"+user1.UID, rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)

				code, _ = makeRequest("POST", "/v1/users/"+user2.UID, rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 200)

				code, _ = makeRequest("POST", "/v1/users/"+user1.UID, rightToken2, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)

				code, _ = makeRequest("POST", "/v1/users/"+user2.UID, rightToken, nil, nil)
				convey.So(code, convey.ShouldEqual, 400)

			})
		})

		convey.Convey("Try lists", func() {
			//Users
			code, resp := makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", LastName: "tamp", Phone: "+79099099991"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var user1 bt.User
			db.Db.Last(&user1)
			convey.So(user1.FirstName, convey.ShouldEqual, "temp")

			code, resp = makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp2", LastName: "tamp2", Phone: "+79099099992"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var user2 bt.User
			db.Db.Last(&user2)
			convey.So(user2.FirstName, convey.ShouldEqual, "temp2")

			code, resp = makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Mark: "temp1", Number: "tamp1", Model: "timp1"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var vehicle1 bt.Vehicle
			db.Db.Last(&vehicle1)
			convey.So(vehicle1.Number, convey.ShouldEqual, "tamp1")

			code, resp = makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Mark: "temp1", Number: "tamp2", Model: "timp1"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var vehicle2 bt.Vehicle
			db.Db.Last(&vehicle2)
			convey.So(vehicle2.Number, convey.ShouldEqual, "tamp2")

			code, resp = makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Mark: "temp1", Number: "tamp3", Model: "timp1"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var vehicle3 bt.Vehicle
			db.Db.Last(&vehicle3)
			convey.So(vehicle3.Number, convey.ShouldEqual, "tamp3")

			//Get public vehicles
			var tempTrue = true
			code, resp = makeRequest("POST", "/v1/vehicles/list", rightToken, &it.InputGetListVehicles{Page: 1, PageSize: 5, Filters: it.VehileFilters{Public: &tempTrue}}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			convey.So(resp.Payload, convey.ShouldNotBeNil)
			convey.So(resp.Payload.(map[string]interface{}), convey.ShouldContainKey, "items")
			convey.So(len(resp.Payload.(map[string]interface{})["items"].([]interface{})), convey.ShouldEqual, 3)

			//Create orders and check vehcile statuses
			code, resp = makeRequest("POST", "/v1/managers/", rightToken, &it.InputCreateManager{FirstName: "temp", LastName: "tamp", Phone: "tomp", Email: "timp"}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var manager1 bt.Manager
			db.Db.Last(&manager1)

			code, resp = makeRequest("POST", "/v1/ransom-tariffs/", rightToken, &it.InputCreateRansomTariff{VehiclesUID: vehicle1.UID, DayPayment: 100}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var ransomTariff1 bt.RansomTariff
			db.Db.Last(&ransomTariff1)

			code, resp = makeRequest("POST", "/v1/ransom-orders/", rightToken, &it.InputCreateRansomOrder{UsersUID: user1.UID, VehiclesUID: vehicle1.UID, TariffsUID: ransomTariff1.UID, ManagersUID: manager1.UID, ContractDate: time.Now(), IsActive: true}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var ransomOrder1 bt.RansomOrder
			db.Db.Last(&ransomOrder1)

			db.Db.Last(&vehicle1, vehicle1.ID)
			convey.So(vehicle1.Status, convey.ShouldEqual, "RANSOM")

			code, resp = makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRansomTariff{VehiclesUID: vehicle2.UID, DayPayment: 100}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentTariff1 bt.RentTariff
			db.Db.Last(&rentTariff1)

			code, resp = makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{UsersUID: user1.UID, VehiclesUID: vehicle2.UID, TariffsUID: rentTariff1.UID, ManagersUID: manager1.UID, DateStart: time.Now(), DateEnd: time.Now().Add(time.Hour * 5), IsActive: true}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			var rentOrder1 bt.RentOrder
			db.Db.Last(&rentOrder1)

			db.Db.Last(&vehicle2, vehicle2.ID)
			convey.So(vehicle2.Status, convey.ShouldEqual, "RENT")

			//Get public vehicles
			code, resp = makeRequest("POST", "/v1/vehicles/list", rightToken, &it.InputGetListVehicles{Page: 1, PageSize: 5, Filters: it.VehileFilters{Public: &tempTrue}}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			convey.So(resp.Payload, convey.ShouldNotBeNil)
			convey.So(resp.Payload.(map[string]interface{}), convey.ShouldContainKey, "items")
			convey.So(len(resp.Payload.(map[string]interface{})["items"].([]interface{})), convey.ShouldEqual, 1)

			//Get all vehicles
			code, resp = makeRequest("POST", "/v1/vehicles/list", rightToken, &it.InputGetListVehicles{Page: 1, PageSize: 5}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			convey.So(resp.Payload, convey.ShouldNotBeNil)
			convey.So(resp.Payload.(map[string]interface{}), convey.ShouldContainKey, "items")
			convey.So(len(resp.Payload.(map[string]interface{})["items"].([]interface{})), convey.ShouldEqual, 3)

			//Get all orders
			code, resp = makeRequest("POST", "/v1/orders/list", rightToken, &it.InputGetListOrders{Page: 1, PageSize: 5}, nil)
			convey.So(code, convey.ShouldEqual, 200)
			convey.So(resp.Payload, convey.ShouldNotBeNil)
			convey.So(resp.Payload.(map[string]interface{}), convey.ShouldContainKey, "items")
			convey.So(len(resp.Payload.(map[string]interface{})["items"].([]interface{})), convey.ShouldEqual, 2)

		})

		convey.Convey("Try Add Investment", func() {

			convey.Convey("Account", func() {
				code, _ := makeRequest("POST", "/v1/accounts/", rightToken, &it.InputCreateAccount{Name: "tamp", Token: "4321"}, nil)
				convey.So(code, convey.ShouldEqual, 200)
			})

			convey.Convey("User", func() {
				code, _ := makeRequest("POST", "/v1/users/", rightToken, &it.InputCreateUser{FirstName: "temp", LastName: "tamp", Phone: "123454321"}, nil)
				convey.So(code, convey.ShouldEqual, 200)

				var user bt.User
				db.Db.Last(&user)

				convey.Convey("Vehicle", func() {
					code, _ := makeRequest("POST", "/v1/vehicles/", rightToken, &it.InputCreateVehicle{Number: "temp", Mark: "tamp", Model: "tomp"}, nil)
					convey.So(code, convey.ShouldEqual, 200)

					var vehicle bt.Vehicle
					db.Db.Last(&vehicle)

					convey.Convey("Rent Tariff", func() {
						code, _ := makeRequest("POST", "/v1/rent-tariffs/", rightToken, &it.InputCreateRentTariff{VehiclesUID: vehicle.UID, DayPayment: 10, ToDays: 10, FromDays: 3}, nil)
						convey.So(code, convey.ShouldEqual, 200)

						var rentTariff bt.RentTariff
						db.Db.Last(&rentTariff)

						convey.Convey("Manager ", func() {
							code, _ := makeRequest("POST", "/v1/managers/", rightToken, &it.InputCreateManager{FirstName: "temp", LastName: "tamp", Phone: "tomp", Email: "timp"}, nil)
							convey.So(code, convey.ShouldEqual, 200)

							var manager bt.Manager
							db.Db.Last(&manager)

							convey.Convey("Rent Order", func() {

								code, _ := makeRequest("POST", "/v1/rent-orders/", rightToken, &it.InputCreateRentOrder{DateStart: time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), DateEnd: time.Date(2000, 3, 6, 0, 0, 0, 0, time.UTC), ManagersUID: manager.UID, VehiclesUID: vehicle.UID, UsersUID: user.UID, TariffsUID: rentTariff.UID}, nil)
								convey.So(code, convey.ShouldEqual, 200)

								var rentOrder bt.RentOrder
								db.Db.Last(&rentOrder)

								convey.Convey("Add Investments", func() {

									var tempTitle = "front glass"
									var tempAmount = 1000.0
									var tempDate1 = "2000-03-01"
									var tempDate2 = "2000-03-02"
									var tempDate3 = "2000-03-03"
									var tempDate4 = "2000-03-04"
									var tempDate5 = "2000-03-05"
									var tempWrongDate1 = "2000-03-07"
									var tempWrongDate2 = "1999-12-20"
									var tempRelationTable = "rent_order"

									code, resp := makeRequest("POST", "/v1/add-investments/search", rightToken, &it.AddInvestmentFilters{RelationsTable: &tempRelationTable, RelationsID: &rentOrder.ID}, nil)
									convey.So(code, convey.ShouldEqual, 200)
									convey.So(len(resp.Payload.([]interface{})), convey.ShouldEqual, 0)

									code, _ = makeRequest("POST", "/v1/add-investments/", rightToken, &it.InputAddInvestment{Date: &tempDate1, Amount: &tempAmount, Title: &tempTitle, RelationsID: &rentOrder.ID, RelationsTable: &tempRelationTable}, nil)
									convey.So(code, convey.ShouldEqual, 200)

									code, resp = makeRequest("POST", "/v1/add-investments/search", rightToken, &it.AddInvestmentFilters{RelationsTable: &tempRelationTable, RelationsID: &rentOrder.ID}, nil)
									convey.So(code, convey.ShouldEqual, 200)
									convey.So(len(resp.Payload.([]interface{})), convey.ShouldEqual, 1)

									code, _ = makeRequest("POST", "/v1/add-investments/", rightToken, &it.InputAddInvestment{Date: &tempDate2, Amount: &tempAmount, Title: &tempTitle, RelationsID: &rentOrder.ID, RelationsTable: &tempRelationTable}, nil)
									convey.So(code, convey.ShouldEqual, 200)

									code, _ = makeRequest("POST", "/v1/add-investments/", rightToken, &it.InputAddInvestment{Date: &tempDate3, Amount: &tempAmount, Title: &tempTitle, RelationsID: &rentOrder.ID, RelationsTable: &tempRelationTable}, nil)
									convey.So(code, convey.ShouldEqual, 200)

									code, _ = makeRequest("POST", "/v1/add-investments/", rightToken, &it.InputAddInvestment{Date: &tempDate4, Amount: &tempAmount, Title: &tempTitle, RelationsID: &rentOrder.ID, RelationsTable: &tempRelationTable}, nil)
									convey.So(code, convey.ShouldEqual, 200)

									code, _ = makeRequest("POST", "/v1/add-investments/", rightToken, &it.InputAddInvestment{Date: &tempDate5, Amount: &tempAmount, Title: &tempTitle, RelationsID: &rentOrder.ID, RelationsTable: &tempRelationTable}, nil)
									convey.So(code, convey.ShouldEqual, 200)

									code, _ = makeRequest("POST", "/v1/add-investments/", rightToken, &it.InputAddInvestment{Date: &tempWrongDate1, Amount: &tempAmount, Title: &tempTitle, RelationsID: &rentOrder.ID, RelationsTable: &tempRelationTable}, nil)
									convey.So(code, convey.ShouldEqual, 400)

									code, _ = makeRequest("POST", "/v1/add-investments/", rightToken, &it.InputAddInvestment{Date: &tempWrongDate2, Amount: &tempAmount, Title: &tempTitle, RelationsID: &rentOrder.ID, RelationsTable: &tempRelationTable}, nil)
									convey.So(code, convey.ShouldEqual, 400)

									var lastAddInvestment bt.AddInvestment
									db.Db.Last(&lastAddInvestment)

									code, _ = makeRequest("POST", "/v1/add-investments/"+lastAddInvestment.UID+"/delete", rightToken, nil, nil)
									convey.So(code, convey.ShouldEqual, 200)

									code, resp = makeRequest("POST", "/v1/add-investments/search", rightToken, &it.AddInvestmentFilters{RelationsTable: &tempRelationTable, RelationsID: &rentOrder.ID}, nil)
									convey.So(code, convey.ShouldEqual, 200)
									convey.So(len(resp.Payload.([]interface{})), convey.ShouldEqual, 4)

									tempMarshledInvestments, err := json.Marshal(resp.Payload)
									convey.So(err, convey.ShouldBeNil)
									var allAddInvestments []bt.AddInvestment
									err = json.Unmarshal(tempMarshledInvestments, &allAddInvestments)
									convey.So(err, convey.ShouldBeNil)

									convey.So(len(allAddInvestments), convey.ShouldEqual, 4)

									dayPayment, err := tools.CalculateDayPaymentForDayWithAddInvestmentsString(&allAddInvestments, 0, "2000-02-01", rentOrder.DateEnd.Format("2006-01-02"), nil)
									convey.So(err, convey.ShouldBeNil)
									convey.So(dayPayment, convey.ShouldEqual, 0)

									dayPayment, err = tools.CalculateDayPaymentForDayWithAddInvestmentsString(&allAddInvestments, 0, "2000-03-01", rentOrder.DateEnd.Format("2006-01-02"), nil)
									convey.So(err, convey.ShouldBeNil)
									convey.So(dayPayment, convey.ShouldAlmostEqual, math.Round(1000.0/5.0))

									dayPayment, err = tools.CalculateDayPaymentForDayWithAddInvestmentsString(&allAddInvestments, 0, "2000-03-03", rentOrder.DateEnd.Format("2006-01-02"), nil)
									convey.So(err, convey.ShouldBeNil)
									convey.So(dayPayment, convey.ShouldAlmostEqual, math.Round(1000.0/5.0+1000.0/4.0+1000.0/3.0))

									dayPayment, err = tools.CalculateDayPaymentForDayWithAddInvestmentsString(&allAddInvestments, 0, "2000-03-05", rentOrder.DateEnd.Format("2006-01-02"), nil)
									convey.So(err, convey.ShouldBeNil)
									convey.So(dayPayment, convey.ShouldAlmostEqual, math.Round(1000.0/5.0+1000.0/4.0+1000.0/3.0+1000.0/2.0))

								})

							})
						})
					})
				})
			})

		})

	})
}
