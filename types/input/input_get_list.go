package models

import "time"

type InputGetList struct {
	Page     int                    `json:"page"`
	PageSize int                    `json:"page_size"`
	OrderBy  string                 `json:"order_by"`
	OrderDir string                 `json:"order_direction" example:"asc|desc"`
	Search   string                 `json:"search"`
	Filters  map[string]interface{} `json:"filters"`
}

//-----------------Orders------------------

type OrdersFilters struct {
	IsActive   *bool   `json:"is_active"`
	UsersPhone *string `json:"users_phone"`
	UsersUID   *string `json:"users_uid"`
	Search     *string `json:"search"`
}
type InputGetListOrders struct {
	Page     int           `json:"page"`
	PageSize int           `json:"page_size"`
	OrderBy  string        `json:"order_by"`
	OrderDir string        `json:"order_direction" example:"asc|desc"`
	Filters  OrdersFilters `json:"filters"`
}

//-----------------User--------------------

type UserFilters struct {
	UDIs   *[]string `json:"uids"`
	Search *string   `json:"search"`
}

type InputGetListUsers struct {
	Page     int         `json:"page"`
	PageSize int         `json:"page_size"`
	OrderBy  string      `json:"order_by"`
	OrderDir string      `json:"order_direction" example:"asc|desc"`
	Filters  UserFilters `json:"filters"`
}

//---------------Manager------------------

type ManageFilters struct {
	Search *string `json:"search"`
}

type InputGetListManagers struct {
	Page     int           `json:"page"`
	PageSize int           `json:"page_size"`
	OrderBy  string        `json:"order_by"`
	OrderDir string        `json:"order_direction" example:"asc|desc"`
	Filters  ManageFilters `json:"filters"`
}

//---------------Vehicle------------------

type VehileFilters struct {
	Mark                    *string   `json:"mark"`
	Model                   *string   `json:"model"`
	Color                   *string   `json:"color"`
	DateCreatedStart        *string   `json:"date_created_start"`
	DateCreatedEnd          *string   `json:"date_created_end"`
	BuyDateStart            *string   `json:"buy_date_start"`
	BuyDateEnd              *string   `json:"buy_date_end"`
	RegistrationDateStart   *string   `json:"registration_date_start"`
	RegistrationDateEnd     *string   `json:"registration_date_end"`
	TrackerInstallDateStart *string   `json:"tracker_install_date_start"`
	TrackerInstallDateEnd   *string   `json:"tracker_install_date_end"`
	Owner                   *string   `json:"owner"`
	VehiclesUIDs            *[]string `json:"vehicles_uids"`
	VehiclesIDs             *[]int64  `json:"vehicles_ids"`
	InStatuses              *[]string `json:"in_statuses"`
	Public                  *bool     `json:"public"`
	Search                  *string   `json:"search"`
	UsersPhone              *string   `json:"users_phone"`
}

type InputGetListVehicles struct {
	Page     int           `json:"page"`
	PageSize int           `json:"page_size"`
	OrderBy  string        `json:"order_by"`
	OrderDir string        `json:"order_direction" example:"asc|desc"`
	Filters  VehileFilters `json:"filters"`
}

//---------------Reminder------------------

type ReminderFilters struct {
	VehicleID  *uint64 `json:"vehicle_id"`
	VehicleUID *string `json:"vehicle_uid"`
	Search     *string `json:"search"`
}

type InputGetListReminders struct {
	Page     int             `json:"page"`
	PageSize int             `json:"page_size"`
	OrderBy  string          `json:"order_by"`
	OrderDir string          `json:"order_direction" example:"asc|desc"`
	Filters  ReminderFilters `json:"filters"`
}

//-------------Ransom-Order---------------

type RansomOrderFilters struct {
	IsActive        *bool     `json:"is_active"`
	UsersPhone      *string   `json:"users_phone"`
	IncludeUserUIDs *[]string `json:"include_user_uids"`
	ExcludeUserUIDs *[]string `json:"exclude_user_uids"`
	Type            *string   `json:"type"`
	BalanceByMonth  *string   `json:"balance_by_month"`
	Search          *string   `json:"search"`
}

type InputGetListRansomOrders struct {
	Page           int                `json:"page"`
	PageSize       int                `json:"page_size"`
	OrderBy        string             `json:"order_by"`
	OrderDir       string             `json:"order_direction" example:"asc|desc"`
	Filters        RansomOrderFilters `json:"filters"`
	AdditionalData struct {
		Vehicle bool `json:"vehicle"`
		User    bool `json:"user"`
		Tariff  bool `json:"tariff"`
	} `json:"additional_data"`
}

//--------------Rent-Order----------------

type RentOrderFilters struct {
	VehiclesUID     *string    `json:"vehicles_uid"`
	Status          *string    `json:"status"`
	DateStart       *time.Time `json:"date_start"`
	DateEnd         *time.Time `json:"date_end"`
	IsActive        *bool      `json:"is_active"`
	UsersPhone      *string    `json:"users_phone"`
	Search          *string    `json:"search"`
	IncludeUserUIDs *[]string  `json:"include_user_uids"`
	ExcludeUserUIDs *[]string  `json:"exclude_user_uids"`
	Type            *string    `json:"type"`
}

type InputGetListRentOrders struct {
	Page     int              `json:"page"`
	PageSize int              `json:"page_size"`
	OrderBy  string           `json:"order_by"`
	OrderDir string           `json:"order_direction" example:"asc|desc"`
	Filters  RentOrderFilters `json:"filters"`
}

//-------------Ransom-Tariff---------------

type RansomTariffFilters struct {
	VehiclesUID *string `json:"vehicles_uid"`
	Search      *string `json:"search"`
}

type InputGetRansomTariffList struct {
	Page     int                 `json:"page"`
	PageSize int                 `json:"page_size"`
	OrderBy  string              `json:"order_by"`
	OrderDir string              `json:"order_direction" example:"asc|desc"`
	Filters  RansomTariffFilters `json:"filters"`
}

//--------------Rent-Tariff----------------

type RentTariffFilters struct {
	VehiclesUID *string `json:"vehicles_uid"`
	Search      *string `json:"search"`
}

type InputGetRentTariffList struct {
	Page     int               `json:"page"`
	PageSize int               `json:"page_size"`
	OrderBy  string            `json:"order_by"`
	OrderDir string            `json:"order_direction" example:"asc|desc"`
	Filters  RentTariffFilters `json:"filters"`
}

//---------------History------------------

type HistoryFilters struct {
	Search        *string `json:"search"`
	OperationType *string `json:"operation_type" example:"create|edit|delete"`
}

type InputGetHistoryList struct {
	Page     int            `json:"page"`
	PageSize int            `json:"page_size"`
	OrderBy  string         `json:"order_by"`
	OrderDir string         `json:"order_direction" example:"asc|desc"`
	Filters  HistoryFilters `json:"filters"`
}

//----------------Other--------------------

type InputGetByIDs struct {
	IDs []int64 `json:"ids"`
}
