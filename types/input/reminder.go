package models

type InputCreateReminder struct {
	Date       string `json:"date"`
	Text       string `json:"text"`
	VehiclesID int64  `json:"vehicles_id"`
}

type InputEditReminder struct {
	Date       *string `json:"date"`
	Text       *string `json:"text"`
	VehiclesID *int64  `json:"vehicles_id"`
	Closed     *bool   `json:"closed"`
}
