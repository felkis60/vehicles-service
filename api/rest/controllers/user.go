package controllers

import (
	"net/http"

	"gitlab.com/felkis60/vehicles-service/repository"
	_ "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type UserController struct {
	Repository repository.UserRepository
}

// @Summary Create new user
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_     body it.InputCreateUser true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/ [post]
func (ctrl *UserController) Create(c *gin.Context) {
	var input it.InputCreateUser
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Delete user
// @Tags Users
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "User UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/{uid}/delete [post]
func (ctrl *UserController) Delete(c *gin.Context) {

	data, err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Edit user
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ 	body it.InputEditUser true "Request body"
// @Param uid path string true "User UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/{uid}/edit [post]
func (ctrl *UserController) Edit(c *gin.Context) {
	var input it.InputEditUser
	if err := c.BindJSON(&input); err != nil {
		c.String(http.StatusBadRequest, err.Error())
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), c.MustGet("token").(string), &input, 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})

}

// @Summary Get one user
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "User UID"
// @Success 200 {object} _.User
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/{uid} [post]
func (ctrl *UserController) GetByUID(c *gin.Context) {

	user, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string), true)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: user})
}

// @Summary Get users
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListUsers true "Request body"
// @Success 200 {object} []_.User
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/list [post]
func (ctrl *UserController) List(c *gin.Context) {
	var input it.InputGetListUsers
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}
