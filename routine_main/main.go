package main

import (
	startup "gitlab.com/felkis60/vehicles-service/init"
	"gitlab.com/felkis60/vehicles-service/routines"
)

func main() {
	startup.SystemStartup(true, true, true)
	routines.SyncAll()
}
