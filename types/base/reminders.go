package models

import "time"

type Reminder struct {
	ID         int64     `json:"id"`
	Date       time.Time `json:"date"`
	Text       string    `json:"text"`
	VehiclesID int64     `json:"vehicles_id"`
	AccountsID int64     `json:"accounts_id"`
	Closed     bool      `json:"closed"`
}
