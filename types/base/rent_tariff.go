package models

import (
	"time"

	"gorm.io/gorm"
)

type RentTariff struct {
	ID         int64          `json:"id" gorm:"primarykey"`
	UID        string         `json:"uid"`
	ExtID      int64          `json:"ext_id"`
	AccountsID int64          `json:"accounts_id"`
	VehiclesID int64          `json:"vehicles_id"`
	FromDays   int            `json:"from_days"`
	ToDays     int            `json:"to_days"`
	DayPayment float64        `json:"day_payment"`
	Vehicle    *Vehicle       `json:"vehicle,omitempty" gorm:"-"`
	CreatedAt  time.Time      `json:"created_at"`
	UpdatedAt  time.Time      `json:"updated_at"`
	DeletedAt  gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}
