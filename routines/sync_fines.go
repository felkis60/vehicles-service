package routines

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"

	"gitlab.com/felkis60/vehicles-service/database"
	"gitlab.com/felkis60/vehicles-service/repository"
	bt "gitlab.com/felkis60/vehicles-service/types/base"

	log "github.com/sirupsen/logrus"
)

func SyncFinesFromShtrafovnet(account *bt.Account, ignorTG bool, firstPageOnly bool) error {

	var finesOffset = 100
	var finesRepo = repository.FineRepository{Db: database.Db}

	for i := 0; true; i += finesOffset {
		if firstPageOnly && i > 0 {
			return nil
		}
		var params = url.Values{}
		params.Set("order_by", "created_desc")
		params.Set("with", "best_match_car")
		if i > 0 {
			params.Set("offset", strconv.Itoa(i))
		}

		req, err := http.NewRequest("GET", "https://api.b2b.shtrafovnet.ru/v3/companies/"+strconv.FormatInt(account.ShtrafovnetCompanyID, 10)+"/fines?"+params.Encode(), nil)
		if err != nil {
			log.Error(err)
			return err
		}
		req.Header.Add("Authorization", "Bearer "+account.ShtrafovnetAuthToken)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Error(err)
			return err
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Error(err)
			return err
		}

		var originFines bt.FinesFromSite
		err = json.Unmarshal(body, &originFines)
		if err != nil {
			log.Error(err)
			return err
		}

		if len(originFines.Fines) <= 0 {
			return nil
		}

		for j := range originFines.Fines {
			var newFine = finesRepo.ConvertFineFromSite(&originFines.Fines[j])
			fine, _ := finesRepo.GetByNumber(account.Token, originFines.Fines[j].Number)
			if fine == nil {
				if _, err = finesRepo.Create(account.Token, newFine, 0, ignorTG); err != nil {
					if err.Error() != "fine already exists" {
						log.Warn(err.Error())
					}
				}
				continue
			} else {
				if !fine.PaidAt.Equal(newFine.PaidAt) {
					newFine.ID = fine.ID
					newFine.UID = fine.UID
					newFine.AccountsID = fine.AccountsID
					newFine.VehiclesID = fine.VehiclesID
					if result := database.Db.Save(newFine); result.Error != nil {
						log.Warn(result.Error)
					}
				}
				continue
			}

		}
	}

	return nil
}
