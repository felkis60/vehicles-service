## Запуск 
    go run .
    go run ./api/rest/server/.
    go run ./api/grpc/server/.

## Синхрониззация автомобилей из REX
    go run ./routine_main/.

## Тесты
	go test ./api/rest/test/. -v

## Swagger
    swag init --parseDependency --parseInternal --parseDepth 3 -o ./api/rest/server/docs

## GRPC Init
    protoc --go_out=. --go_opt=paths=source_relative api/grpc/app/vehicles.proto
    protoc --go-grpc_out=. --go-grpc_opt=paths=source_relative api/grpc/app/vehicles.proto