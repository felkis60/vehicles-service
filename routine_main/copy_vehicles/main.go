package main

import (
	"log"

	startup "gitlab.com/felkis60/vehicles-service/init"
	"gitlab.com/felkis60/vehicles-service/routines"
)

func main() {
	startup.SystemStartup(true, true, false)
	if err := routines.CopyVehiclesToAccount("REX", "Jambo", "Джамбулат", "Юнусов"); err != nil {
		log.Print(err)
	}
}
