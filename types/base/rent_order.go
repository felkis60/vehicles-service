package models

import (
	"time"

	"gorm.io/gorm"
)

const (
	StatusPreliminary = "PRELIMINARY"
	StatusReserve     = "RESERVE"
	StatusSuccess     = "SUCCESS"
	StatusCencel      = "CANCEL"
)

func CheckStatus(status_ string) bool {
	switch status_ {
	case StatusPreliminary:
		return true
	case StatusReserve:
		return true
	case StatusSuccess:
		return true
	case StatusCencel:
		return true
	}
	return false

}

type RentOrder struct {
	ID                   int64          `json:"id" gorm:"primarykey"`
	UID                  string         `json:"uid"`
	ExtID                int64          `json:"ext_id"`
	AccountsID           int64          `json:"accounts_id"`
	UsersID              int64          `json:"users_id"`
	ManagersID           int64          `json:"managers_id"`
	VehiclesID           int64          `json:"vehicles_id"`
	TariffsID            int64          `json:"tariffs_id"`
	DateStart            time.Time      `json:"date_start"`
	DateEnd              time.Time      `json:"date_end"`
	DateEndDescription   *string        `json:"date_end_description"`
	ReasonForTerminating *string        `json:"reason_for_terminating"`
	Status               string         `json:"status"  default:"PRELIMINARY" example:"PRELIMINARY|RESERVE|SUCCESS|CANCEL"`
	IsActive             bool           `json:"is_active"`
	DepositPayout        float64        `json:"deposit_payout"`
	RentTariff           *RentTariff    `json:"rent_tariff,omitempty" gorm:"-"`
	User                 *User          `json:"user,omitempty" gorm:"-"`
	Manager              *Manager       `json:"manager,omitempty" gorm:"-"`
	Vehicle              *Vehicle       `json:"vehicle,omitempty" gorm:"-"`
	UserFullName         *string        `json:"user_full_name,omitempty" gorm:"-"`
	VehicleFullName      *string        `json:"vehicle_full_name,omitempty" gorm:"-"`
	UserUID              *string        `json:"user_uid,omitempty" gorm:"-"`
	VehicleUID           *string        `json:"vehicle_uid,omitempty" gorm:"-"`
	Bill                 *Bill          `json:"bill,omitempty" gorm:"-"`
	LastPayment          *interface{}   `json:"last_payment,omitempty" gorm:"-"`
	CreatedAt            time.Time      `json:"created_at"`
	UpdatedAt            time.Time      `json:"updated_at"`
	DeletedAt            gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}

type PaginationRentOrder struct {
	Page       int          `json:"page"`
	Items      *[]RentOrder `json:"items"`
	TotalItems int64        `json:"total_items"`
	TotalPages int          `json:"total_pages"`
}
