package controllers

import (
	"context"
	"encoding/json"
	"errors"

	pb "gitlab.com/felkis60/vehicles-service/api/grpc/app"
	"gitlab.com/felkis60/vehicles-service/helpers"
	logs "gitlab.com/felkis60/vehicles-service/log"
	"gitlab.com/felkis60/vehicles-service/repository"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"
)

type GRPCRansomOrderServer struct {
	Repository repository.RansomOrderRepository
	pb.UnimplementedRansomOrderServiceServer
}

func (s *GRPCRansomOrderServer) Create(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputCreateRansomOrder
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.Create(&unmarshledInput, account.ID, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCRansomOrderServer) List(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputGetListRansomOrders
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	pagInfo, err := s.Repository.List(req.AccountToken, &unmarshledInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonPage, err := json.Marshal(pagInfo)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonPage}, nil
}

func (s *GRPCRansomOrderServer) Search(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputGetListRansomOrders
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var data, err = s.Repository.Search(req.AccountToken, &unmarshledInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCRansomOrderServer) Edit(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputEditRansomOrder
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.Edit(req.UID, &unmarshledInput, req.AccountToken, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCRansomOrderServer) Get(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	data, err := s.Repository.GetByUID(req.UID, req.AccountToken, true, true)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCRansomOrderServer) GetByID(ctx context.Context, req *pb.InputDoByID) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	data, err := s.Repository.GetByID(req.ID, req.AccountToken, true, true)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCRansomOrderServer) Delete(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	data, err := s.Repository.Delete(req.UID, req.AccountToken, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCRansomOrderServer) History(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputGetHistoryList
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.History(req.UID, req.AccountToken, &unmarshledInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCRansomOrderServer) Contribute(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputContributeIntoRansomOrder
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.Contribute(req.UID, &unmarshledInput, req.AccountToken, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil
}
