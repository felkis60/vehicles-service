package models

import (
	"time"

	"gorm.io/gorm"
)

type Account struct {
	ID    int64  `json:"id" gorm:"primarykey"`
	Name  string `json:"name"`
	Token string `json:"token" gorm:"unique, not null"`

	ShtrafovnetAuthToken string `json:"shtrafovnet_auth_token"`
	ShtrafovnetCompanyID int64  `json:"shtrafovnet_company_id"`
	TelegramBotToken     string `json:"telegram_bot_token"`
	TelegramChatID       string `json:"telegram_chat_id"`

	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}

type AccountToken string
