package models

import (
	"time"

	"gorm.io/gorm"
)

type Fine struct {
	ID         int64          `json:"id" gorm:"primarykey"`
	UID        string         `json:"uid"`
	AccountsID int64          `json:"accounts_id"`
	VehiclesID int64          `json:"vehicles_id"`
	CreatedAt  time.Time      `json:"created_at"`
	UpdatedAt  time.Time      `json:"updated_at"`
	DeletedAt  gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`

	Number               string     `json:"number"`
	BillDate             string     `json:"bill_date"`
	Amount               int        `json:"amount"`
	AmountToPay          int        `json:"amount_to_pay"`
	PaymentStatus        string     `json:"payment_status"`
	DiscountDate         string     `json:"discount_date"`
	DiscountSize         int        `json:"discount_size"`
	ReplacedWith         string     `json:"replaced_with"`
	ChangeStatus         string     `json:"change_status"`
	ChangeStatusReason   string     `json:"change_status_reason"`
	ViolationDate        string     `json:"violation_date"`
	ViolatorName         string     `json:"violator_name"`
	Location             string     `json:"location"`
	Coordinates          string     `json:"coordinates"`
	KoapCode             string     `json:"koap_code"`
	KoapText             string     `json:"koap_text"`
	DivisionName         string     `json:"division_name"`
	DivisionCode         string     `json:"division_code"`
	Name                 string     `json:"name"`
	Org                  string     `json:"org"`
	Vin                  string     `json:"vin"`
	Sts                  string     `json:"sts"`
	PayeeUsername        string     `json:"payee_username"`
	PayeeInn             string     `json:"payee_inn"`
	PayeeKpp             string     `json:"payee_kpp"`
	PayeeOktmo           string     `json:"payee_oktmo"`
	PayeeKbk             string     `json:"payee_kbk"`
	PayeeBankName        string     `json:"payee_bank_name"`
	PayeeBankBik         string     `json:"payee_bank_bik"`
	PayeeBankAccount     string     `json:"payee_bank_account"`
	PayeeBankCorrAccount string     `json:"payee_bank_corr_account"`
	ManualPayment        bool       `json:"manual_payment"`
	FsspIP               string     `json:"fssp_ip"`
	FsspUin              string     `json:"fssp_uin"`
	OriginalCreatedAt    time.Time  `json:"original_created_at"`
	OriginalUpdatedAt    time.Time  `json:"original_updated_at"`
	OriginalArchivedAt   *time.Time `json:"original_archived_at"`
	Comment              string     `json:"comment"`
	CommentUpdatedAt     time.Time  `json:"comment_updated_at"`
	PaidAt               time.Time  `json:"paid_at"`
}

type FineFromSite struct {
	Number               string   `json:"number"`
	BillDate             string   `json:"bill_date"`
	Amount               float64  `json:"amount"`
	AmountToPay          float64  `json:"amount_to_pay"`
	PaymentStatus        string   `json:"payment_status"`
	DiscountDate         string   `json:"discount_date"`
	DiscountSize         float64  `json:"discount_size"`
	ReplacedWith         string   `json:"replaced_with"`
	ChangeStatus         string   `json:"change_status"`
	ChangeStatusReason   string   `json:"change_status_reason"`
	ViolationDate        string   `json:"violation_date"`
	ViolatorName         string   `json:"violator_name"`
	Location             string   `json:"location"`
	Coordinates          string   `json:"coordinates"`
	KoapCode             string   `json:"koap_code"`
	KoapText             string   `json:"koap_text"`
	DivisionName         string   `json:"division_name"`
	DivisionCode         string   `json:"division_code"`
	Name                 string   `json:"name"`
	Org                  string   `json:"org"`
	PayeeUsername        string   `json:"payee_username"`
	PayeeInn             string   `json:"payee_inn"`
	PayeeKpp             string   `json:"payee_kpp"`
	PayeeOktmo           string   `json:"payee_oktmo"`
	PayeeKbk             string   `json:"payee_kbk"`
	PayeeBankName        string   `json:"payee_bank_name"`
	PayeeBankBik         string   `json:"payee_bank_bik"`
	PayeeBankAccount     string   `json:"payee_bank_account"`
	PayeeBankCorrAccount string   `json:"payee_bank_corr_account"`
	Pics                 []string `json:"pics"`
	Docs                 []struct {
		Type   string `json:"type"`
		Number string `json:"number"`
		IsMain bool   `json:"is_main"`
	} `json:"docs"`
	ManualPayment bool        `json:"manual_payment"`
	FsspIP        string      `json:"fssp_ip"`
	FsspUin       string      `json:"fssp_uin"`
	CreatedAt     time.Time   `json:"created_at"`
	UpdatedAt     time.Time   `json:"updated_at"`
	ArchivedAt    interface{} `json:"archived_at"`
	BestMatchCar  struct {
		ID             int       `json:"id"`
		UnitID         string    `json:"unit_id"`
		Sts            string    `json:"sts"`
		Grz            string    `json:"grz"`
		Name           string    `json:"name"`
		Status         string    `json:"status"`
		InactiveReason string    `json:"inactive_reason"`
		CreatedAt      time.Time `json:"created_at"`
		UpdatedAt      time.Time `json:"updated_at"`
		ArchivedAt     time.Time `json:"archived_at"`
		MkadPass       []struct {
			Number     string    `json:"number"`
			Grz        string    `json:"grz"`
			ValidFrom  time.Time `json:"valid_from"`
			ValidTo    time.Time `json:"valid_to"`
			CancelDate time.Time `json:"cancel_date"`
			Zone       string    `json:"zone"`
			Status     string    `json:"status"`
			Type       string    `json:"type"`
		} `json:"mkad_pass"`
		Vin              string   `json:"vin"`
		RestrictionTypes []string `json:"restriction_types"`
	} `json:"best_match_car"`
	Unit struct {
		ID        string    `json:"id"`
		Name      string    `json:"name"`
		IsDefault bool      `json:"is_default"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
	} `json:"unit"`
	Driver struct {
		ID              int       `json:"id"`
		UnitID          string    `json:"unit_id"`
		LicenseNumber   string    `json:"license_number"`
		LicenseIssuedAt time.Time `json:"license_issued_at"`
		Name            string    `json:"name"`
		Status          string    `json:"status"`
		CreatedAt       time.Time `json:"created_at"`
		UpdatedAt       time.Time `json:"updated_at"`
		Restrictions    struct {
			Decisions []struct {
				Date        time.Time `json:"date"`
				FisID       string    `json:"fis_id"`
				BreachPlace string    `json:"breach_place"`
				Comment     string    `json:"comment"`
				RegName     string    `json:"reg_name"`
				State       string    `json:"state"`
				PeriodMonth int       `json:"period_month"`
				RegCode     string    `json:"reg_code"`
			} `json:"decisions"`
			Doc struct {
				Date   time.Time `json:"date"`
				Bdate  time.Time `json:"bdate"`
				Wanted string    `json:"wanted"`
				Reg    string    `json:"reg"`
				Cat    []string  `json:"cat"`
				Num    string    `json:"num"`
				VUch   string    `json:"v_uch"`
				Type   string    `json:"type"`
				Period time.Time `json:"period"`
				Divid  string    `json:"divid"`
				Status string    `json:"status"`
			} `json:"doc"`
			RequestTime time.Time `json:"request_time"`
		} `json:"restrictions"`
	} `json:"driver"`
	Owner struct {
		ID           string    `json:"id"`
		Type         string    `json:"type"`
		Form         string    `json:"form"`
		Status       string    `json:"status"`
		Rawid        string    `json:"rawid"`
		Inn          string    `json:"inn"`
		Kpp          string    `json:"kpp"`
		Name         string    `json:"name"`
		ShortName    string    `json:"short_name"`
		LegalAddress string    `json:"legal_address"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
	} `json:"owner"`
	RegistryStatus []struct {
		RegistryID      int       `json:"registry_id"`
		PayAmount       float64   `json:"pay_amount"`
		PaymentOrderURL string    `json:"payment_order_url"`
		ItemStatus      string    `json:"item_status"`
		RegistryStatus  string    `json:"registry_status"`
		PaidAt          time.Time `json:"paid_at"`
		CheckedAt       time.Time `json:"checked_at"`
		ConfirmedAt     time.Time `json:"confirmed_at"`
		CreatedAt       time.Time `json:"created_at"`
	} `json:"registry_status"`
	Ckadtx struct {
		ID           string    `json:"id"`
		Grz          string    `json:"grz"`
		Date         time.Time `json:"date"`
		Plaza        string    `json:"plaza"`
		Road         string    `json:"road"`
		Cost         float64   `json:"cost"`
		Status       string    `json:"status"`
		BestMatchCar struct {
			ID             int       `json:"id"`
			UnitID         string    `json:"unit_id"`
			Sts            string    `json:"sts"`
			Grz            string    `json:"grz"`
			Name           string    `json:"name"`
			Status         string    `json:"status"`
			InactiveReason string    `json:"inactive_reason"`
			CreatedAt      time.Time `json:"created_at"`
			UpdatedAt      time.Time `json:"updated_at"`
			ArchivedAt     time.Time `json:"archived_at"`
			MkadPass       []struct {
				Number     string    `json:"number"`
				Grz        string    `json:"grz"`
				ValidFrom  time.Time `json:"valid_from"`
				ValidTo    time.Time `json:"valid_to"`
				CancelDate time.Time `json:"cancel_date"`
				Zone       string    `json:"zone"`
				Status     string    `json:"status"`
				Type       string    `json:"type"`
			} `json:"mkad_pass"`
			Vin              string   `json:"vin"`
			RestrictionTypes []string `json:"restriction_types"`
		} `json:"best_match_car"`
		Unit struct {
			ID        string    `json:"id"`
			Name      string    `json:"name"`
			IsDefault bool      `json:"is_default"`
			CreatedAt time.Time `json:"created_at"`
			UpdatedAt time.Time `json:"updated_at"`
		} `json:"unit"`
		Photo  string    `json:"photo"`
		DueAt  time.Time `json:"due_at"`
		CkadID string    `json:"ckad_id"`
	} `json:"ckadtx"`
	Comment          string    `json:"comment"`
	CommentUpdatedAt time.Time `json:"comment_updated_at"`
	PaidAt           time.Time `json:"paid_at"`
	Ckad             struct {
		GracePeriod string  `json:"grace_period"`
		PayApcrr    float64 `json:"pay_apcrr"`
		URLPayApcrr string  `json:"url_pay_apcrr"`
	} `json:"ckad"`
}

type FinesFromSite struct {
	Fines []FineFromSite `json:"fines"`
}

type FinesFilters struct {
	VehicleNumber *string `json:"vehicle_number"`
	VehicleUID    *string `json:"vehicle_uid"`
	Search        *string `json:"search"`
}
type InputGetListFines struct {
	Page     int          `json:"page"`
	PageSize int          `json:"page_size"`
	OrderBy  string       `json:"order_by"`
	OrderDir string       `json:"order_direction" example:"asc|desc"`
	Filters  FinesFilters `json:"filters"`
}
