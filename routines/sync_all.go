package routines

import (
	"log"

	db "gitlab.com/felkis60/vehicles-service/database"
	"gitlab.com/felkis60/vehicles-service/repository"
	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"
)

func SyncAll() {
	var account bt.Account
	if result := db.Db.First(&account, "name = 'REX'"); result.RowsAffected == 0 {
		log.Println("ERROR: SYNC: No rex account!")
		return
	}
	syncVehicles(account.ID)
	//unloadingToRentLandingProvider()
	syncUsers(account.ID)
	syncRansomTariffs(account.ID)

	var tempManager bt.Manager
	if result := db.Db.Find(&tempManager, "phone = '0000000000' AND accounts_id = ?", account.ID); result.RowsAffected == 0 {
		var tempManagerRep = repository.ManagerRepository{Db: db.Db}
		tm, err := tempManagerRep.Create(&it.InputCreateManager{FirstName: "For sync", LastName: "For sync", Phone: "0000000000", Email: "For sync"}, account.ID, 0)
		if err != nil {
			log.Fatal(err.Error())
		}
		tempManager = *tm
	}

	syncRansomOrders(account.ID, tempManager.UID)
	//var envTimeInterval, err = strconv.Atoi(os.Getenv("SYNCINTERVALSECONDS"))
	//if err != nil {
	//	log.Fatal(err.Error())
	//}
	//var ticker = time.NewTicker(time.Duration(envTimeInterval) * time.Second)
	//for {
	//	select {
	//	case <-ticker.C:
	//		syncVehicles(account.ID)
	//		unloadingToRentLandingProvider()
	//		syncUsers(account.ID)
	//		syncRansomTariffs(account.ID)
	//		syncRansomOrders(account.ID, tempManager.UID)
	//	}
	//}
}
