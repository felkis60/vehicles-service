package models

import (
	"time"

	"gorm.io/datatypes"
)

type MandatoryPayment struct {
	ID                 int64     `json:"id" gorm:"primarykey"`
	RansomOrdersID     int64     `json:"ransom_orders_id" gorm:"index"`
	ContractYearStart  time.Time `json:"contract_year_start" gorm:"index"`
	ContractYearEnd    time.Time `json:"contract_year_end"`
	MandatoryAmount    float64   `json:"mandatory_amount"`
	ContributionAmount float64   `json:"contribution_amount"`

	ContributionAmountArray datatypes.JSON `json:"contribution_amount_array"`
}

type MandatoryPaymentOne struct {
	Date   time.Time `json:"date"`
	Amount float64   `json:"amout"`
}
