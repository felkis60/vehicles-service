package main

import (
	"os"

	pb "gitlab.com/felkis60/vehicles-service/api/grpc/app"
	"gitlab.com/felkis60/vehicles-service/api/grpc/controllers"
	db "gitlab.com/felkis60/vehicles-service/database"
	logs "gitlab.com/felkis60/vehicles-service/log"
	"gitlab.com/felkis60/vehicles-service/repository"

	"log"
	"net"

	start "gitlab.com/felkis60/vehicles-service/init"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {

	start.SystemStartup(true, true, true)

	lis, err := net.Listen("tcp", ":"+os.Getenv("GRPCPORT"))
	if err != nil {
		log.Fatalf(logs.ERRListen, err)
	}

	s := grpc.NewServer()

	pb.RegisterVehicleAccountServiceServer(s, &controllers.GRPCAccountServer{Repository: repository.AccountRepository{Db: db.Db}})
	pb.RegisterVehicleVehicleServiceServer(s, &controllers.GRPCVehiclesServer{Repository: repository.VehicleRepository{Db: db.Db}})
	pb.RegisterRentOrderServiceServer(s, &controllers.GRPCRentOrderServer{Repository: repository.RentOrderRepository{Db: db.Db}})
	pb.RegisterRentTariffServiceServer(s, &controllers.GRPCRentTariffServer{Repository: repository.RentTariffRepository{Db: db.Db}})
	pb.RegisterRansomTariffServiceServer(s, &controllers.GRPCRansomTariffServer{Repository: repository.RansomTariffRepository{Db: db.Db}})
	pb.RegisterRansomOrderServiceServer(s, &controllers.GRPCRansomOrderServer{Repository: repository.RansomOrderRepository{Db: db.Db}})
	pb.RegisterManagerServiceServer(s, &controllers.GRPCManagerServer{Repository: repository.ManagerRepository{Db: db.Db}})
	pb.RegisterVehicleUserServiceServer(s, &controllers.GRPCUserServer{Repository: repository.UserRepository{Db: db.Db}})
	pb.RegisterOrderServiceServer(s, &controllers.GRPCOrderServer{Repository: repository.OrderRepository{Db: db.Db}})
	pb.RegisterReminderServiceServer(s, &controllers.GRPCReminderServer{Repository: repository.ReminderRepository{Db: db.Db}})
	pb.RegisterSysInfoServiceServer(s, &controllers.GRPCSysInfoServer{Repository: repository.SysInfoRepository{Db: db.Db}})
	pb.RegisterAddInvestmentServiceServer(s, &controllers.GRPCAddInvestmentServer{Repository: repository.AddInvestmentRepository{Db: db.Db}})
	pb.RegisterFinesServiceServer(s, &controllers.GRPCFineServer{Repository: repository.FineRepository{Db: db.Db}})

	log.Printf(logs.INFStartServer, "grcp")
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf(logs.ERRServe, err)
	}

}
