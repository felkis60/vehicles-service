package models

import (
	"time"

	"gorm.io/gorm"
)

type AddInvestment struct {
	ID             int64      `json:"id" gorm:"primarykey"`
	UID            string     `json:"uid"`
	AccountsID     int64      `json:"accounts_id"`
	Title          string     `json:"title"`
	Type           string     `json:"type"`
	Description    string     `json:"description"`
	Amount         *float64   `json:"amount"`
	DayAmount      *float64   `json:"day_amount"`
	RelationsID    int64      `json:"relations_id"`
	RelationsTable string     `json:"relations_table"`
	Date           time.Time  `json:"date"`
	DateEnd        *time.Time `json:"date_end"`

	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}
