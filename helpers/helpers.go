package helpers

import (
	"errors"
	"log"
	"math"
	"strconv"
	"time"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
)

const serviceName = "[vehicles-service]"

func AddServiceNameToRedisKey(key *string) {
	*key = serviceName + *key
}

func AddServiceNameToError(err error) error {
	return errors.New(serviceName + err.Error())
}

func CalculateRansomOrderDayPayment(order *bt.RansomOrder, tariff *bt.RansomTariff) {
	if order.IsMandatoryPaymentsIncluded {
		order.DayPayment = dayPayment(tariff)
	} else {
		order.DayPayment = tariff.DayPayment
	}
}

func dayPayment(data *bt.RansomTariff) float64 {

	if data.Term == 0 {
		return 0
	}
	return math.Round(vehicleAmountWithRate(data) / (float64(data.Term) * 30.4167))
}

func InvestAmount(data *bt.RansomTariff) float64 {
	return math.Round(
		data.TotalVehicleAmount +
			totalTrackerAmount(data) +
			totalVehicleTaxAmount(data) +
			data.RegistrationAmount +
			data.TrackerAmount +
			totalInsuranceAmountOSAGO(data) -
			float64(data.InitialFee))
}

func totalTrackerAmount(data *bt.RansomTariff) float64 {
	return math.Round(data.TrackerServiceAmount * float64(data.Term))
}

func totalVehicleTaxAmount(data *bt.RansomTariff) float64 {
	return math.Round(data.VehicleTaxAmount * yearCoeff(data))
}

func yearCoeff(data *bt.RansomTariff) float64 {
	return math.Ceil(float64(data.Term) / 12.0)
}

func totalInsuranceAmountOSAGO(data *bt.RansomTariff) float64 {
	return math.Round(data.InsuranceOsagoAmount * yearCoeff(data))
}

func vehicleAmountWithRate(data *bt.RansomTariff) float64 {
	return math.Round(withoutInsuranceVehicleAmountWithRate(data) + insuranceAmount(data)) //|| 0
}

func insuranceAmount(data *bt.RansomTariff) float64 {
	return math.Round(withoutInsuranceVehicleAmountWithRate(data) * insuranceRatePercent(data))
}

func insuranceRatePercent(data *bt.RansomTariff) float64 {
	return float64(data.InsuranceRate) / 100
}

func withoutInsuranceVehicleAmountWithRate(data *bt.RansomTariff) float64 {
	return math.Round(InvestAmount(data) + investAmountInMonthByRate(data)*float64(data.Term)) //|| 0
}

func investAmountInMonthByRate(data *bt.RansomTariff) float64 {
	return math.Round((InvestAmount(data) * ratePercent(data)) / 12.0)
}

func ratePercent(data *bt.RansomTariff) float64 {
	return data.YearRate / 100.0
}

func CalculateDayPaymentForDayWithAddInvestmentsString(investments *[]bt.AddInvestment, usualDayPayment float64, day string, endOfOrder string, yearRate *float64) (float64, error) {

	tempDayTime, err := time.Parse("2006-01-02", day)
	if err != nil {
		return 0, err
	}

	tempendOfOrderTime, err := time.Parse("2006-01-02", endOfOrder)
	if err != nil {
		return 0, err
	}

	return CalculateDayPaymentForDayWithAddInvestmentsTime(investments, usualDayPayment, tempDayTime, tempendOfOrderTime, yearRate), nil
}

func CalculateDayPaymentForDayWithAddInvestmentsTime(investments *[]bt.AddInvestment, usualDayPayment float64, day time.Time, endOfOrder time.Time, yearRate *float64) float64 {
	if usualDayPayment < 0 {
		usualDayPayment *= -1
	}
	log.Print("-----------------")
	log.Print("endOfOrder: ", endOfOrder)
	log.Print("day: ", day)
	log.Print("usualDayPayment: " + strconv.FormatFloat(usualDayPayment, 'f', 3, 64))
	log.Printf("investments.lenght: %d", len(*investments))
	var tempAddSum = float64(0.0)
	for i := range *investments {
		log.Printf("(*investments)[i].Date: %s", (*investments)[i].Date.Format("2006-01-02"))
		if !(*investments)[i].Date.After(day) {
			log.Printf("(*investments)[i].DateEnd: %t", (*investments)[i].DateEnd == nil)
			log.Printf("(*investments)[i].DateEnd: %t", ((*investments)[i].DateEnd != nil && day.Before(*(*investments)[i].DateEnd)) || (*investments)[i].DateEnd == nil)
			if ((*investments)[i].DateEnd != nil && day.Before(*(*investments)[i].DateEnd)) || (*investments)[i].DateEnd == nil {
				log.Print("(*investments)[i].Type: " + (*investments)[i].Type)
				if (*investments)[i].Type == "day_amount" && (*investments)[i].DayAmount != nil {
					tempAddSum += *(*investments)[i].DayAmount
				} else {
					tempAddSum += *(*investments)[i].Amount / (endOfOrder.Sub((*investments)[i].Date).Hours() / 24.0)
				}
			}
		}
	}

	// if yearRate != nil {
	// 	log.Print("yearRate: " + strconv.FormatFloat(*yearRate, 'f', 3, 64))
	// 	tempAddSum *= (1.0 + (*yearRate / (30.4167 * 12.0)))
	// }

	usualDayPayment += tempAddSum
	log.Print("usualDayPayment after sum: " + strconv.FormatFloat(usualDayPayment, 'f', 3, 64))

	usualDayPayment = math.Round(usualDayPayment)
	log.Print("usualDayPayment after round: " + strconv.FormatFloat(usualDayPayment, 'f', 3, 64))
	return usualDayPayment
}
