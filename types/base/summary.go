package models

//type Summary struct {
//	NumberOfVehicles                int64            `json:"number_of_vehicles"`
//	NumberOfActiveRentOrders        int64            `json:"number_of_active_rent_orders"`
//	NumberOfActiveRansomOrders      int64            `json:"number_of_active_ransom_orders"`
//	NumberOfVehiclesGroupedByStatus map[string]int64 `json:"number_of_vehicles_grouped_by_status"`
//}

type PeriodCashSummary struct {
	BalanceReplenishment float64 `json:"balance_replenishment"` //Opertaions Category 10
	MonthlyPayments      float64 `json:"monthly_payments"`      //Opertaions Category 8
	Delta                float64 `json:"delta"`                 //Opertaions Category 8 - Opertaions Category 10
	TotalRepairAmount    float64 `json:"total_repair_amount"`   //Opertaions Category 21
}

type OrdersPeriodSummary struct {
	//All
	TotalNewAndEndedOrders int64 `json:"total_new_and_ended_orders"`
	TotalEndedOrders       int64 `json:"total_ended_orders"`
	TotalNewOrders         int64 `json:"total_new_orders"`
	//Rent
	TotalNewAndEndedRentOrders int64 `json:"total_new_and_ended_rent_orders"`
	TotalEndedRentOrders       int64 `json:"total_ended_rent_orders"`
	TotalNewRentOrders         int64 `json:"total_new_rent_orders"`
	//Ransom
	TotalNewAndEndedRansomOrders int64 `json:"total_new_and_ended_ransom_orders"`
	TotalEndedRansomOrders       int64 `json:"total_ended_ransom_orders"`
	TotalNewRansomOrders         int64 `json:"total_new_ransom_orders"`
	//Sums
	RentCashSummary   PeriodCashSummary `json:"rent_cash_summary"`
	RansomCashSummary PeriodCashSummary `json:"ransom_cash_summary"`
	TotalCashSummary  PeriodCashSummary `json:"total_cash_summary"`
}
