package models

type InputCreateManager struct {
	ExtID      int64  `json:"ext_id"`
	FirstName  string `json:"first_name"`
	MiddleName string `json:"middle_name"`
	LastName   string `json:"last_name"`
	Phone      string `json:"phone"`
	Email      string `json:"email"`
	Birthday   string `json:"birthday"`
}

type InputEditManager struct {
	ExtID      *int64  `json:"ext_id"`
	FirstName  *string `json:"first_name"`
	MiddleName *string `json:"middle_name"`
	LastName   *string `json:"last_name"`
	Phone      *string `json:"phone"`
	Email      *string `json:"email"`
	Birthday   *string `json:"birthday"`
}
