package controllers

import (
	"gitlab.com/felkis60/vehicles-service/repository"
	_ "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type RentOrderController struct {
	Repository repository.RentOrderRepository
}

// @Summary Create new order
// @Tags Rent Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ 	body it.InputCreateRentOrder true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-orders/ [post]
func (ctrl *RentOrderController) Create(c *gin.Context) {
	var input it.InputCreateRentOrder
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Create new multiple orders
// @Tags Rent Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ 	body []it.InputCreateRentOrder true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-orders/multiple [post]
func (ctrl *RentOrderController) CreateMultiple(c *gin.Context) {
	var input []it.InputCreateRentOrder
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	rentOrders, err := ctrl.Repository.CreateMultiple(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: *rentOrders})
}

// @Summary Delete rent order
// @Tags Rent Orders
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Rent Order UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-orders/{uid}/delete [post]
func (ctrl *RentOrderController) Delete(c *gin.Context) {

	data, err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Edit existing order
// @Tags Rent Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ 	body it.InputEditRentOrder true "Request body"
// @Param uid path string true "User UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-orders/{uid}/edit [post]
func (ctrl *RentOrderController) Edit(c *gin.Context) {
	var input it.InputEditRentOrder
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), &input, c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get one rent order
// @Tags Rent Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Rent Order UID"
// @Success 200 {object} _.RentOrder
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-orders/{uid} [post]
func (ctrl *RentOrderController) GetByUID(c *gin.Context) {

	rentOrder, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string), true, true)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: rentOrder})
}

// @Summary Get all rent orders for one vehicle in given time gap
// @Tags Rent Orders
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListRentOrders true "Request body"
// @Success 200 {object} []_.RentOrder
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/rent-orders/list [post]
func (ctrl *RentOrderController) List(c *gin.Context) {
	var input it.InputGetListRentOrders
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}
