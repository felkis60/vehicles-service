package controllers

import (
	"gitlab.com/felkis60/vehicles-service/repository"

	_ "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type RansomTariffController struct {
	Repository repository.RansomTariffRepository
}

// @Summary Create ransom tariff
// @Tags Ransom Tariffs
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param   _     body it.InputCreateRansomTariff true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-tariffs/ [post]
func (ctrl *RansomTariffController) Create(c *gin.Context) {
	var input it.InputCreateRansomTariff
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Edit ransom tariff
// @Tags Ransom Tariffs
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param   _     body it.InputEditRansomTariff true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-tariffs/{uid}/edit [post]
func (ctrl *RansomTariffController) Edit(c *gin.Context) {
	var input it.InputEditRansomTariff
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), &input, c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Delete ransom tariff
// @Tags Ransom Tariffs
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Ransom Tariff UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-tariffs/{uid}/delete [post]
func (ctrl *RansomTariffController) Delete(c *gin.Context) {

	data, err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string), 0)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get one ransom tariff
// @Tags Ransom Tariffs
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Ransom Tariff UID"
// @Success 200 {object} _.RansomTariff
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-tariffs/{uid} [post]
func (ctrl *RansomTariffController) GetByUID(c *gin.Context) {

	data, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string), true)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get ransom tariffs
// @Tags Ransom Tariffs
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetRansomTariffList true "Request body"
// @Success 200 {object} []_.RansomTariff
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/ransom-tariffs/list [post]
func (ctrl *RansomTariffController) List(c *gin.Context) {
	var input it.InputGetRansomTariffList
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	pagInfo, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: pagInfo})
}
