package repository

import (
	"errors"
	"log"
	"strings"
	"time"

	db "gitlab.com/felkis60/vehicles-service/database"
	"gitlab.com/felkis60/vehicles-service/helpers/history"
	logs "gitlab.com/felkis60/vehicles-service/log"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

const (
	rentOrderTableName = "rent_orders"
)

type RentOrderRepository struct {
	Db *gorm.DB
}

func (rep *RentOrderRepository) findAllUIDs(data *bt.RentOrder, vehUID, userUID, managerUID, tariffUID *string, accountToken string) error {
	if vehUID != nil {
		var vehRep = VehicleRepository{Db: rep.Db}
		vehicle, err := vehRep.GetByUID(*vehUID, accountToken, false)
		if err != nil {
			return err
		}
		data.VehiclesID = vehicle.ID
		data.VehicleUID = &vehicle.UID
	}

	if userUID != nil {
		var userRep = UserRepository{Db: rep.Db}
		user, err := userRep.GetByUID(*userUID, accountToken, false)
		if err != nil {
			return err
		}
		data.UsersID = user.ID
		data.UserUID = &user.UID
	}

	if managerUID != nil {
		var managerRep = ManagerRepository{Db: rep.Db}
		manager, err := managerRep.GetByUID(*managerUID, accountToken)
		if err != nil {
			return err
		}
		data.ManagersID = manager.ID
	}

	if tariffUID != nil {
		var rentTarRep = RentTariffRepository{Db: rep.Db}
		rentTariff, err := rentTarRep.GetByUID(*tariffUID, accountToken, false)
		if err != nil {
			return err
		}
		data.TariffsID = rentTariff.ID
	}
	return nil
}

func (rep *RentOrderRepository) checkOrder(data *bt.RentOrder) error {

	if data.VehiclesID <= 0 {
		return errors.New(logs.ERRNoVehicleInOrder)
	}
	if data.UsersID <= 0 {
		return errors.New(logs.ERRNoUserInOrder)
	}
	if data.ManagersID <= 0 {
		return errors.New(logs.ERRNoManagerInOrder)
	}
	if data.TariffsID <= 0 {
		return errors.New(logs.ERRNoTariffInOrder)
	}

	if data.DateStart.IsZero() || data.DateEnd.IsZero() || data.DateStart.After(data.DateEnd) {
		return errors.New("ERROR: 'date_end' must be greater than 'date_start'")
	}

	if data.DateEnd.Sub(data.DateStart).Hours()/24 > 365 {
		return errors.New("ERROR: rent time must be no more than 1 year")
	}

	{
		var tempTariff bt.RentTariff
		rep.Db.Find(&tempTariff, "id = ?", data.TariffsID)
		if tempTariff.VehiclesID == 0 || data.VehiclesID == 0 {
			return errors.New(logs.ERRWrongTariff)
		}
		if tempTariff.VehiclesID != data.VehiclesID {
			return errors.New(logs.ERRWrongTariff)
		}
	}
	{
		var orders []bt.RentOrder
		if result := db.DbDateTimeIntersect(data.DateStart, &data.DateEnd, rep.Db, "rent_orders.date_start", "rent_orders.date_end").Where("rent_orders.vehicles_id = ? AND rent_orders.id != ?", data.VehiclesID, data.ID).Find(&orders); result.RowsAffected > 0 {
			return errors.New(logs.ERRBusyVehicle)
		}
	}
	return nil
}

func (rep *RentOrderRepository) Create(input *it.InputCreateRentOrder, accountID int64, authUserID int64) (*bt.RentOrder, error) {

	var account bt.Account
	rep.Db.First(&account, accountID)

	var data bt.RentOrder

	if err := rep.findAllUIDs(&data, &input.VehiclesUID, &input.UsersUID, &input.ManagersUID, &input.TariffsUID, account.Token); err != nil {
		return nil, err
	}

	data.UID = uuid.Must(uuid.NewV4()).String()
	data.AccountsID = accountID
	data.DateStart = input.DateStart
	data.DateEnd = input.DateEnd
	data.Status = bt.StatusPreliminary
	data.IsActive = input.IsActive
	data.DepositPayout = input.DepositPayout
	data.DateEndDescription = input.DateEndDescription
	data.ReasonForTerminating = input.ReasonForTerminating

	data.DateStart = time.Date(data.DateStart.Year(), data.DateStart.Month(), data.DateStart.Day(), data.DateStart.Hour(), data.DateStart.Minute(), 0, 0, data.DateStart.Location())
	data.DateEnd = time.Date(data.DateEnd.Year(), data.DateEnd.Month(), data.DateEnd.Day(), data.DateEnd.Hour(), data.DateEnd.Minute(), 0, 0, data.DateEnd.Location())

	if err := rep.checkOrder(&data); err != nil {
		return nil, err
	}

	if data.IsActive {
		rep.Db.Model(&bt.RentOrder{}).Where("rent_orders.is_active = 'true' AND rent_orders.vehicles_id = ?", data.VehiclesID).Update("is_active", false)
	}

	var tx = rep.Db

	if data.IsActive {
		var vehRep = VehicleRepository{Db: rep.Db}
		vehicle, err := vehRep.GetByID(data.VehiclesID, account.Token, false)
		if err != nil {
			return nil, err
		}

		tx = tx.Begin()
		vehicle.Status = "RENT"
		if result := tx.Save(vehicle); result.Error != nil {
			tx.Rollback()
			return nil, result.Error
		}

	}

	if result := rep.Db.Create(&data); result.Error != nil {
		if data.IsActive {
			tx.Rollback()
		}
		return nil, result.Error
	}

	if data.IsActive {
		tx.Commit()
	}

	if err := history.CreateHistory(data.ID, rentOrderTableName, "create", nil, data, authUserID, accountID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	var tempRentTariffs bt.RentTariff
	if result := rep.Db.Find(&tempRentTariffs, "id = ?", data.TariffsID); result.RowsAffected > 0 {
		data.RentTariff = &tempRentTariffs
	}

	var tempUser bt.User
	if result := rep.Db.Find(&tempUser, "id = ?", data.UsersID); result.RowsAffected > 0 {
		data.User = &tempUser
	}

	var tempVehicle bt.Vehicle
	if result := rep.Db.Find(&tempVehicle, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
		data.Vehicle = &tempVehicle
	}

	//if natsH.EncodedConnection != nil {
	//	if err := natsH.EncodedConnection.Publish("rent-order-created", &data); err != nil {
	//		log.Print("ERROR: " + err.Error())
	//	}
	//} else {
	//	log.Print("INFO: No nats connection. No bill for rent order")
	//}

	return &data, nil

}

func (rep *RentOrderRepository) CreateMultiple(inputs *[]it.InputCreateRentOrder, accountID int64, authUserID int64) (*[]*bt.RentOrder, error) {
	if len(*inputs) < 1 {
		return nil, errors.New("ERROR: no rent_orders to create")
	}

	txRep := RentOrderRepository{rep.Db.Begin()}

	var data []*bt.RentOrder
	for _, input := range *inputs {
		dataChunk, err := txRep.Create(&input, accountID, authUserID)
		if err != nil {
			txRep.Db.Rollback()
			return nil, err
		}
		data = append(data, dataChunk)
	}
	txRep.Db.Commit()
	return &data, nil
}

func (rep *RentOrderRepository) Delete(uid string, accountToken string, authUserID int64) (*bt.RentOrder, error) {

	data, err := rep.GetByUID(uid, accountToken, false, false)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	rep.Db.Delete(data)

	if err := history.CreateHistory(data.ID, rentOrderTableName, "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil
}

func (rep *RentOrderRepository) Edit(uid string, input *it.InputEditRentOrder, accountToken string, authUserID int64) (*bt.RentOrder, error) {

	data, err := rep.GetByUID(uid, accountToken, false, false)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	if err := rep.findAllUIDs(data, input.VehiclesUID, input.UsersUID, input.ManagersUID, input.TariffsUID, accountToken); err != nil {
		return nil, err
	}

	if input.Status != nil {
		tempStatus := strings.ReplaceAll(*input.Status, " ", "")
		tempStatus = strings.ToUpper(tempStatus)
		if !bt.CheckStatus(tempStatus) {
			return nil, errors.New(logs.ERRWrongStatus)
		}
		data.Status = tempStatus
	}
	if input.DateStart != nil {
		data.DateStart = *input.DateStart
		data.DateStart = time.Date(data.DateStart.Year(), data.DateStart.Month(), data.DateStart.Day(), data.DateStart.Hour(), data.DateStart.Minute(), 0, 0, data.DateStart.Location())
	}
	if input.DateEnd != nil {
		data.DateEnd = *input.DateEnd
		data.DateEnd = time.Date(data.DateEnd.Year(), data.DateEnd.Month(), data.DateEnd.Day(), data.DateEnd.Hour(), data.DateEnd.Minute(), 0, 0, data.DateEnd.Location())
	}
	if input.IsActive != nil {
		data.IsActive = *input.IsActive
	}
	if input.DepositPayout != nil {
		data.DepositPayout = *input.DepositPayout
	}
	if input.DateEndDescription != nil {
		data.DateEndDescription = input.DateEndDescription
	}
	if input.ReasonForTerminating != nil {
		data.ReasonForTerminating = input.ReasonForTerminating
		if *data.ReasonForTerminating == "" {
			data.ReasonForTerminating = nil
		}
	}
	if input.VehicleStatus != nil {
		if CheckVehicleStatus(*input.VehicleStatus) {
			var vehicleFromOrder bt.Vehicle
			if result := rep.Db.Find(&vehicleFromOrder, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
				vehicleFromOrder.Status = *input.VehicleStatus
				rep.Db.Save(&vehicleFromOrder)
			}
		}
	}

	if err := rep.checkOrder(data); err != nil {
		return nil, err
	}

	if data.IsActive {
		rep.Db.Model(&bt.RentOrder{}).Where("rent_orders.is_active = 'true' AND rent_orders.vehicles_id = ?", data.VehiclesID).Update("is_active", false)
	}

	var tx = rep.Db

	if input.IsActive != nil {
		var vehRep = VehicleRepository{Db: rep.Db}
		vehicle, err := vehRep.GetByID(data.VehiclesID, accountToken, false)
		if err != nil {
			return nil, err
		}

		tx = tx.Begin()

		if data.IsActive {
			vehicle.Status = "RENT"
		} else {
			var activeCount int64
			rep.Db.Model(&bt.RentOrder{}).Where("rent_orders.is_active = 'true' AND rent_orders.vehicles_id = ?", data.VehiclesID).Count(&activeCount)
			if activeCount <= 0 {
				vehicle.Status = "FREE"
			}
		}
		if result := tx.Save(vehicle); result.Error != nil {
			tx.Rollback()
			return nil, result.Error
		}

	}

	if result := rep.Db.Save(data); result.Error != nil {
		if input.IsActive != nil {
			tx.Rollback()
		}
		return nil, result.Error
	}

	if input.IsActive != nil {
		tx.Commit()
	}

	if err := history.CreateHistory(data.ID, rentOrderTableName, "edit", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	var tempRentTariffs bt.RentTariff
	if result := rep.Db.Find(&tempRentTariffs, "id = ?", data.TariffsID); result.RowsAffected > 0 {
		data.RentTariff = &tempRentTariffs
	}

	var tempUser bt.User
	if result := rep.Db.Find(&tempUser, "id = ?", data.UsersID); result.RowsAffected > 0 {
		data.User = &tempUser
	}

	var tempVehicle bt.Vehicle
	if result := rep.Db.Find(&tempVehicle, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
		data.Vehicle = &tempVehicle

	}

	return data, nil
}

func (rep *RentOrderRepository) GetByID(ID int64, accountToken string, withNestedData bool, withAuto bool) (*bt.RentOrder, error) {
	var data bt.RentOrder
	if result := helpers.DbAccountLeftJoin(rentOrderTableName, accountToken, rep.Db).Find(&data, "rent_orders.id = ? AND rent_orders.deleted_at IS NULL", ID); result.RowsAffected == 0 {
		return nil, errors.New("ERROR: No such Rent Order ID")
	}

	if withNestedData {

		var tempRentTariff bt.RentTariff
		if result := rep.Db.Find(&tempRentTariff, "id = ?", data.TariffsID); result.RowsAffected > 0 {
			data.RentTariff = &tempRentTariff
		}

		var tempUser bt.User
		if result := rep.Db.Find(&tempUser, "id = ?", data.UsersID); result.RowsAffected > 0 {
			data.User = &tempUser
		}

		var tempManager bt.Manager
		if result := rep.Db.Find(&tempManager, "id = ?", data.ManagersID); result.RowsAffected > 0 {
			data.Manager = &tempManager
		}

		if withAuto {
			var tempVehicle bt.Vehicle
			if result := rep.Db.Find(&tempVehicle, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
				data.Vehicle = &tempVehicle
			}
		}
	}

	return &data, nil
}

func (rep *RentOrderRepository) GetByUID(UID string, accountToken string, withNestedData bool, withAuto bool) (*bt.RentOrder, error) {
	var data bt.RentOrder
	if result := helpers.DbAccountLeftJoin(rentOrderTableName, accountToken, rep.Db).Find(&data, "rent_orders.uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchOrdersUID)
	}

	if withNestedData {

		var tempRentTariff bt.RentTariff
		if result := rep.Db.Find(&tempRentTariff, "id = ?", data.TariffsID); result.RowsAffected > 0 {
			data.RentTariff = &tempRentTariff
		}

		var tempUser bt.User
		if result := rep.Db.Find(&tempUser, "id = ?", data.UsersID); result.RowsAffected > 0 {
			data.User = &tempUser
		}

		var tempManager bt.Manager
		if result := rep.Db.Find(&tempManager, "id = ?", data.ManagersID); result.RowsAffected > 0 {
			data.Manager = &tempManager
		}

		if withAuto {
			var tempVehicle bt.Vehicle
			if result := rep.Db.Find(&tempVehicle, "id = ?", data.VehiclesID); result.RowsAffected > 0 {
				data.Vehicle = &tempVehicle
			}
		}
	}

	return &data, nil
}

func (rep *RentOrderRepository) List(accountToken string, input *it.InputGetListRentOrders) (*helpers.Pagination, error) {

	if input.Filters.DateStart != nil && input.Filters.DateEnd != nil && input.Filters.DateStart.After(*input.Filters.DateEnd) {
		return nil, errors.New("ERROR: 'date_end' must be greater than 'date_start'")
	}

	var tx = helpers.DbAccountLeftJoin(rentOrderTableName, accountToken, rep.Db.Model(&bt.RentOrder{}))

	//Filters
	if input.Filters.VehiclesUID != nil {
		tx = tx.Joins("LEFT JOIN vehicles ON rent_orders.vehicles_id = vehicles.id").Where("vehicles.uid = ?", *input.Filters.VehiclesUID)
	}

	if input.Filters.Status != nil {
		tempStatus := strings.ReplaceAll(*input.Filters.Status, " ", "")
		tempStatus = strings.ToUpper(tempStatus)
		statuses := strings.Split(tempStatus, ",")
		tx = tx.Where("rent_orders.status IN ?", statuses)
	}

	if input.Filters.UsersPhone != nil {
		var userRep = UserRepository{Db: rep.Db}
		user, err := userRep.GetByPhone(accountToken, *input.Filters.UsersPhone)
		if err != nil {
			return nil, err
		}
		tx = tx.Where("rent_orders.users_id = ?", user.ID)
	}

	if input.Filters.IsActive != nil {
		tx = tx.Where("rent_orders.is_active = ?", input.Filters.IsActive)
	}

	var tempDateStart = time.Time{}
	if input.Filters.DateStart != nil {
		tempDateStart = *input.Filters.DateStart
	}

	tx = db.DbDateTimeIntersect(tempDateStart, input.Filters.DateEnd, tx, "rent_orders.date_start", "rent_orders.date_end")

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.RentOrder
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, rentOrderTableName, &data)(tx)

}

func (rep *RentOrderRepository) Search(accountToken string, input *it.InputGetListRentOrders) (*[]map[string]interface{}, error) {

	var existingKeys = map[string]bool{
		"rent_order_id":       true,
		"rent_order_uid":      true,
		"rent_users_id":       true,
		"rent_users_uid":      true,
		"vehicles_id":         true,
		"vehicles_uid":        true,
		"rent_users_phone":    true,
		"rent_users_fullname": true,
		"vehicles_fullname":   true,
		"day_payment":         true,
		"date_start":          true,
		"date_end":            true,
	}

	// userFullName, vehicleFullname, uids, idRentOrder, UserPhone, dayPeyment, dayStart. dateEnd, balance, dayDiff. daysOfDelay, paymentDate
	var tx = rep.Db.Table(rentOrderTableName)
	tx = tx.Select("*, rent_orders.id AS rent_order_id, rent_orders.uid AS rent_order_uid, u.id AS rent_users_id, u.uid AS rent_users_uid, v.id AS vehicles_id, v.uid AS vehicles_uid, v.number AS vehicles_number, u.phone AS rent_users_phone, CONCAT(u.last_name, ' ', u.first_name) AS rent_users_fullname, CONCAT(v.mark, ' ', v.model, ' ', v.number) AS vehicles_fullname, rt.day_payment AS day_payment, rent_orders.date_start AS date_start, rent_orders.date_end AS date_end")
	tx = tx.Joins("LEFT JOIN users AS u ON rent_orders.users_id = u.id")
	tx = tx.Joins("LEFT JOIN vehicles AS v ON rent_orders.vehicles_id = v.id")
	tx = tx.Joins("LEFT JOIN rent_tariffs AS rt ON rent_orders.tariffs_id = rt.id")
	tx = helpers.DbAccountLeftJoin("rent_orders", accountToken, tx)

	if input.Filters.Type != nil {
		switch *input.Filters.Type {
		case "active":
			tx = tx.Where("rent_orders.date_start IS NULL")
		case "contractEnd":
			tx = tx.Where("rans_orders.date_end IS NOT NULL")
		}
	}

	if input.Filters.Search != nil {
		tx = tx.Where("CONCAT(u.last_name, ' ', u.first_name, ' ', v.mark, ' ', v.model, ' ', v.number, ' ', u.phone) ILIKE '%" + *input.Filters.Search + "%'")
	}

	if input.Filters.IsActive != nil {
		tx = tx.Where("rent_orders.is_active = ?", *input.Filters.IsActive)
	}

	if input.Filters.IncludeUserUIDs != nil {
		tx = tx.Where("u.uid IN (?)", *input.Filters.IncludeUserUIDs)
	}

	if input.Filters.ExcludeUserUIDs != nil {
		tx = tx.Where("u.uid NOT IN (?)", *input.Filters.ExcludeUserUIDs)
	}

	if input.OrderBy != "" {
		if _, ok := existingKeys[input.OrderBy]; ok {
			var tempDir = "asc"
			if input.OrderDir != "" {
				if input.OrderDir == "descending" || input.OrderDir == "desc" {
					tempDir = "desc"
				}
			}
			if input.OrderBy == "date_start" || input.OrderBy == "date_end" {
				tx = tx.Order("rent_orders." + input.OrderBy + " " + tempDir)
			} else {
				tx = tx.Order(input.OrderBy + " " + tempDir)
			}
		}
	}

	var data []map[string]interface{}

	if err := tx.Scan(&data).Error; err != nil {
		return nil, err
	}

	for i := range data {
		var addInvestments []map[string]interface{}
		helpers.DbAccountLeftJoin(addInvestmentTableName, accountToken, rep.Db.Model(&bt.AddInvestment{})).Where("add_investments.deleted_at is NULL AND add_investments.relations_id = ? AND add_investments.relations_table = 'rent_order'", data[i]["rent_order_id"]).Find(&addInvestments)
		if addInvestments != nil {
			data[i]["add_investments"] = addInvestments
		}
	}

	return &data, nil

}

func (rep *RentOrderRepository) History(UID string, accountToken string, input *it.InputGetHistoryList) (*helpers.Pagination, error) {

	//Getting base model data
	dataByUID, err := rep.GetByUID(UID, accountToken, false, false)
	if err != nil {
		return nil, err
	}

	//Filters
	tx := rep.Db.Model(&bt.History{}).Where("source_table = ? AND source_id = ?", rentOrderTableName, dataByUID.ID)

	if input.Filters.OperationType != nil {
		tx = tx.Where("operation_type = ?", *input.Filters.OperationType)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.History
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, "histories", &data)(tx)
}
