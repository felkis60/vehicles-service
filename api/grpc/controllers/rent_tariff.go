package controllers

import (
	"context"
	"encoding/json"
	"errors"

	pb "gitlab.com/felkis60/vehicles-service/api/grpc/app"
	"gitlab.com/felkis60/vehicles-service/helpers"
	logs "gitlab.com/felkis60/vehicles-service/log"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	"gitlab.com/felkis60/vehicles-service/repository"
)

type GRPCRentTariffServer struct {
	Repository repository.RentTariffRepository
	pb.RentTariffServiceServer
}

func (s *GRPCRentTariffServer) List(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputGetRentTariffList
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	pagInfo, err := s.Repository.List(req.AccountToken, &unmarshledInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonPage, err := json.Marshal(pagInfo)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonPage}, nil
}

func (s *GRPCRentTariffServer) Create(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputCreateRentTariff
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.Create(&unmarshledInput, account.ID, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonVehicle, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonVehicle}, nil
}

func (s *GRPCRentTariffServer) Edit(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputEditRentTariff
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.Edit(req.UID, &unmarshledInput, req.AccountToken, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonVehicle, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonVehicle}, nil
}

func (s *GRPCRentTariffServer) Get(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	vehicle, err := s.Repository.GetByUID(req.UID, req.AccountToken, true)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonVehicle, err := json.Marshal(vehicle)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonVehicle}, nil
}

func (s *GRPCRentTariffServer) Delete(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	data, err := s.Repository.Delete(req.UID, req.AccountToken, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonVehicle, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonVehicle}, nil
}

func (s *GRPCRentTariffServer) History(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	var account bt.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var unmarshledInput it.InputGetHistoryList
	if err := json.Unmarshal(req.Body, &unmarshledInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.History(req.UID, req.AccountToken, &unmarshledInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}
