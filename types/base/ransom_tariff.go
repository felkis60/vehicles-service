package models

import (
	"time"

	"gorm.io/gorm"
)

type RansomTariff struct {
	ID                                 int64          `json:"id" gorm:"primarykey"`
	UID                                string         `json:"uid"`
	ExtID                              int64          `json:"ext_id"`
	AccountsID                         int64          `json:"accounts_id"`
	VehiclesID                         int64          `json:"vehicles_id"`
	InitialFee                         int            `json:"initial_fee"`
	VehicleAmount                      float64        `json:"vehicle_amount"`
	TrackerAmount                      float64        `json:"tracker_amount"`
	TrackerServiceAmount               float64        `json:"tracker_service_amount"`
	SourceTrackerMonthAmount           float64        `json:"source_tracker_month_amount"`
	VehicleTaxAmount                   float64        `json:"vehicle_tax_amount"`
	RegistrationAmount                 float64        `json:"regisration_amount"`
	InsuranceOsagoAmount               float64        `json:"insurance_osago_amount"`
	Term                               int            `json:"term"`
	YearRate                           float64        `json:"year_rate"`
	InsuranceRate                      int            `json:"insurance_rate"`
	MonthPayment                       float64        `json:"month_payment"`
	DayPayment                         float64        `json:"day_payment"`
	TotalVehicleAmount                 float64        `json:"total_vehicle_amount"`
	TotalVehicleWithoutInsuranceAmount float64        `json:"total_vehicle_without_insurance_amount"`
	Vehicle                            *Vehicle       `json:"vehicle,omitempty" gorm:"-"`
	CreatedAt                          time.Time      `json:"created_at"`
	UpdatedAt                          time.Time      `json:"updated_at"`
	DeletedAt                          gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}
