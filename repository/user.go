package repository

import (
	"errors"
	"log"
	"time"

	"gitlab.com/felkis60/vehicles-service/helpers/history"
	logs "gitlab.com/felkis60/vehicles-service/log"

	bt "gitlab.com/felkis60/vehicles-service/types/base"
	it "gitlab.com/felkis60/vehicles-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

const (
	userTableName = "users"
)

type UserRepository struct {
	Db *gorm.DB
}

func (rep *UserRepository) checkUser(data *bt.User) error {
	//if data.FirstName == "" {
	//	return errors.New(logs.ErrFirstNameReq)
	//}
	//if data.LastName == "" {
	//	return errors.New(logs.ErrNameReq)
	//}
	if data.Phone == "" {
		return errors.New(logs.ERRPhoneReq)
	}
	return nil
}

func (rep *UserRepository) Create(input *it.InputCreateUser, accountID int64, authUserID int64) (*bt.User, error) {

	var data bt.User

	input.Phone = helpers.CleanPhone(input.Phone)

	if result := rep.Db.Find(&data, "phone = ? AND accounts_id = ?", input.Phone, accountID); result.RowsAffected != 0 {
		return nil, errors.New(logs.ERRExistingPhoNum)
	}

	data.ExtID = input.ExtID
	data.FirstName = input.FirstName
	data.LastName = input.LastName
	data.MiddleName = input.MiddleName
	data.Phone = helpers.CleanPhone(input.Phone)
	data.FirebaseUID = input.FirebaseUID
	data.Firebase = input.Firebase

	data.PartnerType = input.PartnerType
	data.PassportNumber = input.PassportNumber
	data.PassportSeries = input.PassportSeries
	data.PassportIssuedBy = input.PassportIssuedBy
	if input.PassportDateOfIssue != "" {
		tempTime, err := time.Parse("2006-01-02", input.PassportDateOfIssue)
		if err != nil {
			return nil, err
		}
		data.PassportDateOfIssue = tempTime
	}
	data.PassportPlaceOfIssue = input.PassportPlaceOfIssue
	if input.Birthday != "" {
		tempTime, err := time.Parse("2006-01-02", input.Birthday)
		if err != nil {
			return nil, err
		}
		data.Birthday = tempTime
	}
	data.Email = input.Email
	data.ContactPerson = input.ContactPerson
	data.Comment = input.Comment
	data.AddressOfLiving = input.AddressOfLiving
	data.LegalAddress = input.LegalAddress
	data.INN = input.INN
	data.OGRN = input.OGRN
	data.Bank = input.Bank
	data.BIK = input.BIK
	data.KPP = input.KPP
	data.CheckingBill = input.CheckingBill
	data.CorrespondentBill = input.CorrespondentBill

	data.UID = uuid.Must(uuid.NewV4()).String()
	data.AccountsID = accountID
	if input.CreatedAt != nil {
		if tempCreatedAt, err := time.Parse("2006-01-02 15:04:05", *input.CreatedAt); err == nil {
			data.CreatedAt = tempCreatedAt
		} else {
			log.Print(err)
		}
	}

	if err := rep.checkUser(&data); err != nil {
		return nil, err
	}

	rep.Db.Create(&data)

	if err := history.CreateHistory(data.ID, userTableName, "create", nil, &data, authUserID, accountID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return &data, nil

}

func (rep *UserRepository) Edit(uid string, accountToken string, input *it.InputEditUser, authUserID int64) (*bt.User, error) {

	data, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var dataBefore = *data

	if input.ExtID != nil {
		data.ExtID = *input.ExtID
	}
	if input.FirstName != nil {
		data.FirstName = *input.FirstName
	}
	if input.LastName != nil {
		data.LastName = *input.LastName
	}
	if input.MiddleName != nil {
		data.MiddleName = *input.MiddleName
	}
	if input.Phone != nil {
		*input.Phone = helpers.CleanPhone(*input.Phone)
		data.Phone = *input.Phone
		var users []bt.User
		if result := rep.Db.Find(&users, "phone = ? AND id != ? AND accounts_id = ?", data.Phone, data.ID, data.AccountsID); result.RowsAffected != 0 {
			return nil, errors.New(logs.ERRExistingPhoNum)
		}
	}
	if input.FirebaseUID != nil {
		data.FirebaseUID = *input.FirebaseUID
	}
	if input.Firebase != nil {
		data.Firebase = *input.Firebase
	}

	if input.PartnerType != nil {
		data.PartnerType = *input.PartnerType
	}
	if input.PassportNumber != nil {
		data.PassportNumber = *input.PassportNumber
	}
	if input.PassportSeries != nil {
		data.PassportSeries = *input.PassportSeries
	}
	if input.PassportIssuedBy != nil {
		data.PassportIssuedBy = *input.PassportIssuedBy
	}
	if input.PassportDateOfIssue != nil {
		tempTime, err := time.Parse("2006-01-02", *input.PassportDateOfIssue)
		if err != nil {
			return nil, err
		}
		data.PassportDateOfIssue = tempTime
	}
	if input.PassportPlaceOfIssue != nil {
		data.PassportPlaceOfIssue = *input.PassportPlaceOfIssue
	}
	if input.Birthday != nil {
		tempTime, err := time.Parse("2006-01-02", *input.Birthday)
		if err != nil {
			return nil, err
		}
		data.Birthday = tempTime
	}
	if input.Email != nil {
		data.Email = *input.Email
	}
	if input.ContactPerson != nil {
		data.ContactPerson = *input.ContactPerson
	}
	if input.Comment != nil {
		data.Comment = *input.Comment
	}
	if input.AddressOfLiving != nil {
		data.AddressOfLiving = *input.AddressOfLiving
	}
	if input.LegalAddress != nil {
		data.LegalAddress = *input.LegalAddress
	}
	if input.INN != nil {
		data.INN = *input.INN
	}
	if input.OGRN != nil {
		data.OGRN = *input.OGRN
	}
	if input.Bank != nil {
		data.Bank = *input.Bank
	}
	if input.BIK != nil {
		data.BIK = *input.BIK
	}
	if input.KPP != nil {
		data.KPP = *input.KPP
	}
	if input.CheckingBill != nil {
		data.CheckingBill = *input.CheckingBill
	}
	if input.CorrespondentBill != nil {
		data.CorrespondentBill = *input.CorrespondentBill
	}

	if err := rep.checkUser(data); err != nil {
		return nil, err
	}

	rep.Db.Save(data)

	if err := history.CreateHistory(data.ID, userTableName, "edit", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil

}

func (rep *UserRepository) Delete(uid string, accountToken string, authUserID int64) (*bt.User, error) {

	data, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var orderCount = int64(0)
	if result := helpers.DbAccountLeftJoin("ransom_orders", accountToken, rep.Db.Model(&bt.RansomOrder{})).Where("ransom_orders.users_id = ? AND ransom_orders.is_active = true", data.ID).Count(&orderCount); result.Error != nil {
		return nil, err
	} else if orderCount > 0 {
		return nil, errors.New(logs.ERRAttachedData)
	}
	if result := helpers.DbAccountLeftJoin("rent_orders", accountToken, rep.Db.Model(&bt.RentOrder{})).Where("rent_orders.users_id = ? AND rent_orders.is_active = true", data.ID).Count(&orderCount); result.Error != nil {
		return nil, err
	} else if orderCount > 0 {
		return nil, errors.New(logs.ERRAttachedData)
	}

	var dataBefore = *data

	rep.Db.Delete(data)

	if err := history.CreateHistory(data.ID, userTableName, "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return data, nil

}

func (rep *UserRepository) GetByUID(UID string, accountToken string, withNestedData bool) (*bt.User, error) {
	var data bt.User
	if result := helpers.DbAccountLeftJoin(userTableName, accountToken, rep.Db).Find(&data, "uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchUsersUID)
	}

	if withNestedData {
		var tempRansomOrder []bt.RansomOrder
		rep.Db.Find(&tempRansomOrder, "users_id = ? AND deleted_at IS null", data.ID)
		for i := range tempRansomOrder {
			var tempManager bt.Manager
			var tempVehicle bt.Vehicle
			var tempTariff bt.RansomTariff

			rep.Db.Find(&tempTariff, tempRansomOrder[i].TariffsID)
			if tempTariff.ID > 0 {
				tempRansomOrder[i].RansomTariff = &tempTariff
			}

			rep.Db.Find(&tempVehicle, tempRansomOrder[i].VehiclesID)
			if tempVehicle.ID > 0 {
				tempRansomOrder[i].Vehicle = &tempVehicle
			}

			rep.Db.Find(&tempManager, tempRansomOrder[i].ManagersID)
			if tempManager.ID > 0 {
				tempRansomOrder[i].Manager = &tempManager
			}
		}
		data.RansomOrders = &tempRansomOrder

		var tempRentOrder []bt.RentOrder
		rep.Db.Find(&tempRentOrder, "users_id = ? AND deleted_at IS null", data.ID)
		for i := range tempRentOrder {
			var tempManager bt.Manager
			var tempVehicle bt.Vehicle
			var tempTariff bt.RentTariff

			rep.Db.Find(&tempTariff, tempRentOrder[i].TariffsID)
			if tempTariff.ID > 0 {
				tempRentOrder[i].RentTariff = &tempTariff
			}

			rep.Db.Find(&tempVehicle, tempRentOrder[i].VehiclesID)
			if tempVehicle.ID > 0 {
				tempRentOrder[i].Vehicle = &tempVehicle
			}

			rep.Db.Find(&tempManager, tempRentOrder[i].ManagersID)
			if tempManager.ID > 0 {
				tempRentOrder[i].Manager = &tempManager
			}
		}
		data.RentOrders = &tempRentOrder

	}

	return &data, nil
}

func (rep *UserRepository) GetByPhone(accountToken string, phone string) (*bt.User, error) {
	var data bt.User
	if result := helpers.DbAccountLeftJoin(userTableName, accountToken, rep.Db).Find(&data, "users.phone = ?", helpers.CleanPhone(phone)); result.Error != nil {
		return nil, result.Error
	} else if result.RowsAffected == 0 {
		return nil, errors.New("ERROR: no such user's phone")
	}

	return &data, nil
}

func (rep *UserRepository) List(accountToken string, input *it.InputGetListUsers) (*helpers.Pagination, error) {

	var tx = helpers.DbAccountLeftJoin(userTableName, accountToken, rep.Db.Model(&bt.User{}))

	//Filters
	if input.Filters.Search != nil {
		var tempSearch = "%" + *input.Filters.Search + "%"
		tx = tx.Where("LOWER(first_name) LIKE LOWER(?) OR LOWER(last_name) LIKE LOWER(?)", tempSearch, tempSearch)
	}
	if input.Filters.UDIs != nil {
		tx = tx.Where("users.uid IN (?)", *input.Filters.UDIs)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.User
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 1000, userTableName, &data)(tx)

}

func (rep *UserRepository) History(UID string, accountToken string, input *it.InputGetHistoryList) (*helpers.Pagination, error) {

	//Getting base model data
	dataByUID, err := rep.GetByUID(UID, accountToken, false)
	if err != nil {
		return nil, err
	}

	//Filters
	tx := rep.Db.Model(&bt.History{}).Where("source_table = ? AND source_id = ?", userTableName, dataByUID.ID)

	if input.Filters.OperationType != nil {
		tx = tx.Where("operation_type = ?", *input.Filters.OperationType)
	}

	//Session
	tx = tx.Session(&gorm.Session{})

	//Order, Paginate, Set data
	var data []bt.History
	return helpers.DbGetDataAndPaginateList(input.OrderBy, input.OrderDir, &input.Page, &input.PageSize, 5, 100, "histories", &data)(tx)
}

func (rep *UserRepository) OrdersList(accountToken string, uid string) (*map[string]interface{}, error) {
	userData, err := rep.GetByUID(uid, accountToken, false)
	if err != nil {
		return nil, err
	}

	var allRentOrders []bt.RentOrder
	if result := helpers.DbAccountLeftJoin("rent_orders", accountToken, rep.Db.Unscoped()).Order("date_start DESC").Find(&allRentOrders, "users_id = ?", userData.ID); result.Error != nil {
		return nil, result.Error
	}
	for i := range allRentOrders {
		var tempVehicle bt.Vehicle
		var tempTariff bt.RentTariff
		rep.Db.Find(&tempVehicle, allRentOrders[i].VehiclesID)
		rep.Db.Find(&tempTariff, allRentOrders[i].TariffsID)
		allRentOrders[i].Vehicle = &tempVehicle
		allRentOrders[i].RentTariff = &tempTariff
		allRentOrders[i].VehicleUID = &tempVehicle.UID
		allRentOrders[i].UserUID = &userData.UID

	}

	var allRansomOrders []bt.RansomOrder
	if result := helpers.DbAccountLeftJoin("ransom_orders", accountToken, rep.Db.Unscoped()).Order("contract_date DESC").Find(&allRansomOrders, "users_id = ?", userData.ID); result.Error != nil {
		return nil, result.Error
	}
	for i := range allRansomOrders {
		var tempVehicle bt.Vehicle
		var tempTariff bt.RansomTariff
		rep.Db.Find(&tempVehicle, allRansomOrders[i].VehiclesID)
		rep.Db.Find(&tempTariff, allRansomOrders[i].TariffsID)
		allRansomOrders[i].Vehicle = &tempVehicle
		allRansomOrders[i].RansomTariff = &tempTariff
		allRansomOrders[i].VehicleUID = &tempVehicle.UID
		allRansomOrders[i].UserUID = &userData.UID

	}

	return &map[string]interface{}{"rent_orders": &allRentOrders, "ransom_orders": &allRansomOrders}, nil
}
