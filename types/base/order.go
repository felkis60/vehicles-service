package models

import "time"

type Order struct {
	ID              int64      `json:"id"`
	UID             string     `json:"uid"`
	IsActive        bool       `json:"is_active"`
	Status          string     `json:"status"`
	UserFullName    string     `json:"user_full_name"`
	VehicleFullName string     `json:"vehicle_full_name"`
	UserUID         string     `json:"user_uid"`
	VehicleUID      string     `json:"vehicle_uid"`
	UserPhone       string     `json:"user_phone"`
	RansomTerm      *int       `json:"ransom_term"`
	RansomYearRate  *float64   `json:"ransom_year_rate"`
	DayPayment      float64    `json:"day_payment"`
	DateStart       *time.Time `json:"date_start"`
	DateEnd         *time.Time `json:"date_end"`
	ContractDate    *time.Time `json:"contract_date"`
	ContractDateEnd *time.Time `json:"contract_date_end"`
	Type            string     `json:"type"`
}

type PaginationOrder struct {
	Page       int      `json:"page"`
	Items      *[]Order `json:"items"`
	TotalItems int64    `json:"total_items"`
	TotalPages int      `json:"total_pages"`
}
