package models

type Bill struct {
	Balance           float64 `json:"balance"`
	Deposit           float64 `json:"deposit"`
	DaysOfDelay       uint64  `json:"days_of_delay"`
	DaysOfDelayAmount float64 `json:"days_of_delay_amount"`
	DayPaymentsAmount float64 `json:"day_payments_amount"`
	RentDebitAmount   float64 `json:"rent_debit_amount"`
}
