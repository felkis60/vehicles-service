package other

import (
	controller "gitlab.com/felkis60/vehicles-service/api/rest/controllers"
	middleware "gitlab.com/felkis60/vehicles-service/api/rest/middleware"
	db "gitlab.com/felkis60/vehicles-service/database"
	elastic_search "gitlab.com/felkis60/vehicles-service/database/elastic_search"
	repos "gitlab.com/felkis60/vehicles-service/repository"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()

	v1 := r.Group("/v1")
	{
		{
			es := v1.Group("/es")
			es.GET("/list", func(c *gin.Context) {
				if err := elastic_search.ESPrintAll(); err != nil {
					c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
					return
				}
				c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: nil})
			})
		}
		{
			accounts := v1.Group("/accounts")
			rep := repos.AccountRepository{Db: db.Db}
			ctrl := &controller.AccountController{Repository: rep}

			accounts.POST("", ctrl.Create)
			accounts.POST("/", ctrl.Create)
			accounts.POST("/:token", ctrl.Get)
			accounts.POST("/:token/edit", ctrl.Edit)
		}

		{
			users := v1.Group("/users").Use(middleware.TokenLogin())

			rep := repos.UserRepository{Db: db.Db}
			ctrl := &controller.UserController{Repository: rep}

			users.POST("", ctrl.Create)
			users.POST("/", ctrl.Create)
			users.POST("/list", ctrl.List)

			usersUID := users.Use(middleware.UIDHandler())
			{
				usersUID.POST("/:uid", ctrl.GetByUID)
				usersUID.POST("/:uid/edit", ctrl.Edit)
				usersUID.POST("/:uid/delete", ctrl.Delete)
			}
		}

		{
			managers := v1.Group("/managers").Use(middleware.TokenLogin())

			rep := repos.ManagerRepository{Db: db.Db}
			ctrl := &controller.ManagerController{Repository: rep}

			managers.POST("", ctrl.Create)
			managers.POST("/", ctrl.Create)
			managers.POST("/list", ctrl.List)

			managersUID := managers.Use(middleware.UIDHandler())
			{
				managersUID.POST("/:uid", ctrl.GetByUID)
				managersUID.POST("/:uid/edit", ctrl.Edit)
				managersUID.POST("/:uid/delete", ctrl.Delete)
			}
		}

		{
			vehicles := v1.Group("/vehicles").Use(middleware.TokenLogin())

			rep := repos.VehicleRepository{Db: db.Db}
			ctrl := &controller.VehicleController{Repository: rep}

			vehicles.POST("", ctrl.Create)
			vehicles.POST("/", ctrl.Create)
			vehicles.POST("/list", ctrl.List)

			vehiclesUID := vehicles.Use(middleware.UIDHandler())
			{
				vehiclesUID.POST("/:uid", ctrl.GetByUID)
				vehiclesUID.POST("/:uid/edit", ctrl.Edit)
				vehiclesUID.POST("/:uid/delete", ctrl.Delete)
			}
		}

		{
			rentOrders := v1.Group("/rent-orders").Use(middleware.TokenLogin())

			rep := repos.RentOrderRepository{Db: db.Db}
			ctrl := &controller.RentOrderController{Repository: rep}

			rentOrders.POST("", ctrl.Create)
			rentOrders.POST("/", ctrl.Create)
			rentOrders.POST("/multiple", ctrl.CreateMultiple)
			rentOrders.POST("/list", ctrl.List)

			rentOrdersUID := rentOrders.Use(middleware.UIDHandler())
			{
				rentOrdersUID.POST("/:uid", ctrl.GetByUID)
				rentOrdersUID.POST("/:uid/edit", ctrl.Edit)
				rentOrdersUID.POST("/:uid/delete", ctrl.Delete)
			}
		}

		{
			rentTariffs := v1.Group("/rent-tariffs").Use(middleware.TokenLogin())

			rep := repos.RentTariffRepository{Db: db.Db}
			ctrl := &controller.RentTariffController{Repository: rep}

			rentTariffs.POST("", ctrl.Create)
			rentTariffs.POST("/", ctrl.Create)
			rentTariffs.POST("/list", ctrl.List)

			rentTariffsUID := rentTariffs.Use(middleware.UIDHandler())
			{
				rentTariffsUID.POST("/:uid", ctrl.GetByUID)
				rentTariffsUID.POST("/:uid/edit", ctrl.Edit)
				rentTariffsUID.POST("/:uid/delete", ctrl.Delete)
			}
		}

		{
			ransomOrders := v1.Group("/ransom-orders").Use(middleware.TokenLogin())

			rep := repos.RansomOrderRepository{Db: db.Db}
			ctrl := &controller.RansomOrderController{Repository: rep}

			ransomOrders.POST("", ctrl.Create)
			ransomOrders.POST("/", ctrl.Create)
			ransomOrders.POST("/list", ctrl.List)
			ransomOrders.POST("/search", ctrl.Search)

			ransomOrdersUID := ransomOrders.Use(middleware.UIDHandler())
			{
				ransomOrdersUID.POST("/:uid", ctrl.GetByUID)
				ransomOrdersUID.POST("/:uid/edit", ctrl.Edit)
				ransomOrdersUID.POST("/:uid/delete", ctrl.Delete)
				ransomOrdersUID.POST("/:uid/contribute", ctrl.Contribute)
			}
		}

		{
			ransomTariffs := v1.Group("/ransom-tariffs").Use(middleware.TokenLogin())

			rep := repos.RansomTariffRepository{Db: db.Db}
			ctrl := &controller.RansomTariffController{Repository: rep}

			ransomTariffs.POST("", ctrl.Create)
			ransomTariffs.POST("/", ctrl.Create)
			ransomTariffs.POST("/list", ctrl.List)

			ransomTariffsUID := ransomTariffs.Use(middleware.UIDHandler())
			{
				ransomTariffsUID.POST("/:uid", ctrl.GetByUID)
				ransomTariffsUID.POST("/:uid/edit", ctrl.Edit)
				ransomTariffsUID.POST("/:uid/delete", ctrl.Delete)
			}
		}

		{
			addInvestments := v1.Group("/add-investments").Use(middleware.TokenLogin())

			rep := repos.AddInvestmentRepository{Db: db.Db}
			ctrl := &controller.AddInvestmentController{Repository: rep}

			addInvestments.POST("", ctrl.Create)
			addInvestments.POST("/", ctrl.Create)
			addInvestments.POST("/list", ctrl.List)
			addInvestments.POST("/search", ctrl.Search)

			addInvestmentsUID := addInvestments.Use(middleware.UIDHandler())
			{
				addInvestmentsUID.POST("/:uid", ctrl.GetByUID)
				addInvestmentsUID.POST("/:uid/edit", ctrl.Edit)
				addInvestmentsUID.POST("/:uid/delete", ctrl.Delete)
			}
		}

		{
			orders := v1.Group("/orders").Use(middleware.TokenLogin())

			rep := repos.OrderRepository{Db: db.Db}
			ctrl := &controller.OrderController{Repository: rep}

			orders.POST("/list", ctrl.List)
			//orders.POST("/list2", ctrl.List2)

		}
	}

	return r
}
