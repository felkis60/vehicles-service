package models

import (
	"time"
)

type InputCreateRentOrder struct {
	UsersUID             string    `json:"users_uid"`
	VehiclesUID          string    `json:"vehicles_uid"`
	ManagersUID          string    `json:"managers_uid"`
	TariffsUID           string    `json:"tariffs_uid"`
	DateStart            time.Time `json:"date_start"`
	DateEnd              time.Time `json:"date_end"`
	IsActive             bool      `json:"is_active"`
	DepositPayout        float64   `json:"deposit_payout"`
	DateEndDescription   *string   `json:"date_end_description"`
	ReasonForTerminating *string   `json:"reason_for_terminating"`
}

type InputEditRentOrder struct {
	Status               *string    `json:"status" example:"PRELIMINARY|RESERVE|SUCCESS|CANCEL"`
	ManagersUID          *string    `json:"managers_uid"`
	VehiclesUID          *string    `json:"vehicles_uid"`
	UsersUID             *string    `json:"users_uid"`
	DateStart            *time.Time `json:"date_start"`
	DateEnd              *time.Time `json:"date_end"`
	IsActive             *bool      `json:"is_active"`
	TariffsUID           *string    `json:"tariffs_uid"`
	DepositPayout        *float64   `json:"deposit_payout"`
	DateEndDescription   *string    `json:"date_end_description"`
	ReasonForTerminating *string    `json:"reason_for_terminating"`
	VehicleStatus        *string    `json:"vehicle_status"`
}
