package models

import (
	it "gitlab.com/felkis60/vehicles-service/types/input"
	"gorm.io/datatypes"
)

type ResponceRexGetVehcile struct {
	Payload interface{} `json:"payload"`
	Message string      `json:"message"`
	Code    string      `json:"code"`
	Status  int         `json:"status"`
}

type RexVehicle struct {
	ID           int64          `json:"id"`
	ExtID        int64          `json:"ext_id"`
	AccountsID   int64          `json:"accounts_id"`
	Number       string         `json:"number"`
	Mark         string         `json:"mark"`
	Model        string         `json:"model"`
	Type         string         `json:"type"`
	Sts          string         `json:"sts"`
	Win          string         `json:"win"`
	Width        *int           `json:"widht"`
	Height       *int           `json:"height"`
	Depth        *int           `json:"depth"`
	Capacity     *float32       `json:"capacity"`
	FuelTank     *int           `json:"fuel_tank"`
	SummerRate   *float32       `json:"summer_rate"`
	WinterRate   *float32       `json:"winter_rate"`
	Ransom       bool           `json:"ransom"`
	Description  string         `json:"description"`
	CreatedAt    string         `json:"created_at"`
	UpdatedAt    string         `json:"updated_at"`
	AchivedAt    string         `json:"achive_at"`
	Bodywork     string         `json:"bodywork"`      //Кузов
	Color        string         `json:"color"`         //цвет
	CustomFields datatypes.JSON `json:"custom_fields"` //ДЖОС ПОЛЯ
	Drive        string         `json:"drive"`         //Привод
	Engine       string         `json:"engine"`
	Kpp          string         `json:"kpp"` //Коробка передач
	Owner        string         `josn:"owner"`
	RentUsersID  *int           `json:"rent_users_id"`
	Salon        string         `json:"salon"`
	Year         string         `json:"year"`
	//Files       string  `json:"files"`
}

func (from *RexVehicle) ToInputCreateVehicle(to *it.InputCreateVehicle) {

	to.ExtID = from.ID
	to.Number = from.Number
	to.Mark = from.Mark
	to.Model = from.Model
	to.Type = from.Type
	to.Sts = from.Sts
	to.Win = from.Win
	to.Width = from.Width
	to.Height = from.Height
	to.Depth = from.Depth
	to.Capacity = from.Capacity
	to.FuelTank = from.FuelTank
	to.SummerRate = from.SummerRate
	to.WinterRate = from.WinterRate
	to.Ransom = from.Ransom
	to.Description = from.Description
	to.Bodywork = from.Bodywork
	to.Color = from.Color
	to.CustomFields = from.CustomFields
	to.Drive = from.Drive
	to.Engine = from.Engine
	to.Kpp = from.Kpp
	to.Owner = from.Owner
	to.Salon = from.Salon
	to.Year = from.Year
	to.CreatedAt = &from.CreatedAt
}
